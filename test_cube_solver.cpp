#include <chrono>
#include <thread>
#include "CubeSolver.h"
#include "gtest/gtest.h"

namespace cube
{

void solve_cube(int num_iterations, int length, size_t seed, int& num_successful)
{
    num_successful = 0;
    std::mt19937_64 random_number_generator{seed};
    for (int iteration = 0; iteration < num_iterations; iteration++)
    {
        const TrackedAlgorithm test_algorithm =
            random_tracked_algorithm(length, random_number_generator);
        CubeState cube{test_algorithm};
        CubeSolver solver{cube};
        cube.perform(solver.solve());
        if (cube.is_solved()) {num_successful++;};
    }
}

TEST(CubeSolverTest, ManySeeds)
{
    const int num_iterations{1000};
    const int num_threads = 6;
    const int quotient = num_iterations / num_threads;
    const int modulus = num_iterations - (quotient * num_threads);
    const int algorithm_length = 20;
    int num_solved{0};
    int* successful_by_thread = new int[num_threads];
    std::vector<std::thread> threads;
    const auto start = std::chrono::steady_clock::now();
    for (int thread = 0; thread < num_threads - 1; thread++)
    {
        const int num_iterations_in_this_thread =
            quotient + ((thread < modulus) ? 1 : 0);
        threads.emplace_back(solve_cube, num_iterations_in_this_thread,
            algorithm_length, thread, std::ref(successful_by_thread[thread]));
    }
    solve_cube(quotient, algorithm_length, num_threads - 1,
        successful_by_thread[num_threads - 1]);
    for (auto& thread : threads)
    {
        thread.join();
    }
    for (int thread = 0; thread < num_threads; thread++)
    {
        num_solved += successful_by_thread[thread];
    }
    delete[] successful_by_thread;
    const auto end = std::chrono::steady_clock::now();
    EXPECT_EQ(num_solved, num_iterations) << "Scrambled and attempted to solve " 
        << num_iterations << " cubes, but was only able to solve "
        << num_solved << ".";
    const int64_t duration =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "Took " << duration << " ms to scramble and solve "
        << num_iterations << " cubes ("
        << ((double)duration) / num_iterations << " ms per cube) with "
        << num_threads << " threads." << std::endl;
}

}  // namespace cube::test

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}