#ifndef COLOR_H
#define COLOR_H

#include <tuple>
#include <vector>
#include <optional>
#include "TripleSwapInfo.h"

namespace cube
{
    /**
     * Enumeration used to represent the colors on a Rubik's cube. The values
     * are designed so that opposite colors add up to 5.
     */
    enum class Color
    {
        /// white color
        WHITE = 0,
        /// orange color
        ORANGE = 1,
        /// blue color
        BLUE = 2,
        /// green color
        GREEN = 3,
        /// red color
        RED = 4,
        /// yellow color
        YELLOW = 5
    };
    
    /**
     * Gives a character form of the color for printing purposes.
     * 
     * @param color the color to turn into a char
     * 
     * @returns the character form of the color (upper case first letter)
     */
    char color_to_char(Color color);
    
    /**
     * Translates the given character into a Color.
     * 
     * @param character the char to make into a Color
     * 
     * @returns the Color form of character
     */
    Color char_to_color(char character);
    
    /**
     * Writes out the color to an output stream.
     * 
     * @param out the output stream to print to
     * @param color the Color to print out
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const Color& color);
    
    /**
     * Reads in a color from an input stream.
     * 
     * @param in the input stream to read from
     * @param color reference to Color to fill
     * 
     * @returns reference to the input stream
     */
    std::istream& operator>>(std::istream& in, Color& color);
    
    /// static const variable referring to the color white
    static const Color WHITE{Color::WHITE};
    /// static const variable referring to the color orange
    static const Color ORANGE{Color::ORANGE};
    /// static const variable referring to the color blue
    static const Color BLUE{Color::BLUE};
    /// static const variable referring to the color green
    static const Color GREEN{Color::GREEN};
    /// static const variable referring to the color red
    static const Color RED{Color::RED};
    /// static const variable referring to the color yellow
    static const Color YELLOW{Color::YELLOW};

    /// Hasher for the Color class.
    class ColorHash
    {
        public:
            /**
             * Hashes a Color object
             * 
             * @param color the color to hash
             * 
             * @returns the integer value of the color
             */
            size_t operator()(const Color& color) const;
    };

    /// Static hasher object for ColorPairHash and ColorTripleHash to use.
    static const ColorHash color_hasher;
    
    /**
     * @brief Class representing the colors of an edge
     * 
     * Class representing a pair of colors that can form an edge on a Rubik's
     * cube.
     */
    class ColorPair
    {
        private:
            /// The first color
            Color first_;
            /// The second color
            Color second_;
        public:
            /**
             * Constructs a color pair with the given colors
             * 
             * @param first the first color
             * @param second the second color
             */
            ColorPair(Color first, Color second);
            
            /**
             * Copies the given ColorPair into this one.
             * 
             * @param pair the ColorPair to copy
             */
            ColorPair(const ColorPair& pair);

            /**
             * Swaps the two colors in this ColorPair.
             */
            void swap();
            
            /**
             * Getter for the first color.
             * 
             * @returns the first color.
             */
            Color first() const;
            
            /**
             * Getter for the second color.
             * 
             * @returns the second color.
             */
            Color second() const;
            
            /**
             * Checks if the two colors can represent an edge on a Rubik's cube.
             * 
             * @returns true if and only if first and second form an edge that
             *          exists on a Rubik's cube (i.e. the colors are neither
             *          the same or opposite)
             */
            bool is_valid() const;
            
            /**
             * Computes whether the given ColorPair is a permutation of this
             * ColorPair.
             * 
             * @param pair ColorPair to check
             * 
             * @returns an optional bool. If pair is a permuted version of this
             *          ColorPair, then the value it stores is true if pair is
             *          equal to this ColorPair and false if pair is the
             *          opposite of this ColorPair (i.e. it has the same colors
             *          but they are flipped).
             */
            std::optional<bool> permutation(const ColorPair& pair) const;
            
            /**
             * Checks for equality between two ColorPair objects.
             * 
             * @param pair the ColorPair to check for equality with this
             *             ColorPair
             * 
             * @returns true if and only if pair has same first and second
             *          colors as this ColorPair
             */
            bool operator==(const ColorPair& pair) const;
            
            /**
             * Checks for inequality between two ColorPair objects.
             * 
             * @param pair the ColorPair to check for inequality with this
             *             ColorPair
             * 
             * @returns false if and only if pair has same first and second
             *          colors as this ColorPair
             */
            bool operator!=(const ColorPair& pair) const;
    };
    
    /**
     * Prints a string about the given ColorPair to the given output stream.
     * 
     * @param out the output stream to print to
     * @param pair the ColorPair to output
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const ColorPair& pair);
    
    /// Hasher for the ColorPair class.
    class ColorPairHash
    {
        public:
            /**
             * Creates a unique hash of the ColorPair.
             * 
             * @param pair ColorPair to hash
             * 
             * @returns hash of the ColorPair
             */
            size_t operator() (const ColorPair& pair) const;
    };
    
    /**
     * @brief Class representing the colors of a corner
     * 
     * Class representing a triple of colors that can represent a corner on a
     * Rubik's cube.
     */
    class ColorTriple
    {
        private:
            /// the first color in the triple
            Color first_;
            /// the second color in the triple
            Color second_;
            /// the third color in the triple
            Color third_;
        public:
            /**
             * Constructs a new ColorTriple with the given colors
             * 
             * @param first the first color
             * @param second the second color
             * @param third the third color
             */
            ColorTriple(Color first, Color second, Color third);
            
            /**
             * Copies the given ColorTriple.
             * 
             * @param triple the ColorTriple to copy
             */
            ColorTriple(const ColorTriple& triple);

            /**
             * Modifies this ColorTriple by performing the given permutation.
             * 
             * @param required_swaps the permutation to perform
             */
            void perform_swaps(const TripleSwapInfo& required_swaps);
            
            /**
             * Creates a copy of this ColorTriple with the given permutation
             * applied.
             * 
             * @param required_swaps TripleSwapInfo describing the permutation
             *                       to perform
             * 
             * @returns copy of this ColorTriple with the given permutation
             *          performed
             */
            ColorTriple copy_with_swaps(const TripleSwapInfo& required_swaps) const;
            
            /**
             * Getter for the first color.
             * 
             * @returns the first color.
             */
            Color first() const;
            
            /**
             * Getter for the second color.
             * 
             * @returns the second color.
             */
            Color second() const;
            
            /**
             * Getter for the third color.
             * 
             * @returns the third color.
             */
            Color third() const;
            
            /**
             * Checks if this ColorTriple can represent a valid corner of a
             * Rubik's cube.
             */
            bool is_valid() const;
            
            /**
             * Finds how to make the given triple into this triple (if
             * possible).
             * 
             * @param triple the triple to compare to this ColorTriple
             * 
             * @returns an optional TripleSwapInfo. If triple is a permuted
             *          version of this ColorTriple, then the value it stores
             *          is the permutation needed to make triple into this
             *          ColorTriple.
             */
            std::optional<TripleSwapInfo>
                permutation(const ColorTriple& triple) const;
            
            /**
             * Checks for equality between two ColorTriple objects.
             * 
             * @param triple the ColorTriple to compare with
             * 
             * @returns true if and only if the three colors are identical
             */
            bool operator==(const ColorTriple& triple) const;
            
            /**
             * Checks for inequality between two ColorTriple objects.
             * 
             * @param triple the ColorTriple to compare with
             * 
             * @returns false if and only if the three colors are identical
             */
            bool operator!=(const ColorTriple& triple) const;
            
            /**
             * Creates a vector of the three colors.
             * 
             * @returns vector form of this ColorTriple
             */
            std::vector<Color> as_vector() const;
            
            /**
             * Finds the number of colors shared between this ColorTriple and
             * the given ColorTriple.
             * 
             * @param triple the triple to find 
             * 
             * @returns one of 0, 1, 2, or 3
             */
            int shared_colors(const ColorTriple& triple) const;
    };
    
    /**
     * Writes out a string form of this triple to the given output stream.
     * 
     * @param out the output stream to print to
     * @param triple the ColorTriple to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const ColorTriple& triple);
    
    /// Hasher for the ColorTriple class
    class ColorTripleHash
    {
        public:
            /**
             * Hashes a ColorTriple.
             * 
             * @param triple the ColorTriple to hash
             * 
             * @returns unique hash representing this ColorTriple
             */
            size_t operator()(const ColorTriple& triple) const;
    };
}

#endif
