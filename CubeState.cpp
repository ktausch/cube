#include <sstream>
#include "CubeState.h"

namespace cube
{
    void CubeState::initialize_centers()
    {
        centers = std::make_unique<CenterColorScheme>();
        for (const Direction& direction : all_directions)
        {
            centers->insert({direction, default_color_scheme.at(direction)});
        }
    }
    
    void CubeState::initialize_edges()
    {
        edges = std::make_unique<EdgeColorScheme>();
        for (const DirectionPair& direction_pair : all_direction_pairs)
        {
            const Color& first = default_color_scheme.at(direction_pair.first());
            const Color& second = default_color_scheme.at(direction_pair.second());
            const ColorPair color_pair{first, second};
            edges->insert({direction_pair, color_pair});
        }
    }

    void CubeState::initialize_corners()
    {
        corners = std::make_unique<CornerColorScheme>();
        for (const DirectionTriple& direction_triple : all_direction_triples)
        {
            const Color& first = default_color_scheme.at(direction_triple.first());
            const Color& second = default_color_scheme.at(direction_triple.second());
            const Color& third = default_color_scheme.at(direction_triple.third());
            const ColorTriple color_triple{first, second, third};
            corners->insert({direction_triple, color_triple});
        }
    }

    void CubeState::apply_center_map(const DirectionMap& center_map)
    {
        CenterColorScheme new_centers;
        for (const auto& [from, to] : center_map)
        {
            new_centers.insert({to, centers->at(from)});
        }
        centers = std::make_unique<CenterColorScheme>(new_centers);
    }

    void CubeState::apply_edge_map(const DirectionPairMap& edge_map)
    {
        EdgeColorScheme new_edges;
        for (const auto& [from, to_and_required_swap] : edge_map)
        {
            const auto& [to, required_swap] = to_and_required_swap;
            ColorPair edge_colors = edges->at(from);
            if (required_swap) {edge_colors.swap();}
            new_edges.insert({to, edge_colors});
        }
        edges = std::make_unique<EdgeColorScheme>(new_edges);
    }

    void CubeState::apply_corner_map(const DirectionTripleMap& corner_map)
    {
        CornerColorScheme new_corners;
        for (const auto& [from, to_and_required_swaps] : corner_map)
        {
            const auto& [to, required_swaps] = to_and_required_swaps;
            ColorTriple corner_colors = corners->at(from);
            corner_colors.perform_swaps(required_swaps);
            new_corners.insert({to, corner_colors});
        }
        corners = std::make_unique<CornerColorScheme>(new_corners);
    }

    void CubeState::apply_maps(const DirectionMap& center_map,
        const DirectionPairMap& edge_map, const DirectionTripleMap& corner_map)
    {
        apply_center_map(center_map);
        apply_edge_map(edge_map);
        apply_corner_map(corner_map);
    }
    
    void CubeState::apply_rotation(const DirectionMap& center_map)
    {
        const DirectionPairMap& edge_map =
            pair_map_from_direction_map(center_map);
        const DirectionTripleMap& corner_map =
            triple_map_from_direction_map(center_map);
        apply_maps(center_map, edge_map, corner_map);
    }

    void CubeState::apply_layer_move(const DirectionPairMap& edge_map)
    {
        const DirectionTripleMap& corner_map =
            triple_map_from_pair_map(edge_map);
        apply_maps(IDENTITY_DIRECTION_MAP, edge_map, corner_map);
    }

    CubeState::CubeState() : cheated{false}, guaranteed_solvable{true}
    {
        initialize_centers();
        initialize_edges();
        initialize_corners();
    }

    CubeState::CubeState(const CubeState& other) : 
        centers{std::make_unique<CenterColorScheme>(*(other.centers))},
        edges{std::make_unique<EdgeColorScheme>(*(other.edges))},
        corners{std::make_unique<CornerColorScheme>(*(other.corners))},
        cheated{other.cheated},
        guaranteed_solvable{other.guaranteed_solvable} {}
    
    CubeState::CubeState(const Algorithm& scramble) : CubeState{}
    {
        perform(scramble);
    }
    
    CubeState::CubeState(const TrackedAlgorithm& scramble) : CubeState{}
    {
        perform(scramble);
    }

    CubeState::CubeState(const CenterColorScheme& centers,
        const EdgeColorScheme& edges, const CornerColorScheme& corners) :
        centers{std::make_unique<CenterColorScheme>(centers)},
        edges{std::make_unique<EdgeColorScheme>(edges)},
        corners{std::make_unique<CornerColorScheme>(corners)},
        cheated{false},
        guaranteed_solvable{false} {}
    
    void CubeState::operator=(CubeState&& cube)
    {
        centers = std::move(cube.centers);
        edges = std::move(cube.edges);
        corners = std::move(cube.corners);
        cheated = cube.cheated;
        guaranteed_solvable = cube.guaranteed_solvable;
    }
    
    const CenterColorScheme& CubeState::center_color_scheme() const
    {
        return (*centers);
    }
    
    const EdgeColorScheme& CubeState::edge_color_scheme() const
    {
        return (*edges);
    }
    
    const CornerColorScheme& CubeState::corner_color_scheme() const
    {
        return (*corners);
    }
    
    const DirectionMap& CubeState::get_tumble_map(const Color& top,
        const Color& front) const
    {
        if (centers->at(UP) == top)
        {
            return IDENTITY_DIRECTION_MAP;
        }
        else if (centers->at(DOWN) == top)
        {
            if (centers->at(FRONT) == front)
            {
                return FLIP_LEFT;
            }
            else
            {
                return FLIP_FORWARD;
            }
        }
        else if (centers->at(FRONT) == top)
        {
            return TUMBLE_BACK;
        }
        else if (centers->at(BACK) == top)
        {
            return TUMBLE_FORWARD;
        }
        else if (centers->at(LEFT) == top)
        {
            return TUMBLE_RIGHT;
        }
        else if (centers->at(RIGHT) == top)
        {
            return TUMBLE_LEFT;
        }
        else
        {
            throw std::runtime_error("The cube is malformed in some way.");
        }
    }
    
    const DirectionMap& CubeState::get_spin_map(const Color& front) const
    {
        if (centers->at(FRONT) == front)
        {
            return IDENTITY_DIRECTION_MAP;
        }
        else if (centers->at(LEFT) == front)
        {
            return SPIN_RIGHT;
        }
        else if (centers->at(RIGHT) == front)
        {
            return SPIN_LEFT;
        }
        else if (centers->at(BACK) == front)
        {
            return SPIN_TWICE;
        }
        else if ((centers->at(UP) == front) || (centers->at(DOWN) == front))
        {
            throw std::invalid_argument(
                "No way to spin about up-down axis so that top or bottom becomes front.");
        }
        else
        {
            throw std::runtime_error("The cube is malformed in some way.");
        }
    }
    
    const DirectionPairMap& CubeState::get_pair_map(const Move& move) const
    {
        const Direction& direction = move.layer();
        switch (direction.axis())
        {
            case Axis::UPDOWN:
                if (direction.first())
                {
                    if (move.twice()) {return UP_TWICE;}
                    else if (move.clockwise()) {return UP_CLOCKWISE;}
                    else {return UP_COUNTER_CLOCKWISE;}
                }
                else
                {
                    if (move.twice()) {return DOWN_TWICE;}
                    else if (move.clockwise()) {return DOWN_CLOCKWISE;}
                    else {return DOWN_COUNTER_CLOCKWISE;}
                }
                break;
            case Axis::FRONTBACK:
                if (direction.first())
                {
                    if (move.twice()) {return FRONT_TWICE;}
                    else if (move.clockwise()) {return FRONT_CLOCKWISE;}
                    else {return FRONT_COUNTER_CLOCKWISE;}
                }
                else
                {
                    if (move.twice()) {return BACK_TWICE;}
                    else if (move.clockwise()) {return BACK_CLOCKWISE;}
                    else {return BACK_COUNTER_CLOCKWISE;}
                }
                break;
            default: //case Axis::LEFTRIGHT:
                if (direction.first())
                {
                    if (move.twice()) {return LEFT_TWICE;}
                    else if (move.clockwise()) {return LEFT_CLOCKWISE;}
                    else {return LEFT_COUNTER_CLOCKWISE;}
                }
                else
                {
                    if (move.twice()) {return RIGHT_TWICE;}
                    else if (move.clockwise()) {return RIGHT_CLOCKWISE;}
                    else {return RIGHT_COUNTER_CLOCKWISE;}
                }
                break;
        }
    }
    
    const Color& CubeState::center_at(const Direction& direction) const
    {
        return centers->at(direction);
    }
    
    const ColorPair& CubeState::edge_at(const DirectionPair& pair) const
    {
        return edges->at(pair);
    }
    
    const ColorTriple& CubeState::corner_at(const DirectionTriple& triple) const
    {
        return corners->at(triple);
    }
    
    DirectionMap CubeState::orient(const Color& top, const Color& front)
    {
        const DirectionMap& tumble_map = get_tumble_map(top, front);
        if (tumble_map != IDENTITY_DIRECTION_MAP)
        {
            apply_rotation(tumble_map);
        }
        const DirectionMap& spin_map = get_spin_map(front);
        if (spin_map != IDENTITY_DIRECTION_MAP)
        {
            apply_rotation(spin_map);
        }
        return tumble_map + spin_map;
    }
    
    DirectionMap CubeState::orient(const ColorPair& top_front)
    {
        return orient(top_front.first(), top_front.second());
    }
    
    DirectionMap CubeState::orient(const DirectionMap& orientation_map)
    {
        if (is_valid(orientation_map))
        {
            if (orientation_map != IDENTITY_DIRECTION_MAP)
            {
                apply_rotation(orientation_map);
            }
            return orientation_map;
        }
        else
        {
            throw std::invalid_argument("The cube could not be oriented via a "
                "provided orientation map because it does not satisfy both of "
                "the following properties:\n1) the mapping is one to one\n2) "
                "opposite faces remain opposite\n3) the mapping does not "
                "imply a reflection");
        }
    }
    
    Direction CubeState::find(const Color& center) const
    {
        for (const Direction& direction : all_directions)
        {
            if (center == center_at(direction))
            {
                return direction;
            }
        }
        throw std::invalid_argument("The given edge is not a valid edge "
            "(i.e. it has two colors which are opposite and thus don't "
            "share an edge).");
    }
    
    std::pair<DirectionPair,bool> CubeState::find(const ColorPair& edge) const
    {
        for (const DirectionPair& pair : all_direction_pairs)
        {
            const std::optional<bool>& permutation =
                edge.permutation(edge_at(pair));
            if (permutation)
            {
                return {pair, *permutation};
            }
        }
        if (edge.is_valid())
        {
            throw std::runtime_error("The cube must be malformed in some way. "
                "The desired edge, which is valid, was not found.");
        }
        else
        {
            throw std::invalid_argument("The given edge is not a valid edge "
                "(i.e. it has two colors which are opposite and thus don't "
                "share an edge).");
        }
    }

    std::pair<DirectionTriple,TripleSwapInfo>
        CubeState::find(const ColorTriple& corner) const
    {
        for (const DirectionTriple& triple : all_direction_triples)
        {
            const std::optional<TripleSwapInfo>& permutation =
                corner.permutation(corner_at(triple));
            if (permutation)
            {
                return {triple, ~(*permutation)};
            }
        }
        if (corner.is_valid())
        {
            throw std::runtime_error("The cube must be malformed in some way. "
                "The desired corner, which is valid, was not found.");
        }
        else
        {
            throw std::invalid_argument("The given corner is not a valid corner "
                "(i.e. it has two colors which are opposite and thus don't "
                "share a corner).");
        }
    }

    const Move& CubeState::perform(const Move& move)
    {
        apply_layer_move(get_pair_map(move));
        return move;
    }
    
    const Algorithm& CubeState::perform(const Algorithm& algorithm)
    {
        for (const Move& move : algorithm)
        {
            perform(move);
        }
        return algorithm;
    }
    
    const TrackedAlgorithm& CubeState::perform(const TrackedAlgorithm& algorithm)
    {
        perform(algorithm.algorithm());
        orient(algorithm.orientation_map());
        return algorithm;
    }
    
    void CubeState::cheat(const DirectionMap& center_map,
        const DirectionPairMap& edge_map, const DirectionTripleMap& corner_map)
    {
        apply_maps(center_map, edge_map, corner_map);
        cheated = true;
        guaranteed_solvable = false;
    }
    
    bool CubeState::is_solved() const
    {
        for (const DirectionPair& pair : all_direction_pairs)
        {
            ColorPair expected_colors{
                center_at(pair.first()), center_at(pair.second())};
            if (edge_at(pair) != expected_colors)
            {
                return false;
            }
        }
        for (const DirectionTriple& triple : all_direction_triples)
        {
            ColorTriple expected_colors{center_at(triple.first()),
                center_at(triple.second()), center_at(triple.third())};
            if (corner_at(triple) != expected_colors)
            {
                return false;
            }
        }
        return true;
    }

    bool CubeState::operator==(const CubeState& other) const
    {
        // intentionally does not account for the cheated property!
        for (const Direction& direction : all_directions)
        {
            if (centers->at(direction) != other.centers->at(direction))
            {
                return false;
            }
        }
        for (const DirectionPair& direction_pair : all_direction_pairs)
        {
            if (edges->at(direction_pair) != other.edges->at(direction_pair))
            {
                return false;
            }
        }
        for (const DirectionTriple& direction_triple : all_direction_triples)
        {
            if (corners->at(direction_triple) != other.corners->at(direction_triple))
            {
                return false;
            }
        }
        return true;
    }

    bool CubeState::is_cheated() const
    {
        return cheated;
    }
    
    bool CubeState::is_guaranteed_solvable() const
    {
        return guaranteed_solvable;
    }

    bool CubeState::congruent(const CubeState& other) const
    {
        CubeState copy_of_other(other);
        copy_of_other.orient(centers->at(UP), centers->at(FRONT));
        return operator==(copy_of_other);
    }
    
    std::string CubeState::to_string() const
    {
        std::string string{""};
        Color* colors = new Color[54];
        for (const Direction& direction : all_directions)
        {
            const Color& center_color = center_at(direction);
            const short& index = to_string_center_indices.at(direction);
            colors[index] = center_color;
        }
        for (const DirectionPair& pair : all_direction_pairs)
        {
            const ColorPair& edge_colors = edge_at(pair);
            const std::pair<short,short>& indices =
                to_string_edge_indices.at(pair);
            colors[indices.first] = edge_colors.first();
            colors[indices.second] = edge_colors.second();
        }
        for (const DirectionTriple& triple : all_direction_triples)
        {
            const ColorTriple& corner_colors = corner_at(triple);
            const std::tuple<short,short,short>& indices =
                to_string_corner_indices.at(triple);
            colors[std::get<0>(indices)] = corner_colors.first();
            colors[std::get<1>(indices)] = corner_colors.second();
            colors[std::get<2>(indices)] = corner_colors.third();
        }
        int line = 0;
        int index = 0;
        for (; line < 3; line++)
        {
            string += "   ";
            for (int read = 0; read < 3; read++, index++)
            {
                string += color_to_char(colors[index]);
            }
            string += "   \n";
        }
        for (; line < 6; line++)
        {
            for (int read = 0; read < 9; read++, index++)
            {
                string += color_to_char(colors[index]);
            }
            string += "\n";
        }
        for (; line < 12; line++)
        {
            string += "   ";
            for (int read = 0; read < 3; read++, index++)
            {
                string += color_to_char(colors[index]);
            }
            string += "   \n";
        }
        delete[] colors;
        return string;
    }

    std::istream& ignore_three_spaces(std::istream& in)
    {
        for (int which = 0; which < 3; which++)
        {
            if (in.peek() == ' ')
            {
                in.ignore();
            }
        }
        return in;
    }

    std::istream& ignore_newline(std::istream& in)
    {
        if (in.peek() == '\n')
        {
            in.ignore();
        }
        return in;
    }

    std::istream& read_three_colors(std::istream& in, Color* color_array, int& index)
    {
        const int starting_index = index;
        for (int read = 0; read < 3; read++, index++)
        {
            char character;
            in >> character;
            color_array[index] = char_to_color(character);
        }
        const int ending_index = index;
        const int read = ending_index - starting_index;
        return in;
    }

    std::istream& operator>>(std::istream& in, Color* colors)
    {
        int index = 0;
        int line = 0;
        for (; line < 3; line++)
        {
            ignore_three_spaces(in);
            read_three_colors(in, colors, index);
            ignore_three_spaces(in);
            ignore_newline(in);
        }
        for (; line < 6; line++)
        {
            read_three_colors(in, colors, index);
            read_three_colors(in, colors, index);
            read_three_colors(in, colors, index);
            ignore_newline(in);
        }
        for (; line < 12; line++)
        {
            ignore_three_spaces(in);
            read_three_colors(in, colors, index);
            ignore_three_spaces(in);
            ignore_newline(in);
        }
        return in;
    }
    
    std::istream& operator>>(std::istream& in, CubeState& cube)
    {
        Color* colors = new Color[54];
        in >> colors;
        CenterColorScheme centers;
        for (const auto& [direction, index] : to_string_center_indices)
        {
            centers.emplace(direction, colors[index]);
        }
        EdgeColorScheme edges;
        for (const auto& [direction_pair, indices] : to_string_edge_indices)
        {
            const Color first_color = colors[indices.first];
            const Color second_color = colors[indices.second];
            const ColorPair this_edge{first_color, second_color};
            edges.emplace(direction_pair, this_edge);
        }
        CornerColorScheme corners;
        for (const auto& [direction_triple, indices] : to_string_corner_indices)
        {
            const Color first_color = colors[std::get<0>(indices)];
            const Color second_color = colors[std::get<1>(indices)];
            const Color third_color = colors[std::get<2>(indices)];
            const ColorTriple this_corner{first_color, second_color, third_color};
            corners.emplace(direction_triple, this_corner);
        }
        delete[] colors;
        cube = std::move(CubeState{centers, edges, corners});
        return in;
    }
    
    unsigned long period(const Algorithm& algorithm)
    {
        int num_iterations = 0;
        CubeState cube{};
        while ((num_iterations == 0) || (!cube.is_solved()))
        {
            cube.perform(algorithm);
            num_iterations++;
        }
        return num_iterations;
    }
    
    unsigned long period(const TrackedAlgorithm& algorithm)
    {
        int num_iterations = 0;
        CubeState cube{};
        while ((num_iterations == 0) || (!cube.is_solved()))
        {
            cube.perform(algorithm);
            num_iterations++;
        }
        return num_iterations;
    }
}
