#include "CubeState.h"

namespace cube
{

class CubeSolver
{
    /**
     * Writes a string describing the solution step by step.
     * 
     * @param cube the initial CubeState
     * @param print_intermediates if true, intermediate states of the cube are
     *                            printed to the output
     * @param spell_out_first_layer if true, the algorithms that solve the
     *                              edges and corners of the first layer are
     *                              printed to the output
     * @param spell_out_second_layer if true, the algorithms that solve the
     *                               edges of the second layer are printed to
     *                               the output
     * @param spell_out_third_layer if true, the algorithms that solve the
     *                              third layer are printed to the output
     * 
     * @returns string describing the time and algorithms it took to perform
     *          the steps of the solution
     */
    friend std::string solve_step_by_step(CubeState cube,
        bool print_intermediates, bool spell_out_first_layer,
        bool spell_out_second_layer, bool spell_out_third_layer);
    private:
        /// the cube being solved by this object
        CubeState cube;
        
        /**
         * Solves the cross on the white side.
         * 
         * @returns a TrackedAlgorithm that solves the cross on the white side
         */
        TrackedAlgorithm solve_first_cross();
        
        /**
         * Solves an edge on the first layer.
         * 
         * @param num_edges_solved the number of first layer edges already
         *                         solved
         * 
         * @returns TrackedAlgorithm that solves another first layer edge
         */
        TrackedAlgorithm solve_first_layer_edge(int num_edges_solved);
        
        /**
         * Solves all of the first layer's corners.
         * 
         * @returns TrackedAlgorithm that solves the corners of the first layer
         */
        TrackedAlgorithm solve_first_layer_corners();
        
        /**
         * Solves a first layer corner.
         * 
         * @param num_corners_solved the number of corners already solved
         * 
         * @returns TrackedAlgorithm that solves the next first layer corner
         */
        TrackedAlgorithm solve_first_layer_corner(int num_corners_solved);
        
        /**
         * Solves a second layer edge.
         * 
         * @param num_edges_solved the number of second layer edges already
         *                         solved
         * 
         * @returns TrackedAlgorithm that solves the next second layer edge
         */
        TrackedAlgorithm solve_second_layer_edge(int num_edges_solved);
        
        /**
         * Orients the third layer edges and corners so that the yellow faces
         * of the pieces are on the same side as the yellow center.
         * 
         * @returns TrackedAlgorithm that orients the third layer edges and
         *          corners
         */
        TrackedAlgorithm orient_third_layer();
        
        /**
         * Orients the third layer edges so that their yellow faces are on the
         * same side of the cube as the yellow center.
         * 
         * @returns TrackedAlgorithm that orients the third layer edges
         */
        TrackedAlgorithm orient_third_layer_edges();
        
        /**
         * Orients the third layer corners so that their yellow faces are on
         * the same side of the cube as the yellow center.
         * 
         * @returns TrackedAlgorithm that orients the third layer corners
         */
        TrackedAlgorithm orient_third_layer_corners();
        
        /**
         * Permutes the third layer's edges and corners (which have already
         * been oriented), solving the cube.
         * 
         * @returns TrackedAlgorithm that finishes the solution of the cube
         *          after third layer is oriented
         */
        TrackedAlgorithm permute_third_layer();
        
        /**
         * Permutes the third layer's edges.
         * 
         * @returns TrackedAlgorithm that moves the third layer edges into the
         *          correct position
         */
        TrackedAlgorithm permute_third_layer_edges();
        
        /**
         * Permutes the third layer's corners.
         * 
         * @returns TrackedAlgorithm that moves the third layer corners into
         *          the correct position
         */
        TrackedAlgorithm permute_third_layer_corners();
        
        /**
         * Solves the first layer of the cube.
         * 
         * @returns TrackedAlgorithm that solves the first layer of the cube
         */
        TrackedAlgorithm solve_first_layer();
        
        /**
         * Solves the second layer of the cube.
         * 
         * @returns TrackedAlgorithm that solves the second layer of the cube
         */
        TrackedAlgorithm solve_second_layer();
        
        /**
         * Solves the third layer of the cube.
         * 
         * @returns TrackedAlgorithm that solves the third layer of the cube
         */
        TrackedAlgorithm solve_third_layer();
    public:
        /**
         * Initializes a CubeSolver with a copy of the given cube.
         * 
         * @param state the Cube to solve
         */
        CubeSolver(const CubeState& state);
        
        /**
         * Finds the current state of the cube.
         * 
         * @returns the current state of the cube
         */
        const CubeState& state() const;
        
        /**
         * Finds an Algorithm that solves the cube.
         * 
         * @returns an Algorithm that solves the cube
         */
        Algorithm solve();
};

std::string solve_step_by_step(CubeState cube,
    bool print_intermediates, bool spell_out_first_layer,
    bool spell_out_second_layer, bool spell_out_third_layer);

}