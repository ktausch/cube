#include <chrono>
#include <iomanip>
#include "CubeSolver.h"

int main(int argc, char* argv[])
{
    cube::CubeState cube;
    std::cin >> cube;
    cube::CubeSolver solver{cube};
    const cube::Algorithm solution = solver.solve();
    std::cout << "\nsolution: " << solution << '\n';
    std::cout << "\nsolution length: " << solution.size() << '\n';
    return 0;
}
