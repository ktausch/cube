#include <chrono>
#include "CubeSolver.h"

int main()
{
    std::cout
        << "Please enter the algorithm to find the period of:" << std::endl;
    cube::Algorithm algorithm;
    std::cin >> algorithm;
    auto start = std::chrono::steady_clock::now();
    unsigned long period = cube::period(algorithm);
    auto end = std::chrono::steady_clock::now();
    std::cout
        << "Period of " << algorithm << " is " << period << "." << std::endl;
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    std::cout << "It took " << duration.count()
        << " us to evaluate the period!" << std::endl;
    return 0;
}