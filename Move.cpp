#include <memory>
#include <random>
#include <sstream>
#include "Move.h"

namespace cube
{
    Move::Move(Direction layer, bool clockwise, bool twice)
        : layer_{layer}, clockwise_{clockwise}, twice_{twice} {}

    Move::Move(const Move& move) :
        layer_{move.layer_}, clockwise_{move.clockwise_}, twice_{move.twice_} {}
    
    const Direction& Move::layer() const {return layer_;}

    const bool Move::clockwise() const {return clockwise_;}
    
    const bool Move::twice() const {return twice_;}

    Move Move::inverse() const {return Move{layer_, !clockwise_, twice_};}
    
    Move Move::mirror(const Axis& axis) const
    {
        if (layer_.axis() == axis)
        {
            return Move{Direction{axis,!layer_.first()}, !clockwise_, twice_};
        }
        else
        {
            return Move{layer_, !clockwise_, twice_};
        }
    }
    
    std::string Move::to_string() const
    {
        std::string result{""};
        if (layer_ == UP)
        {
            result += "U";
        }
        else if (layer_ == DOWN)
        {
            result += "D";
        }
        else if (layer_ == FRONT)
        {
            result += "F";
        }
        else if (layer_ == BACK)
        {
            result += "B";
        }
        else if (layer_ == LEFT)
        {
            result += "L";
        }
        else //(layer_ == RIGHT)
        {
            result += "R";
        }
        if (twice_)
        {
            result += "2";
        }
        else if (!clockwise_)
        {
            result += "'";
        }
        return result;
    }
    
    bool Move::operator==(const Move& other) const
    {
        if (layer_ != other.layer_)
        {
            return false;
        }
        if (twice_ && other.twice_)
        {
            return true;
        }
        return ((clockwise_ == other.clockwise_) && (twice_ == other.twice_));
    }
    
    /**
     * Applies the given pre-orientation to the Move.
     * 
     * @param pre_orientation the orientation to apply before the move
     * @param move the Move to transform
     * 
     * @returns the transformed Move
     */
    Move operator*(const DirectionMap& pre_orientation, const Move& move)
    {
        for (const auto& [from, to] : pre_orientation)
        {
            if (to == move.layer())
            {
                return {from, move.clockwise(), move.twice()};
            }
        }
        throw std::invalid_argument("The given pre_orientation was not "
            "well-formed. It could not be used to transform the given move.");
    }

    std::ostream& operator<<(std::ostream& out, const Move& move)
    {
        out << move.to_string();
        return out;
    }
    
    std::istream& operator>>(std::istream& in, std::unique_ptr<Move>& move)
    {
        char layer;
        in >> layer;
        bool clockwise = !(in.peek() == '\'');
        bool twice = (in.peek() == '2');
        
        if (layer == 'U' || layer == 'u')
        {
            move = std::make_unique<Move>(UP, clockwise, twice);
        }
        else if (layer == 'D' || layer == 'd')
        {
            move = std::make_unique<Move>(DOWN, clockwise, twice);
        }
        else if (layer == 'F' || layer == 'f')
        {
            move = std::make_unique<Move>(FRONT, clockwise, twice);
        }
        else if (layer == 'B' || layer == 'b')
        {
            move = std::make_unique<Move>(BACK, clockwise, twice);
        }
        else if (layer == 'L' || layer == 'l')
        {
            move = std::make_unique<Move>(LEFT, clockwise, twice);
        }
        else if (layer == 'R' || layer == 'r')
        {
            move = std::make_unique<Move>(RIGHT, clockwise, twice);
        }
        else
        {
            throw std::invalid_argument(
                "No move could be interpreted from the input stream.");
        }
        if ((!clockwise) || twice)
        {
            in >> layer;
        }
        return in;
    }
    
    std::ostream& operator<<(std::ostream& out, const Algorithm& algorithm)
    {
        if (algorithm.empty())
        {
            out << "(do nothing)";
        }
        for (const Move& move : algorithm)
        {
            out << move;
        }
        return out;
    }
    
    std::istream& operator>>(std::istream& in, Algorithm& algorithm)
    {
        while (in)
        {
            std::unique_ptr<Move> move{nullptr};
            in >> move;
            algorithm.push_back(*move);
            if (in.peek() == ' ') {in.ignore();}
            else if ((in.peek() == EOF) || (in.peek() == '\n')) {break;}
        }
        if (in.peek() == '\n') {in.ignore();}
        return in;
    }
    
    Algorithm read_algorithm(const std::string& string)
    {
        Algorithm algorithm;
        std::stringstream stream{string};
        stream >> algorithm;
        return algorithm;
    }
    
    Algorithm operator+(const Algorithm& first, const Algorithm& second)
    {
        Algorithm algorithm;
        algorithm.reserve(first.size() + second.size());
        algorithm.insert(algorithm.end(), first.begin(), first.end());
        algorithm.insert(algorithm.end(), second.begin(), second.end());
        return algorithm;
    }
    
    Algorithm operator*(const DirectionMap& orientation_map,
        const Algorithm& algorithm)
    {
        Algorithm transformed_algorithm;
        transformed_algorithm.reserve(algorithm.size());
        const DirectionMap inverse_map = ~orientation_map;
        for (const Move& move : algorithm)
        {
            transformed_algorithm.emplace_back(
                inverse_map.at(move.layer()), move.clockwise(), move.twice());
        }
        return transformed_algorithm;
    }
    
    Algorithm reduce_algorithm(const Algorithm& algorithm)
    {
        Algorithm new_algorithm;
        for (auto it = algorithm.begin(); it != algorithm.end();)
        {
            const Axis reduction_axis = it->layer().axis();
            std::unordered_map<bool,int> clockwise_turns{{false,0},{true,0}};
            while ((it != algorithm.end()) && (it->layer().axis() == reduction_axis))
            {
                clockwise_turns.at(it->layer().first()) +=
                    ((it->twice()) ? (2) : ((it->clockwise()) ? (1) : (3)));
                it++;
            }
            std::vector<bool> firsts{true,false};
            for (const bool first : firsts)
            {
                const Direction layer{reduction_axis, first};
                const int num_turns = ((clockwise_turns.at(first)) % 4);
                if (num_turns != 0)
                {
                    new_algorithm.emplace_back(
                        layer, num_turns == 1, num_turns == 2);
                }
            }
        }
        if (algorithm == new_algorithm)
        {
            return new_algorithm;
        }
        else
        {
            return reduce_algorithm(new_algorithm);
        }
    }

    Algorithm invert_algorithm(const Algorithm& algorithm)
    {
        Algorithm new_algorithm;
        new_algorithm.reserve(algorithm.size());
        for (auto it = algorithm.end(); it != algorithm.begin();)
        {
            it--;
            new_algorithm.push_back(std::move(it->inverse()));
        }
        return new_algorithm;
    }

    Algorithm mirror_algorithm(const Algorithm& algorithm, const Axis& axis)
    {
        Algorithm new_algorithm;
        new_algorithm.reserve(algorithm.size());
        for (const Move& move : algorithm)
        {
            new_algorithm.push_back(std::move(move.mirror(axis)));
        }
        return new_algorithm;
    }
    
    Algorithm random_algorithm(const size_t length,
        std::mt19937_64& random_number_generator)
    {
        Algorithm algorithm;
        algorithm.reserve(length);
        std::uniform_int_distribution<short> axis_distribution{0,2};
        std::bernoulli_distribution first_distribution{0.5};
        std::bernoulli_distribution clockwise_distribution{0.5};
        std::bernoulli_distribution twice_distribution{1. / 6};
        std::unique_ptr<Axis> last_axis{nullptr};
        for (size_t index = 0; index < length;)
        {
            const Axis axis =
                static_cast<Axis>(axis_distribution(random_number_generator));
            if ((last_axis != nullptr) && (axis == (*last_axis)))
            {
                continue;
            }
            const bool first = first_distribution(random_number_generator);
            const Direction layer{axis, first};
            const bool clockwise = clockwise_distribution(random_number_generator);
            const bool twice = twice_distribution(random_number_generator);
            algorithm.emplace_back(layer, clockwise, twice);
            last_axis = std::make_unique<Axis>(axis);
            index++;
        }
        return algorithm;
    }
    
    Algorithm random_algorithm(const size_t length, const unsigned long seed)
    {
        std::mt19937_64 random_number_generator{seed};
        return random_algorithm(length, random_number_generator);
    }
    
    TrackedAlgorithm::TrackedAlgorithm()
        : algorithm_{}, orientation_map_{IDENTITY_DIRECTION_MAP} {}
    
    TrackedAlgorithm::TrackedAlgorithm(const Algorithm& algorithm)
        : algorithm_{algorithm}, orientation_map_{IDENTITY_DIRECTION_MAP} {}
    
    TrackedAlgorithm::TrackedAlgorithm(const DirectionMap& orientation_map)
        : algorithm_{}, orientation_map_{orientation_map} {}
    
    TrackedAlgorithm::TrackedAlgorithm(const Algorithm& algorithm,
        const DirectionMap& orientation_map)
        : algorithm_{algorithm}, orientation_map_{orientation_map} {}
    
    TrackedAlgorithm::TrackedAlgorithm(const TrackedAlgorithm& to_copy)
        : algorithm_{to_copy.algorithm_}, orientation_map_{to_copy.orientation_map_} {}
    
    const Algorithm& TrackedAlgorithm::algorithm() const
    {
        return algorithm_;
    }

    const DirectionMap& TrackedAlgorithm::orientation_map() const
    {
        return orientation_map_;
    }

    size_t TrackedAlgorithm::size() const
    {
        return algorithm_.size();
    }
    
    void TrackedAlgorithm::reduce()
    {
        algorithm_ = reduce_algorithm(algorithm_);
    }
    
    TrackedAlgorithm TrackedAlgorithm::operator+(const TrackedAlgorithm& other) const
    {
        Algorithm algorithm =
            algorithm_ + (orientation_map_ * other.algorithm_);
        DirectionMap orientation_map =
            orientation_map_ + other.orientation_map_;
        return {algorithm, orientation_map};
    }

    TrackedAlgorithm& TrackedAlgorithm::operator+=(const TrackedAlgorithm& other)
    {
        algorithm_ = algorithm_ + (orientation_map_ * other.algorithm_);
        orientation_map_ = orientation_map_ + other.orientation_map_;
        return (*this);
    }
            
    TrackedAlgorithm& TrackedAlgorithm::operator+=(const Move& next)
    {
        algorithm_.push_back(std::move(orientation_map_ * next));
        return (*this);
    }
    
    TrackedAlgorithm& TrackedAlgorithm::operator+=(const Algorithm& next)
    {
        if (!next.empty())
        {
            algorithm_ = algorithm_ + (orientation_map_ * next);
        }
        return (*this);
    }
    
    TrackedAlgorithm& TrackedAlgorithm::operator+=(const DirectionMap& rotation)
    {
        if (rotation != IDENTITY_DIRECTION_MAP)
        {
            orientation_map_ = orientation_map_ + rotation;
        }
        return (*this);
    }
    
    std::ostream& operator<<(std::ostream& out, const TrackedAlgorithm& algorithm)
    {
        out << "Algorithm: " << algorithm.algorithm()
            << ", Orientation: " << algorithm.orientation_map();
        return out;
    }
    
    TrackedAlgorithm reduce_tracked_algorithm(const TrackedAlgorithm& algorithm)
    {
        TrackedAlgorithm reduced{algorithm};
        reduced.reduce();
        return reduced;
    }

    TrackedAlgorithm random_tracked_algorithm(const size_t length,
        std::mt19937_64& random_number_generator)
    {
        const Algorithm& algorithm =
            random_algorithm(length, random_number_generator);
        const DirectionMap& orientation =
            random_orientation(random_number_generator);
        return {algorithm, orientation};
    }
    
    TrackedAlgorithm random_tracked_algorithm(const size_t length,
        const unsigned long seed)
    {
        std::mt19937_64 random_number_generator{seed};
        return random_tracked_algorithm(length, random_number_generator);
    }
}
