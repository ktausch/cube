#ifndef TRIPLESWAP_H
#define TRIPLESWAP_H

#include <iostream>
#include <tuple>

namespace cube
{
    /**
     * TripleSwapInfo objects represent permutations of 3 objects using three
     * bools representing sequential swaps.
     * 1. The first bool determines if the first two elements should be swapped
     * 2. The second bool then determines if the last two elements should be swapped
     * 3. The third bool finally determines if the first two elements should be swapped
     * 
     * \- - - = + - + = (123->123)
     * 
     * \+ - - = - - + = (123->213)
     * 
     * \- + - =       (123->132)
     * 
     * \+ + - =       (123->231)
     * 
     * \- + + =       (123->312)
     * 
     * \+ + + =       (123->321)
     */
    typedef std::tuple<bool,bool,bool> TripleSwapInfo;

    /**
     * The ~ operator implements the inverse swap.
     * 
     * @param info TripleSwapInfo to invert
     * 
     * @returns the inverse operation as a TripleSwapInfo
     */
    TripleSwapInfo operator~(const TripleSwapInfo& info);

    /**
     * The * operator implements the concatenation of swaps.
     * 
     * @param info1 the first set of swaps to perform
     * @param info2 the second set of swaps to perform
     * 
     * @returns the concatenation of the two swaps as a TripleSwapInfo
     */
    TripleSwapInfo operator*(
        const TripleSwapInfo& info1, const TripleSwapInfo& info2);
    std::ostream& operator<<(std::ostream& out, const TripleSwapInfo& info);
}

#endif