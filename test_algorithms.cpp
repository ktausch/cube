#include <sstream>
#include <random>
#include "CubeState.h"
#include "gtest/gtest.h"

namespace cube
{

class AlgorithmTestOneCube : public ::testing::Test
{
    protected:
        CubeState cube{};
};

class AlgorithmTestTwoCubes : public ::testing::Test
{
    protected:
        CubeState first_cube{};
        CubeState second_cube{};
};

TEST(ReduceAlgorithm, ReduceDegenerateAlgorithm)
{
    for (const Direction& direction : all_directions)
    {
        Algorithm bloated1{{direction, true, false}, {direction, false, false}};
        Algorithm bloated2{{direction, false, false}, {direction, true, false}};
        Algorithm bloated3{{direction, false, true}, {direction, false, true}};
        Algorithm reduced{};
        EXPECT_EQ(reduce_algorithm(bloated1), reduced);
        EXPECT_EQ(reduce_algorithm(bloated2), reduced);
        EXPECT_EQ(reduce_algorithm(bloated3), reduced);
    }
}

TEST(AlgorithmPeriod, DegenerateAlgorithmPeriod)
{
    Algorithm algorithm{};
    EXPECT_EQ(period(algorithm), 1);
}

TEST(AlgorithmPeriod, SingleMovePeriods)
{
    for (const Direction& direction : all_directions)
    {
        EXPECT_EQ(period({{direction, false, false}}), 4);
        EXPECT_EQ(period({{direction, true, false}}), 4);
        EXPECT_EQ(period({{direction, false, true}}), 2);
    }
}

TEST_F(AlgorithmTestOneCube, RandomishAlgorithmStringCheck)
{
    Algorithm algorithm = read_algorithm("BL2BULDL2BRD'");
    cube.perform(algorithm);
    EXPECT_EQ(cube.to_string(),
        "   YGR   \n"
        "   BRY   \n"
        "   GRY   \n"
        "BYYRBBRRW\n"
        "WBOBWOGGW\n"
        "RBOBWOWOO\n"
        "   WGG   \n"
        "   WOY   \n"
        "   WYY   \n"
        "   BGG   \n"
        "   RYO   \n"
        "   ORG   \n");
    EXPECT_FALSE(cube.is_cheated());
}

TEST_F(AlgorithmTestOneCube, RandomAlgorithmInverse)
{
    int algorithm_length = 20;
    std::mt19937_64 seed_generator{0u};
    for (int iteration = 0; iteration < 50; iteration++)
    {
        Algorithm algorithm =
            random_algorithm(algorithm_length, seed_generator());
        cube.perform(algorithm);
        cube.perform(invert_algorithm(algorithm));
        EXPECT_TRUE(cube.is_solved());
        EXPECT_FALSE(cube.is_cheated());
    }
}

TEST_F(AlgorithmTestOneCube, TestMiddleUp)
{
    cube.perform(MIDDLE_UP);
    EXPECT_EQ(cube.to_string(),
        "   RWR   \n"
        "   RWR   \n"
        "   RWR   \n"
        "BBBWOWGGG\n"
        "BBBWOWGGG\n"
        "BBBWOWGGG\n"
        "   OYO   \n"
        "   OYO   \n"
        "   OYO   \n"
        "   YRY   \n"
        "   YRY   \n"
        "   YRY   \n");
}

TEST_F(AlgorithmTestOneCube, TestMiddleDown)
{
    cube.perform(MIDDLE_DOWN);
    EXPECT_EQ(cube.to_string(),
        "   RYR   \n"
        "   RYR   \n"
        "   RYR   \n"
        "BBBWRWGGG\n"
        "BBBWRWGGG\n"
        "BBBWRWGGG\n"
        "   OWO   \n"
        "   OWO   \n"
        "   OWO   \n"
        "   YOY   \n"
        "   YOY   \n"
        "   YOY   \n");
}

TEST_F(AlgorithmTestOneCube, TestMiddleClockwise)
{
    cube.perform(MIDDLE_CLOCKWISE);
    EXPECT_EQ(cube.to_string(),
        "   RRR   \n"
        "   RRR   \n"
        "   RRR   \n"
        "BBBWWWGGG\n"
        "YYYBBBWWW\n"
        "BBBWWWGGG\n"
        "   OOO   \n"
        "   OOO   \n"
        "   OOO   \n"
        "   YYY   \n"
        "   GGG   \n"
        "   YYY   \n");
}

TEST_F(AlgorithmTestOneCube, TestMiddleCounterClockwise)
{
    cube.perform(MIDDLE_COUNTER_CLOCKWISE);
    EXPECT_EQ(cube.to_string(),
        "   RRR   \n"
        "   RRR   \n"
        "   RRR   \n"
        "BBBWWWGGG\n"
        "WWWGGGYYY\n"
        "BBBWWWGGG\n"
        "   OOO   \n"
        "   OOO   \n"
        "   OOO   \n"
        "   YYY   \n"
        "   BBB   \n"
        "   YYY   \n");
}

TEST_F(AlgorithmTestOneCube, TestMiddleLeft)
{
    cube.perform(MIDDLE_LEFT);
    EXPECT_EQ(cube.to_string(),
        "   RRR   \n"
        "   BBB   \n"
        "   RRR   \n"
        "BOBWWWGRG\n"
        "BOBWWWGRG\n"
        "BOBWWWGRG\n"
        "   OOO   \n"
        "   GGG   \n"
        "   OOO   \n"
        "   YYY   \n"
        "   YYY   \n"
        "   YYY   \n");
}

TEST_F(AlgorithmTestOneCube, TestMiddleRight)
{
    cube.perform(MIDDLE_RIGHT);
    EXPECT_EQ(cube.to_string(),
        "   RRR   \n"
        "   GGG   \n"
        "   RRR   \n"
        "BRBWWWGOG\n"
        "BRBWWWGOG\n"
        "BRBWWWGOG\n"
        "   OOO   \n"
        "   BBB   \n"
        "   OOO   \n"
        "   YYY   \n"
        "   YYY   \n"
        "   YYY   \n");
}

TEST_F(AlgorithmTestOneCube, CheckerBoardToString)
{
    Algorithm algorithm = read_algorithm("r2l2u2d2f2b2");
    cube.perform(algorithm);
    EXPECT_EQ(cube.to_string(),
        "   ROR   \n"
        "   ORO   \n"
        "   ROR   \n"
        "BGBWYWGBG\n"
        "GBGYWYBGB\n"
        "BGBWYWGBG\n"
        "   ORO   \n"
        "   ROR   \n"
        "   ORO   \n"
        "   YWY   \n"
        "   WYW   \n"
        "   YWY   \n");
}

TEST_F(AlgorithmTestOneCube, OtherCheckerBoardToString)
{
    TrackedAlgorithm algorithm{};
    algorithm += MIDDLE_UP_TWICE;
    algorithm += MIDDLE_CLOCKWISE_TWICE;
    algorithm += MIDDLE_LEFT_TWICE;
    cube.perform(algorithm);
    EXPECT_EQ(cube.to_string(),
        "   ROR   \n"
        "   ORO   \n"
        "   ROR   \n"
        "BGBWYWGBG\n"
        "GBGYWYBGB\n"
        "BGBWYWGBG\n"
        "   ORO   \n"
        "   ROR   \n"
        "   ORO   \n"
        "   YWY   \n"
        "   WYW   \n"
        "   YWY   \n");
}

TEST_F(AlgorithmTestTwoCubes, CornerCycle)
{
    Algorithm algorithm = read_algorithm("r'fr'b2rf'r'b2r2");
    first_cube.perform(algorithm);
    DirectionMap expected_center_map = IDENTITY_DIRECTION_MAP;
    DirectionPairMap expected_edge_map = IDENTITY_PAIR_MAP;
    DirectionTripleMap expected_corner_map{
        {{UP,FRONT,LEFT},{{UP,FRONT,LEFT},{false,false,false}}},
        {{DOWN,FRONT,LEFT},{{DOWN,FRONT,LEFT},{false,false,false}}},
        {{DOWN,FRONT,RIGHT},{{DOWN,FRONT,RIGHT},{false,false,false}}},
        {{DOWN,BACK,LEFT},{{DOWN,BACK,LEFT},{false,false,false}}},
        {{DOWN,BACK,RIGHT},{{DOWN,BACK,RIGHT},{false,false,false}}},
        {{UP,FRONT,RIGHT},{{UP,BACK,LEFT},{false,false,false}}},
        {{UP,BACK,LEFT},{{UP,BACK,RIGHT},{false,true,false}}},
        {{UP,BACK,RIGHT},{{UP,FRONT,RIGHT},{false,true,false}}}};
    second_cube.cheat(expected_center_map, expected_edge_map, expected_corner_map);
    EXPECT_EQ(first_cube, second_cube);
    EXPECT_FALSE(first_cube.is_cheated());
    EXPECT_TRUE(second_cube.is_cheated());
}

TEST_F(AlgorithmTestTwoCubes, EdgeCycle)
{
    Algorithm algorithm = read_algorithm("f2u'r'lf2rl'u'f2");
    first_cube.perform(algorithm);
    DirectionMap expected_center_map = IDENTITY_DIRECTION_MAP;
    DirectionPairMap expected_edge_map{
        {{UP,BACK},{{UP,BACK},false}},
        {{DOWN,FRONT},{{DOWN,FRONT},false}},
        {{DOWN,BACK},{{DOWN,BACK},false}},
        {{DOWN,LEFT},{{DOWN,LEFT},false}},
        {{DOWN,RIGHT},{{DOWN,RIGHT},false}},
        {{FRONT,LEFT},{{FRONT,LEFT},false}},
        {{FRONT,RIGHT},{{FRONT,RIGHT},false}},
        {{BACK,LEFT},{{BACK,LEFT},false}},
        {{BACK,RIGHT},{{BACK,RIGHT},false}},
        {{UP,FRONT},{{UP,RIGHT},false}},
        {{UP,RIGHT},{{UP,LEFT},false}},
        {{UP,LEFT},{{UP,FRONT},false}}};
    DirectionTripleMap expected_corner_map = IDENTITY_TRIPLE_MAP;
    second_cube.cheat(expected_center_map, expected_edge_map, expected_corner_map);
    EXPECT_EQ(first_cube, second_cube);
    EXPECT_FALSE(first_cube.is_cheated());
    EXPECT_TRUE(second_cube.is_cheated());
}

}  // namespace cube::test

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}