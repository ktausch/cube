#ifndef DIRECTION_H
#define DIRECTION_H

#include <ostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <tuple>
#include <memory>
#include <random>
#include "TripleSwapInfo.h"

namespace cube
{
    /// Enumeration representing the three axes of a Rubik's cube.
    enum class Axis
    {
        /// axis containing UP and DOWN
        UPDOWN = 0,
        /// axis containing FRONT and BACK
        FRONTBACK = 1,
        /// axis containing LEFT and RIGHT
        LEFTRIGHT = 2
    };
    
    /// Vector containing all three axes
    static const std::vector<Axis>
        all_axes{Axis::UPDOWN,Axis::FRONTBACK,Axis::LEFTRIGHT};
    
    /// Class representing a Direction that specifies a face of a Rubik's cube.
    class Direction
    {
        private:
            /// the axis the direction lies on
            Axis axis_;
            /// true if and only if direction is one of UP, FRONT, LEFT
            bool first_;
        public:
            Direction(Axis axis, bool first);
            Direction(const Direction& direction);
            const Axis axis() const;
            const bool first() const;
            const bool operator==(const Direction& other) const;
            const bool operator!=(const Direction& other) const;
    };
    
    /**
     * Computes whether a cube can be rotated so that first, second, and third
     * are at the top, front, and left.
     * 
     * @param first the direction that would be top after rotation
     * @param second the direction that would be front after rotation
     * @param third the direction that would be left after rotation
     * 
     * @returns true if a cube can be rotated so that first, second, and third
     *          are at top, front, and left.
     */
    const bool is_valid_top_front_left_corner(const Direction& first,
        const Direction& second, const Direction& third);
    
    /**
     * Prints a direction to the given output stream.
     * 
     * @param out the output stream to print to
     * @param direction the Direction to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const Direction& direction);
    
    /// Hasher for the direction class
    class DirectionHash
    {
        public:
            /**
             * Hashes the given Direction.
             * 
             * @param direction Direction to hash
             * 
             * @returns unique hash for the Direction
             */
            size_t operator()(const Direction& direction) const;
    };
    
    /// DirectionHash to be used by DirectionPairHash and DirectionTripleHash
    static const DirectionHash direction_hasher;
    
    /// Static const direction variable representing up
    static const Direction UP{Axis::UPDOWN, true};
    /// Static const direction variable representing down
    static const Direction DOWN{Axis::UPDOWN, false};
    /// Static const direction variable representing front
    static const Direction FRONT{Axis::FRONTBACK, true};
    /// Static const direction variable representing back
    static const Direction BACK{Axis::FRONTBACK, false};
    /// Static const direction variable representing left
    static const Direction LEFT{Axis::LEFTRIGHT, true};
    /// Static const direction variable representing right
    static const Direction RIGHT{Axis::LEFTRIGHT, false};

    /// Set containing all of the directions
    static const std::unordered_set<Direction,DirectionHash>
        all_directions{UP,DOWN,FRONT,BACK,LEFT,RIGHT};
    
    /**
     * Maps from Direction to Direction will be used to represent orientation
     * transformations of the cube. It takes directions of centers to
     * directions of centers.
     */
    typedef std::unordered_map<Direction,Direction,DirectionHash> DirectionMap;
    
    /**
     * Determines whether orientation represents a valid orientation
     * transformation.
     * 
     * @param orientation map that maps before directions to after directions
     * 
     * @returns true if and only if the given map sends directions to
     *          consistent places
     */
    bool is_valid(const DirectionMap& orientation);
    
    /**
     * Inverts the given DirectionMap.
     * 
     * @param map the DirectionMap to invert
     * 
     * @returns the inverse DirectionMap (i.e. key->value and value->key)
     */
    DirectionMap operator~(const DirectionMap& map);

    /**
     * Concatenates the two DirectionMap objects.
     * 
     * @param first the first orientation map to apply
     * @param second the second orientation map to apply
     * 
     * @returns the DirectionMap resulting from applying second after first
     */
    DirectionMap operator+(
        const DirectionMap& first, const DirectionMap& second);
    
    /**
     * Prints a DirectionMap to an output stream.
     * 
     * @param out the output stream to print to
     * @param orientation_map the DirectionMap to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(
        std::ostream& out, const DirectionMap& orientation_map);
    
    /**
     * Finds a random valid orientation transformation.
     * 
     * @param random_number_generator Mersenne Twister random number generator
     * 
     * @returns random DirectionMap
     */
    DirectionMap random_orientation(std::mt19937_64& random_number_generator);
    
    /**
     * Finds a random valid orientation transformation.
     * 
     * @param seed the seed with which to construct a Mersenne Twister random
     *             number generator
     * 
     * @returns random DirectionMap
     */
    DirectionMap random_orientation(const unsigned long seed);
    
    /**
     * @brief Class representing an edge
     * 
     * Class representing a pair of directions that can represent an edge. The
     * first direction is guaranteed to be on an axis that is lower than the
     * second axis. UPDOWN < FRONTBACK < LEFTRIGHT
     */
    class DirectionPair
    {
        private:
            /// the Direction with the smaller axis variable
            Direction first_;
            /// the Direction with the larger axis variable
            Direction second_;
            
            /**
             * Checks if the directions passed in have a common axis (which
             * would make them invalid)
             */
            void check_if_has_common_axis();
            
            /**
             * Re-orders the DirectionPair so that first and second have sorted
             * axis variables.
             */
            bool reconcile();
        public:
            /**
             * Creates a new DirectionPair out of two Directions (which may be
             * swapped in reconciliation).
             * 
             * @param first a first direction, which may be swapped with the
             *              second after reconciliation
             * @param second a second direction, which may be swapped with the
             *               first after reconciliation
             */
            DirectionPair(Direction first, Direction second);
            
            /**
             * Creates a new DirectionPair out of two Directions (which may be
             * swapped in reconciliation).
             * 
             * @param first a first direction, which may be swapped with the
             *              second after reconciliation
             * @param second a second direction, which may be swapped with the
             *               first after reconciliation
             * @param required_swap reference to a bool in which to store
             *                      whether or not first and second needed to
             *                      be swapped in reconciliation
             */
            DirectionPair(
                Direction first, Direction second, bool& required_swap);
            
            /**
             * Constructs a new DirectionPair by copying the given one.
             * 
             * @param pair the DirectionPair to copy
             */
            DirectionPair(const DirectionPair& pair);
            
            /**
             * Finds a reference to the first Direction
             * 
             * @returns constant reference to the Direction with the lower axis
             */
            const Direction& first() const;
            
            /**
             * Finds a reference to the second Direction
             * 
             * @returns constant reference to the Direction with the higher axis
             */
            const Direction& second() const;
            
            /**
             * Checks if the first direction's axis is less than the second
             * direction's axis.
             * 
             * @returns true if the DirectionPair is well formed.
             */
            bool is_valid() const;
            
            /**
             * Checks for equality between two DirectionPair objects.
             * 
             * @param other the DirectionPair to check for equality with this
             * 
             * @returns true if and only if the first and second directions are
             *          the same for other and this
             */
            const bool operator==(const DirectionPair& other) const;
    };
    
    /**
     * Prints information about a DirectionPair to an output stream.
     * 
     * @param out the output stream to print to
     * @param pair the DirectionPair to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const DirectionPair& pair);
    
    /// Hasher for the DirectionPair class
    class DirectionPairHash
    {
        public:
            /**
             * Hashes a DirectionPair object.
             * 
             * @param pair the DirectionPair to hash
             * 
             * @returns the unique hash value of the given DirectionPair
             */
            size_t operator() (const DirectionPair& pair) const;
    };
    
    /**
     * Maps from DirectionPair to DirectionPair will be used to
     * 1) implement orientation transformations of edges, and
     * 2) refer to layer moves (i.e. the non-orientation moves of a cube
     * 
     * It maps DirectionPair->pair<DirectionPair,bool>. The bool represents
     * whether the directions in the pair had to be flipped (i.e. if the bool
     * is true, the first element of the key DirectionPair maps to the second
     * element of the value DirectionPair)
     */
    typedef std::unordered_map<DirectionPair,std::pair<DirectionPair,bool>,DirectionPairHash> DirectionPairMap;

    /**
     * Inverts a DirectionPairMap.
     * 
     * @param map the DirectionPairMap to invert
     * 
     * @returns the inverse DirectionPairMap
     */
    DirectionPairMap operator~(const DirectionPairMap& map);
    
    /**
     * Concatenates two DirectionPairMap objects.
     * 
     * @param first the first DirectionPairMap to apply
     * @param second the second DirectionPairMap to apply
     * 
     * @returns the result of applying second after applying first as a single
     *          DirectionPairMap
     */
    DirectionPairMap operator+(
        const DirectionPairMap& first, const DirectionPairMap& second);
    
    /// Static const set of all valid DirectionPair objects for iteration uses.
    static const std::unordered_set<DirectionPair,DirectionPairHash>
        all_direction_pairs{{UP,FRONT}, {UP,BACK}, {UP,LEFT},
                            {UP,RIGHT}, {DOWN,FRONT}, {DOWN,BACK},
                            {DOWN,LEFT}, {DOWN,RIGHT}, {FRONT,LEFT},
                            {FRONT,RIGHT}, {BACK,LEFT}, {BACK,RIGHT}};
    
    /**
     * Creates a DirectionPairMap that describes how edges move under an
     * orientation.
     * 
     * @param direction_map the DirectionMap describing the orientation
     * 
     * @returns the DirectionPairMap that describes how edges transform
     */
    DirectionPairMap pair_map_from_direction_map(const DirectionMap& direction_map);
    
    /**
     * @brief Class representing a corner
     * 
     * Class representing a triple of directions that can represent an edge.
     * The directions are guaranteed to be sorted by axis using
     * UPDOWN < FRONTBACK < LEFTRIGHT
     */
    class DirectionTriple
    {
        private:
            /// the first Direction, always one of {UP, DOWN}
            Direction first_;
            /// the second Direction, always one of {FRONT, BACK}
            Direction second_;
            /// the third Direction, always one of {LEFT, RIGHT}
            Direction third_;
            
            /**
             * Reconciles the current state of the triple so that the
             * Direction objects are in the correct order.
             * 
             * @returns the TripleSwapInfo required to order Direction objects
             *          correctly
             */
            TripleSwapInfo reconcile();
            
            /**
             * Checks if any of the directions has a common axis (which would
             * make it invalid).
             */
            void check_if_has_common_axis();
        public:
            /**
             * Creates a new DirectionTriple from its three directions (which
             * may be reordered in reconciliation).
             * 
             * @param first a first Direction
             * @param second a second Direction
             * @param third a third Direction
             */
            DirectionTriple(
                Direction first, Direction second, Direction third);
            
            /**
             * Creates a new DirectionTriple from its three directions (which
             * may be reordered in reconciliation).
             * 
             * @param first a first Direction
             * @param second a second Direction
             * @param third a third Direction
             * @param required_swaps reference to a TripleSwapInfo describing
             *                       the swaps that were required to order the
             *                       Direction objects passed
             */
            DirectionTriple(Direction first, Direction second, Direction third,
                TripleSwapInfo& required_swaps);
            
            /**
             * Constructs a DirectionTriple by copying the given one.
             * 
             * @param triple the DirectionTriple to copy
             */
            DirectionTriple(const DirectionTriple& triple);
            
            /**
             * Finds a reference to the first Direction
             * 
             * @returns constant reference to the Direction with the UPDOWN
             *          axis
             */
            const Direction& first() const;
            
            /**
             * Finds a reference to the second Direction
             * 
             * @returns constant reference to the Direction with the FRONTBACK
             *          axis
             */
            const Direction& second() const;
            
            /**
             * Finds a reference to the third Direction
             * 
             * @returns constant reference to the Direction with the LEFTRIGHT
             *          axis
             */
            const Direction& third() const;
            
            /**
             * Checks if the Direction objects stored in this triple form a
             * possible corner.
             */
            bool is_valid();
            
            /**
             * Gets a reference to the Direction on the given axis stored in
             * this DirectionTriple.
             * 
             * @param which the axis to query
             * 
             * @returns the Direction in this DirectionTriple corresponding to
             *          the given Axis 
             */
            const Direction& operator[](const Axis& which) const;
            
            /**
             * Checks for equality between two DirectionTriple objects.
             * 
             * @param other the other DirectionTriple to compare this to
             * 
             * @returns true if and only if other and this have the same
             *          Direction object
             */
            bool operator==(const DirectionTriple& other) const;
    };
    
    /**
     * Prints out information about the given triple.
     * 
     * @param out the output stream to print to
     * @param triple the DirectionTriple to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const DirectionTriple& triple);
    
    /**
     * Finds the corner that is between the two edges, if one exists.
     * 
     * @param pair1 the first edge
     * @param pair2 the second edge
     * 
     * @returns a pair whose first element is the DirectionTriple representing
     *          the corner between the two edges and whose second element is
     *          the Axis shared by the two edges
     */
    std::pair<DirectionTriple,Axis> between(
        const DirectionPair& pair1, const DirectionPair& pair2);
    
    /// Hasher for the DirectionTriple class
    class DirectionTripleHash
    {
        public:
            /**
             * Hashes the given DirectionTriple.
             * 
             * @param triple the DirectionTriple to hash
             * 
             * @returns the unique hash corresponding to the DirectionTriple
             */
            size_t operator()(const DirectionTriple& triple) const;
    };
    
    /**
     * Maps from DirectionTriple to DirectionTriple will be used to
     * 1) implement orientation transformations of corners, and
     * 2) implement effect layer movements of corners
     * 
     * It maps DirectionTriple->pair<DirectionPair,TripleSwapInfo>. The
     * TripleSwapInfo represents the swaps required to make the Direction
     * objects in the key DirectionTriple to line up with the ones they map to
     * in the value DirectionTriple.
     */
    typedef std::unordered_map<DirectionTriple,std::pair<DirectionTriple,TripleSwapInfo>,DirectionTripleHash> DirectionTripleMap;
    
    /**
     * Inverts the given DirectionTripleMap.
     * 
     * @param map the DirectionTripleMap to invert
     * 
     * @returns the inverse DirectionTripleMap
     */
    DirectionTripleMap operator~(const DirectionTripleMap& map);
    
    /**
     * Concanates two DirectionTripleMap objects
     * 
     * @param first the first DirectionTripleMap to apply
     * @param second the second DirectionTripleMap to apply
     * 
     * @returns the DirectionTripleMap describing the result of applying second
     *          after first
     */
    DirectionTripleMap operator+(
        const DirectionTripleMap& first, const DirectionTripleMap& second);
    
    /// Static const set of valid DirectionTriple objects for iteration uses.
    static const std::unordered_set<DirectionTriple,DirectionTripleHash>
        all_direction_triples{{UP,FRONT,LEFT}, {UP,FRONT,RIGHT},
                              {UP,BACK,LEFT}, {UP,BACK,RIGHT},
                              {DOWN,FRONT,LEFT}, {DOWN,FRONT,RIGHT},
                              {DOWN,BACK,LEFT}, {DOWN,BACK,RIGHT}};
    
    /**
     * Creates a DirectionTripleMap that describes how corners move under an
     * orientation.
     * 
     * @param direction_map the DirectionMap describing the orientation
     * 
     * @returns the DirectionTripleMap that describes how corners transform
     */
    DirectionTripleMap
        triple_map_from_direction_map(const DirectionMap& direction_map);

    /**
     * Creates a DirectionTripleMap that describes how corners move under a
     * layer movement.
     * 
     * @param pair_map the DirectionPairMap that describes how edges transform
     *                 under the layer movement.
     * 
     * @returns the DirectionTripleMap that describes how corners transform
     *          under the layer movement
     */
    DirectionTripleMap
        triple_map_from_pair_map(const DirectionPairMap& pair_map);
    
    /// DirectionMap that does nothing (i.e. the null rotation)
    static const DirectionMap
        IDENTITY_DIRECTION_MAP{{UP,UP}, {DOWN,DOWN}, {FRONT,FRONT},
                               {BACK,BACK}, {LEFT,LEFT}, {RIGHT,RIGHT}};
    
    /// DirectionMap describing orientation that keeps LEFT and RIGHT fixed and
    /// maps UP->FRONT
    static const DirectionMap
        TUMBLE_FORWARD{{UP,FRONT}, {FRONT,DOWN}, {DOWN,BACK},
                       {BACK,UP}, {LEFT,LEFT}, {RIGHT,RIGHT}};
    
    /// DirectionMap describing orientation that keeps LEFT and RIGHT fixed and
    /// maps UP->BACK
    static const DirectionMap TUMBLE_BACK = ~TUMBLE_FORWARD;
    
    /// DirectionMap describing orientation that keeps LEFT and RIGHT fixed and
    /// maps UP->DOWN
    static const DirectionMap FLIP_FORWARD = TUMBLE_FORWARD + TUMBLE_FORWARD;
    
    /// DirectionMap describing orientation that keeps FRONT and BACK fixed and
    /// maps UP->LEFT
    static const DirectionMap
        TUMBLE_LEFT{{UP,LEFT}, {LEFT,DOWN}, {DOWN,RIGHT},
                    {RIGHT,UP}, {FRONT,FRONT}, {BACK,BACK}};
    
    /// DirectionMap describing orientation that keeps FRONT and BACK fixed and
    /// maps UP->RIGHT
    static const DirectionMap TUMBLE_RIGHT = ~TUMBLE_LEFT;
    
    /// DirectionMap describing orientation that keeps FRONT and BACK fixed and
    /// maps UP->DOWN
    static const DirectionMap FLIP_LEFT = TUMBLE_LEFT + TUMBLE_LEFT;
    
    /// DirectionMap describing orientation that keeps UP and DOWN fixed and
    /// maps FRONT->LEFT
    static const DirectionMap
        SPIN_LEFT{{FRONT,LEFT}, {LEFT,BACK}, {BACK,RIGHT},
                  {RIGHT,FRONT}, {UP,UP}, {DOWN,DOWN}};
    
    /// DirectionMap describing orientation that keeps UP and DOWN fixed and
    /// maps FRONT->RIGHT
    static const DirectionMap SPIN_RIGHT = ~SPIN_LEFT;
    
    /// DirectionMap describing orientation that keeps UP and DOWN fixed and
    /// maps FRONT->BACK
    static const DirectionMap SPIN_TWICE = SPIN_LEFT + SPIN_LEFT;
    
    /// DirectionPairMap that describes no layer movement
    static const DirectionPairMap
        IDENTITY_PAIR_MAP{{{UP,FRONT},{{UP,FRONT},false}},
                          {{UP,BACK},{{UP,BACK},false}},
                          {{UP,LEFT},{{UP,LEFT},false}},
                          {{UP,RIGHT},{{UP,RIGHT},false}},
                          {{DOWN,FRONT},{{DOWN,FRONT},false}},
                          {{DOWN,BACK},{{DOWN,BACK},false}},
                          {{DOWN,LEFT},{{DOWN,LEFT},false}},
                          {{DOWN,RIGHT},{{DOWN,RIGHT},false}},
                          {{FRONT,LEFT},{{FRONT,LEFT},false}},
                          {{FRONT,RIGHT},{{FRONT,RIGHT},false}},
                          {{BACK,LEFT},{{BACK,LEFT},false}},
                          {{BACK,RIGHT},{{BACK,RIGHT},false}}};
    
    /// DirectionPairMap describing a clockwise move of the LEFT layer
    static const DirectionPairMap
        LEFT_CLOCKWISE{{{UP,FRONT},{{UP,FRONT},false}},
                       {{UP,BACK},{{UP,BACK},false}},
                       {{UP,RIGHT},{{UP,RIGHT},false}},
                       {{DOWN,FRONT},{{DOWN,FRONT},false}},
                       {{DOWN,BACK},{{DOWN,BACK},false}},
                       {{DOWN,RIGHT},{{DOWN,RIGHT},false}},
                       {{FRONT,RIGHT},{{FRONT,RIGHT},false}},
                       {{BACK,RIGHT},{{BACK,RIGHT},false}},
                       {{UP,LEFT},{{FRONT,LEFT},false}},
                       {{FRONT,LEFT},{{DOWN,LEFT},false}},
                       {{DOWN,LEFT},{{BACK,LEFT},false}},
                       {{BACK,LEFT},{{UP,LEFT},false}}};
    
    /// DirectionPairMap describing a counterclockwise move of the LEFT layer
    static const DirectionPairMap LEFT_COUNTER_CLOCKWISE = ~LEFT_CLOCKWISE;
    
    /// DirectionPairMap describing a double move of the LEFT layer
    static const DirectionPairMap LEFT_TWICE = LEFT_CLOCKWISE + LEFT_CLOCKWISE;
    
    /// DirectionPairMap describing a clockwise move of the RIGHT layer
    static const DirectionPairMap
        RIGHT_CLOCKWISE{{{UP,FRONT},{{UP,FRONT},false}},
                        {{UP,BACK},{{UP,BACK},false}},
                        {{UP,LEFT},{{UP,LEFT},false}},
                        {{DOWN,FRONT},{{DOWN,FRONT},false}},
                        {{DOWN,BACK},{{DOWN,BACK},false}},
                        {{DOWN,LEFT},{{DOWN,LEFT},false}},
                        {{FRONT,LEFT},{{FRONT,LEFT},false}},
                        {{BACK,LEFT},{{BACK,LEFT},false}},
                        {{UP,RIGHT},{{BACK,RIGHT},false}},
                        {{BACK,RIGHT},{{DOWN,RIGHT},false}},
                        {{DOWN,RIGHT},{{FRONT,RIGHT},false}},
                        {{FRONT,RIGHT},{{UP,RIGHT},false}}};

    /// DirectionPairMap describing a counterclockwise move of the RIGHT layer
    static const DirectionPairMap RIGHT_COUNTER_CLOCKWISE = ~RIGHT_CLOCKWISE;
    /// DirectionPairMap describing a double move of the RIGHT layer
    static const DirectionPairMap RIGHT_TWICE =
        RIGHT_CLOCKWISE + RIGHT_CLOCKWISE;
    
    /// DirectionPairMap describing a clockwise move of the FRONT layer
    static const DirectionPairMap
        FRONT_CLOCKWISE{{{UP,BACK},{{UP,BACK},false}},
                        {{UP,LEFT},{{UP,LEFT},false}},
                        {{UP,RIGHT},{{UP,RIGHT},false}},
                        {{DOWN,BACK},{{DOWN,BACK},false}},
                        {{DOWN,LEFT},{{DOWN,LEFT},false}},
                        {{DOWN,RIGHT},{{DOWN,RIGHT},false}},
                        {{BACK,LEFT},{{BACK,LEFT},false}},
                        {{BACK,RIGHT},{{BACK,RIGHT},false}},
                        {{UP,FRONT},{{FRONT,RIGHT},true}},
                        {{FRONT,RIGHT},{{DOWN,FRONT},true}},
                        {{DOWN,FRONT},{{FRONT,LEFT},true}},
                        {{FRONT,LEFT},{{FRONT,UP},true}}};
    
    /// DirectionPairMap describing a counterclockwise move of the FRONT layer
    static const DirectionPairMap FRONT_COUNTER_CLOCKWISE = ~FRONT_CLOCKWISE;
    
    /// DirectionPairMap describing a double move of the FRONT layer
    static const DirectionPairMap FRONT_TWICE =
        FRONT_CLOCKWISE + FRONT_CLOCKWISE;
    
    /// DirectionPairMap describing a clockwise move of the BACK layer
    static const DirectionPairMap
        BACK_CLOCKWISE{{{UP,FRONT},{{UP,FRONT},false}},
                       {{UP,LEFT},{{UP,LEFT},false}},
                       {{UP,RIGHT},{{UP,RIGHT},false}},
                       {{DOWN,FRONT},{{DOWN,FRONT},false}},
                       {{DOWN,LEFT},{{DOWN,LEFT},false}},
                       {{DOWN,RIGHT},{{DOWN,RIGHT},false}},
                       {{FRONT,LEFT},{{FRONT,LEFT},false}},
                       {{FRONT,RIGHT},{{FRONT,RIGHT},false}},
                       {{UP,BACK},{{BACK,LEFT},true}},
                       {{BACK,LEFT},{{DOWN,BACK},true}},
                       {{DOWN,BACK},{{BACK,RIGHT},true}},
                       {{BACK,RIGHT},{{UP,BACK},true}}};
    
    /// DirectionPairMap describing a counterclockwise move of the BACK layer
    static const DirectionPairMap BACK_COUNTER_CLOCKWISE = ~BACK_CLOCKWISE;
    
    /// DirectionPairMap describing a double move of the BACK layer
    static const DirectionPairMap BACK_TWICE = BACK_CLOCKWISE + BACK_CLOCKWISE;
    
    /// DirectionPairMap describing a clockwise move of the UP layer
    static const DirectionPairMap
        UP_CLOCKWISE{{{DOWN,FRONT},{{DOWN,FRONT},false}},
                     {{DOWN,BACK},{{DOWN,BACK},false}},
                     {{DOWN,LEFT},{{DOWN,LEFT},false}},
                     {{DOWN,RIGHT},{{DOWN,RIGHT},false}},
                     {{FRONT,LEFT},{{FRONT,LEFT},false}},
                     {{FRONT,RIGHT},{{FRONT,RIGHT},false}},
                     {{BACK,LEFT},{{BACK,LEFT},false}},
                     {{BACK,RIGHT},{{BACK,RIGHT},false}},
                     {{UP,FRONT},{{UP,LEFT},false}},
                     {{UP,LEFT},{{UP,BACK},false}},
                     {{UP,BACK},{{UP,RIGHT},false}},
                     {{UP,RIGHT},{{UP,FRONT},false}}};
    
    /// DirectionPairMap describing a counterclockwise move of the UP layer
    static const DirectionPairMap UP_COUNTER_CLOCKWISE = ~UP_CLOCKWISE;
    
    /// DirectionPairMap describing a double move of the UP layer
    static const DirectionPairMap UP_TWICE = UP_CLOCKWISE + UP_CLOCKWISE;
    
    /// DirectionPairMap describing a clockwise move of the DOWN layer
    static const DirectionPairMap
        DOWN_CLOCKWISE{{{UP,FRONT},{{UP,FRONT},false}},
                       {{UP,BACK},{{UP,BACK},false}},
                       {{UP,LEFT},{{UP,LEFT},false}},
                       {{UP,RIGHT},{{UP,RIGHT},false}},
                       {{FRONT,LEFT},{{FRONT,LEFT},false}},
                       {{FRONT,RIGHT},{{FRONT,RIGHT},false}},
                       {{BACK,LEFT},{{BACK,LEFT},false}},
                       {{BACK,RIGHT},{{BACK,RIGHT},false}},
                       {{DOWN,FRONT},{{DOWN,RIGHT},false}},
                       {{DOWN,RIGHT},{{DOWN,BACK},false}},
                       {{DOWN,BACK},{{DOWN,LEFT},false}},
                       {{DOWN,LEFT},{{DOWN,FRONT},false}}};
    
    /// DirectionPairMap describing a counterclockwise move of the DOWN layer
    static const DirectionPairMap DOWN_COUNTER_CLOCKWISE = ~DOWN_CLOCKWISE;

    /// DirectionPairMap describing a double move of the DOWN layer
    static const DirectionPairMap DOWN_TWICE = DOWN_CLOCKWISE + DOWN_CLOCKWISE;
    
    /// DirectionTripleMap describing no change to the corners
    static const DirectionTripleMap IDENTITY_TRIPLE_MAP{
        {{UP,FRONT,LEFT},{{UP,FRONT,LEFT},{false,false,false}}},
        {{UP,FRONT,RIGHT},{{UP,FRONT,RIGHT},{false,false,false}}},
        {{UP,BACK,LEFT},{{UP,BACK,LEFT},{false,false,false}}},
        {{UP,BACK,RIGHT},{{UP,BACK,RIGHT},{false,false,false}}},
        {{DOWN,FRONT,LEFT},{{DOWN,FRONT,LEFT},{false,false,false}}},
        {{DOWN,FRONT,RIGHT},{{DOWN,FRONT,RIGHT},{false,false,false}}},
        {{DOWN,BACK,LEFT},{{DOWN,BACK,LEFT},{false,false,false}}},
        {{DOWN,BACK,RIGHT},{{DOWN,BACK,RIGHT},{false,false,false}}}};
}

#endif
