#include <sstream>
#include <iostream>
#include "CubeSolver.h"

int main(int argc, char* argv[])
{
    const bool print_intermediates = true;
    const bool spell_out_first_layer = true;
    const bool spell_out_second_layer = true;
    const bool spell_out_third_layer = true;
    uint64_t seed;
    if (argc > 1)
    {
        std::istringstream stream{argv[1]};
        stream >> seed;
    }
    else
    {
        std::cout << "Please enter your desired seed:" << std::endl;
        std::cin >> seed;
    }
    cube::TrackedAlgorithm scramble = cube::random_tracked_algorithm(20, seed);
    std::cout << "(Scramble) " << scramble << std::endl;
    cube::CubeState initial_state{scramble};
    std::cout << cube::solve_step_by_step(initial_state, print_intermediates,
        spell_out_first_layer, spell_out_second_layer, spell_out_third_layer);
    return 0;
}
