#include "TripleSwapInfo.h"

namespace cube
{
    TripleSwapInfo operator~(const TripleSwapInfo& info)
    {
        return {std::get<2>(info), std::get<1>(info), std::get<0>(info)};
    }
    
    TripleSwapInfo operator*(
        const TripleSwapInfo& info1, const TripleSwapInfo& info2)
    {
        // same as abc,def
        if (std::get<2>(info1) == std::get<0>(info2))
        {
            // same as ab-,-ef
            return {std::get<0>(info1),
                std::get<1>(info1) != std::get<1>(info2), std::get<2>(info2)};
        }
        else if (std::get<1>(info1))
        {
            // same as a++,-ef
            return {std::get<1>(info2) != std::get<0>(info1), true, !std::get<2>(info2)};
        }
        else
        {
            // same as a-+,-ef
            return
                {!std::get<0>(info1), std::get<1>(info2), std::get<2>(info2)};
        }
    }
    
    std::ostream& operator<<(std::ostream& out, const TripleSwapInfo& info)
    {
        out << (std::get<0>(info) ? '+' : '-')
            << (std::get<1>(info) ? '+' : '-')
            << (std::get<2>(info) ? '+' : '-');
        return out;
    }
}