#ifndef MOVE_H
#define MOVE_H

#include <string>
#include <iostream>
#include <random>
#include "Direction.h"

namespace cube
{
    /**
     * @brief Class representing a layer move
     * 
     * A class that represents a layer move of a Rubik's cube (i.e. an atomic,
     * non-orientation transformation of the cube).
     */
    class Move
    {
        private:
            /// Direction describing the layer that will be twisted
            Direction layer_;
            /// bool describing whether the twist is clockwise (true) or
            /// counterclockwise (false)
            bool clockwise_;
            /// bool describing whether the twist is double (true) or single
            /// (false)
            bool twice_;
        public:
            /**
             * Constructs a new move from the layer and parameters of the turn.
             * 
             * @param layer the Direction pointing to the layer to twist
             * @param clockwise true if clockwise, false if counterclockwise
             * @param twice true if double, false if single
             */
            Move(Direction layer, bool clockwise, bool twice);
            
            /**
             * Constructs a new Move by copying an existing one.
             * 
             * @param move the Move to copy
             */
            Move(const Move& move);
            
            /**
             * Gets the Direction pointing at the layer to twist
             * 
             * @returns the Direction to the layer to twist
             */
            const Direction& layer() const;
            
            /**
             * Gets whether or not the turn is clockwise
             * 
             * @returns true if clockwise, false if counterclockwise
             */
            const bool clockwise() const;
            
            /**
             * Gets whether or not the turn is double
             * 
             * @returns true if double, false if single
             */
            const bool twice() const;
            
            /**
             * Finds the move representing the inverse of this move.
             * 
             * @returns the inverse move of this. It is on the same layer. If
             *          twice is true, the move is its own inverse. Otherwise,
             *          the inverse has a flipped clockwise parameter
             */
            Move inverse() const;
            
            /**
             * Mirrors the move about a given Axis.
             * 
             * @param axis the axis to mirror about
             * 
             * @returns Move with the mirrored direction (i.e. opposite
             *          direction if layer is on axis, same direction
             *          otherwise) and with an opposite clockwise parameter
             */
            Move mirror(const Axis& axis) const;
            
            /**
             * Makes a string version of this move.
             * 
             * @returns single character describing layer followed by "2" if
             *          doubled and "'" if not doubled but counterclockwise
             */
            std::string to_string() const;
            
            /**
             * Checks for equality between to Move objects.
             * 
             * @param other the Move to check for equality
             * 
             * @returns true if and only if other and this would generate the
             *          same DirectionPairMap
             */
            bool operator==(const Move& other) const;
    };
    
    /**
     * Prints out the given move.
     * 
     * @param out the output stream to print to
     * @param move the Move to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const Move& move);
    
    /**
     * Reads in a Move.
     * 
     * @param in the input stream to read from
     * @param move reference to set to the read Move
     * 
     * @returns reference to the input stream
     */
    std::istream& operator>>(std::istream& in, Move& move);
    
    /// Simple algorithms are defined via vectors of Move objects.
    typedef std::vector<Move> Algorithm;
    
    /**
     * Prints out the Algorithm.
     * 
     * @param out the output stream to print to
     * @param algorithm the Algorithm to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(std::ostream& out, const Algorithm& algorithm);
    
    /**
     * Reads in an Algorithm.
     * 
     * @param in the input stream to read from
     * @param algorithm reference to an algorithm to read in
     * 
     * @returns reference to the input stream
     */
    std::istream& operator>>(std::istream& in, Algorithm& algorithm);
    
    /**
     * Reads in an algorithm from a string.
     * 
     * @param string the string to read from
     * 
     * @returns the algorithm read from the string
     */
    Algorithm read_algorithm(const std::string& string);
    
    /**
     * Applies the given orientation to all moves in the given algorithm.
     * 
     * @param orientation_map the DirectionMap defining rotation of Algorithm
     * @param algorithm the Algorithm to rotate
     * 
     * @returns the rotated Algorithm
     */
    Algorithm operator*(const DirectionMap& orientation_map,
        const Algorithm& algorithm);
    
    /**
     * Concatenates the two Algorithm objects.
     * 
     * @param first the first Algorithm
     * @param second the second Algorithm
     * 
     * @returns the combined Algorithm
     */
    Algorithm operator+(const Algorithm& first, const Algorithm& second);
    
    /**
     * Reduces the given Algorithm by combining moves along a common Axis that
     * are not separated by moves along different Axis.
     * 
     * @param algorithm the Algorithm to reduce
     * 
     * @returns the reduced Algorithm
     */
    Algorithm reduce_algorithm(const Algorithm& algorithm);
    
    /**
     * Inverts the given Algorithm.
     * 
     * @param algorithm the Algorithm to invert
     * 
     * @returns the inverse Algorithm
     */
    Algorithm invert_algorithm(const Algorithm& algorithm);
    
    /**
     * Mirrors an Algorithm along a specific Axis.
     * 
     * @param algorithm the Algorithm to mirror
     * @param axis the Axis to mirror the algorithm about
     * 
     * @returns the mirrored Algorithm
     */
    Algorithm mirror_algorithm(const Algorithm& algorithm, const Axis& axis);
    
    /**
     * Finds a random algorithm of given length.
     * 
     * @param length the length of the algorithm to produce
     * @param random_number_generator Mersenne Twister generator to use
     * 
     * @returns the random Algorithm
     */
    Algorithm random_algorithm(const size_t length,
        std::mt19937_64& random_number_generator);
    
    /**
     * Finds a random algorithm of given length.
     * 
     * @param length the length of the algorithm to produce
     * @param seed unsigned long to seed the Mersenne Twister generator with
     * 
     * @returns the random Algorithm
     */
    Algorithm random_algorithm(const size_t length, const unsigned long seed);
    
    /**
     * @brief Class that simultaneously tracks algorithm and orientation
     *        informaotion
     * 
     * A class that stores an Algorithm alongside a DirectionMap that
     * represents how the orientation of the cube should change from start to
     * finish.
     */
    class TrackedAlgorithm
    {
        private:
            /// Algorithm in the starting orientation
            Algorithm algorithm_;
            /**
             * DirectionMap storing the orientation transformation to make
             * after applying the algorithm to get to the desired state
             */
            DirectionMap orientation_map_;
        public:
            /**
             * Constructs an empty TrackedAlgorithm representing the null
             * transformation.
             */
            TrackedAlgorithm();
            
            /**
             * Constructs a TrackedAlgorithm that represents the given
             * Algorithm with no orientation transformation.
             * 
             * @param algorithm the Algorithm object with which to construct
             *                  this TrackedAlgorithm
             */
            TrackedAlgorithm(const Algorithm& algorithm);
            
            /**
             * Constructs a TrackedAlgorithm that represents an orientation
             * transformation but no Algorithm.
             * 
             * @param orientation_map DirectionMap defining the transformation
             */
            TrackedAlgorithm(const DirectionMap& orientation_map);
            
            /**
             * Constructs a TrackedAlgorithm that represents an Algorithm
             * applied before an orientation transformation.
             * 
             * @param algorithm the Algorithm to apply in the starting
             *                  orientation
             * @param orientation_map the DirectionMap defining the orientation
             *                        transformation applied after algorithm
             */
            TrackedAlgorithm(const Algorithm& algorithm,
                const DirectionMap& orientation_map);
            
            /**
             * Constructs a TrackedAlgorithm by copying an existing one.
             * 
             * @param to_copy the TrackedAlgorithm to copy
             */
            TrackedAlgorithm(const TrackedAlgorithm& to_copy);
            
            /**
             * Gets the Algorithm to apply in the starting state.
             * 
             * @returns the Algorithm to apply in the starting state
             */
            const Algorithm& algorithm() const;
            
            /**
             * Gets the DirectionMap defining the orientation to apply after
             * the Algorithm.
             * 
             * @returns the DirectionMap defining the orientation to apply
             *          after the Algorithm
             */
            const DirectionMap& orientation_map() const;
            
            /**
             * Gets the size of the Algorithm at the heart of this
             * TrackedAlgorithm.
             * 
             * @returns the number of Move objects in this TrackedAlgorithm
             */
            size_t size() const;
            
            /**
             * Reduces the Algorithm stored inside this TrackedAlgorithm.
             */
            void reduce();
            
            /**
             * Copies this TrackedAlgorithm and extends this algorithm by the
             * given TrackedAlgorithm.
             * 
             * @param other the TrackedAlgorithm with which to extend the copy
             * 
             * @returns concatenation of this and other
             */
            TrackedAlgorithm operator+(const TrackedAlgorithm& other) const;
            
            /**
             * Extends this TrackedAlgorithm with the given TrackedAlgorithm.
             * 
             * @param other the TrackedAlgorithm with which to extend this
             * 
             * @returns reference to this TrackedAlgorithm after extension
             */
            TrackedAlgorithm& operator+=(const TrackedAlgorithm& other);
            
            /**
             * Appends a Move to this TrackedAlgorithm.
             * 
             * @param next the Move to add, assumed to apply in the ending
             *             state defined by orientation_map
             * 
             * @returns reference to this TrackedAlgorithm after appending
             */
            TrackedAlgorithm& operator+=(const Move& next);
            
            /**
             * Extends this TrackedAlgorithm by the given Algorithm.
             * 
             * @param next the algorithm with which to extent this, assumed to
             *             apply in the ending state of this TrackedAlgorithm
             * 
             * @returns reference to this TrackedAlgorithm after extension
             */
            TrackedAlgorithm& operator+=(const Algorithm& next);

            /**
             * Modifies the ending orientation of this TrackedAlgorithm by
             * adding on the given rotation.
             * 
             * @param rotation DirectionMap representing orientation
             *                 transformation to apply at the current end of
             *                 this TrackedAlgorithm
             * 
             * @returns reference to this TrackedAlgorithm after extension
             */
            TrackedAlgorithm& operator+=(const DirectionMap& rotation);
    };
    
    /**
     * Prints out the given TrackedAlgorithm by printing out its algorithm
     * followed by the DirectionMap defining its orientation transformation.
     * 
     * @param out the output stream to print to
     * @param algorithm the TrackedAlgorithm to print
     * 
     * @returns reference to the output stream
     */
    std::ostream& operator<<(
        std::ostream& out, const TrackedAlgorithm& algorithm);
    
    /**
     * Returns a reduced copy of the given TrackedAlgorithm.
     * 
     * @param algorithm TrackedAlgorithm to copy and reduce
     * 
     * @returns reduced copy of algorithm
     */
    TrackedAlgorithm reduce_tracked_algorithm(
        const TrackedAlgorithm& algorithm);
    
    /**
     * Finds a random TrackedAlgorithm of the given length by creating a random
     * Algorithm and following it by a random orientation transformation.
     * 
     * @param length the length of the TrackedAlgorithm to create
     * @param random_number_generator the Mersenne Twister generator to use
     * 
     * @returns the random TrackedAlgorithm
     */
    TrackedAlgorithm random_tracked_algorithm(const size_t length,
        std::mt19937_64& random_number_generator);
    
    /**
     * Finds a random TrackedAlgorithm of the given length by creating a random
     * Algorithm and following it by a random orientation transformation.
     * 
     * @param length the length of the TrackedAlgorithm to create
     * @param seed unsigned long with which to seed the Mersenne Twister random
     *             number generator
     * 
     * @returns the random TrackedAlgorithm
     */
    TrackedAlgorithm random_tracked_algorithm(const size_t length,
        const unsigned long seed);
    
    /// TrackedAlgorithm that moves the middle layer up
    static const TrackedAlgorithm MIDDLE_UP{
        {{RIGHT,false,false},{LEFT,true,false}},TUMBLE_BACK};
    /// TrackedAlgorithm that moves the middle layer down
    static const TrackedAlgorithm MIDDLE_DOWN{
        {{RIGHT,true,false},{LEFT,false,false}},TUMBLE_FORWARD};
    /// TrackedAlgorithm that moves the middle layer up twice
    static const TrackedAlgorithm MIDDLE_UP_TWICE{
        {{RIGHT,false,true},{LEFT,true,true}},FLIP_FORWARD};
    /// TrackedAlgorithm that moves the middle layer left
    static const TrackedAlgorithm MIDDLE_LEFT{
        {{UP,false,false},{DOWN,true,false}},SPIN_LEFT};
    /// TrackedAlgorithm that moves the middle layer right
    static const TrackedAlgorithm MIDDLE_RIGHT{
        {{UP,true,false},{DOWN,false,false}},SPIN_RIGHT};
    /// TrackedAlgorithm that moves the middle layer left twice
    static const TrackedAlgorithm MIDDLE_LEFT_TWICE{
        {{UP,false,true},{DOWN,true,true}},SPIN_TWICE};
    /// TrackedAlgorithm that moves the middle layer clockwise
    static const TrackedAlgorithm MIDDLE_CLOCKWISE{
        {{FRONT,false,false},{BACK,true,false}},TUMBLE_RIGHT};
    /// TrackedAlgorithm that moves the middle layer counterclockwise
    static const TrackedAlgorithm MIDDLE_COUNTER_CLOCKWISE{
        {{FRONT,true,false},{BACK,false,false}},TUMBLE_LEFT};
    /// TrackedAlgorithm that moves the middle layer clockwise twice
    static const TrackedAlgorithm MIDDLE_CLOCKWISE_TWICE{
        {{FRONT,false,true},{BACK,true,true}},FLIP_LEFT};
}

#endif
