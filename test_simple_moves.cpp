#include "CubeState.h"
#include "gtest/gtest.h"

namespace cube
{

class SimpleMoveTest : public ::testing::Test
{
    protected:
        CubeState cube{};
};

TEST_F(SimpleMoveTest, UndoneSolved)
{
    for (const auto& direction : all_directions)
    {
        cube.perform(Move(direction, false, false));
        cube.perform(Move(direction, true, false));
        EXPECT_TRUE(cube.is_solved());
        EXPECT_FALSE(cube.is_cheated());
    }
}

TEST_F(SimpleMoveTest, TwiceOwnInverse)
{
    for (const auto& direction : all_directions)
    {
        cube.perform(Move(direction, false, true));
        cube.perform(Move(direction, false, true));
        EXPECT_TRUE(cube.is_solved());
        EXPECT_FALSE(cube.is_cheated());
    }
}

TEST_F(SimpleMoveTest, TwoMinusOneMinusOneEqualsZero)
{
    for (const auto& direction : all_directions)
    {
        cube.perform(Move(direction, false, true));
        cube.perform(Move(direction, true, false));
        cube.perform(Move(direction, true, false));
        EXPECT_TRUE(cube.is_solved());
        EXPECT_FALSE(cube.is_cheated());
    }
}

TEST_F(SimpleMoveTest, TwoPlusOnePlusOneEqualsZeroModFour)
{
    for (const auto& direction : all_directions)
    {
        cube.perform(Move(direction, false, true));
        cube.perform(Move(direction, false, false));
        cube.perform(Move(direction, false, false));
        EXPECT_TRUE(cube.is_solved());
        EXPECT_FALSE(cube.is_cheated());
    }
}

TEST_F(SimpleMoveTest, UpMovesAsExpected)
{
    cube.perform(Move(UP, true, false));
    EXPECT_EQ(cube.center_color_scheme(), default_color_scheme);
    EdgeColorScheme expected_edge_coloring{
        {{UP,FRONT},{WHITE,GREEN}},
        {{UP,BACK},{WHITE,BLUE}},
        {{UP,LEFT},{WHITE,ORANGE}},
        {{UP,RIGHT},{WHITE,RED}},
        {{DOWN,FRONT},{YELLOW,ORANGE}},
        {{DOWN,BACK},{YELLOW,RED}},
        {{DOWN,LEFT},{YELLOW,BLUE}},
        {{DOWN,RIGHT},{YELLOW,GREEN}},
        {{FRONT,LEFT},{ORANGE,BLUE}},
        {{FRONT,RIGHT},{ORANGE,GREEN}},
        {{BACK,LEFT},{RED,BLUE}},
        {{BACK,RIGHT},{RED,GREEN}}};
    EXPECT_EQ(cube.edge_color_scheme(), expected_edge_coloring);
    CornerColorScheme expected_corner_coloring{
        {{UP,FRONT,LEFT},{WHITE,GREEN,ORANGE}},
        {{UP,FRONT,RIGHT},{WHITE,GREEN,RED}},
        {{UP,BACK,LEFT},{WHITE,BLUE,ORANGE}},
        {{UP,BACK,RIGHT},{WHITE,BLUE,RED}},
        {{DOWN,FRONT,LEFT},{YELLOW,ORANGE,BLUE}},
        {{DOWN,FRONT,RIGHT},{YELLOW,ORANGE,GREEN}},
        {{DOWN,BACK,LEFT},{YELLOW,RED,BLUE}},
        {{DOWN,BACK,RIGHT},{YELLOW,RED,GREEN}}};
    EXPECT_EQ(cube.corner_color_scheme(), expected_corner_coloring);
    EXPECT_FALSE(cube.is_cheated());
}

TEST_F(SimpleMoveTest, DownMovesAsExpected)
{
    cube.perform(Move(DOWN, true, false));
    EXPECT_EQ(cube.center_color_scheme(), default_color_scheme);
    EdgeColorScheme expected_edge_coloring{
        {{UP,FRONT},{WHITE,ORANGE}},
        {{UP,BACK},{WHITE,RED}},
        {{UP,LEFT},{WHITE,BLUE}},
        {{UP,RIGHT},{WHITE,GREEN}},
        {{DOWN,FRONT},{YELLOW,BLUE}},
        {{DOWN,BACK},{YELLOW,GREEN}},
        {{DOWN,LEFT},{YELLOW,RED}},
        {{DOWN,RIGHT},{YELLOW,ORANGE}},
        {{FRONT,LEFT},{ORANGE,BLUE}},
        {{FRONT,RIGHT},{ORANGE,GREEN}},
        {{BACK,LEFT},{RED,BLUE}},
        {{BACK,RIGHT},{RED,GREEN}}};
    EXPECT_EQ(cube.edge_color_scheme(), expected_edge_coloring);
    CornerColorScheme expected_corner_coloring{
        {{UP,FRONT,LEFT},{WHITE,ORANGE,BLUE}},
        {{UP,FRONT,RIGHT},{WHITE,ORANGE,GREEN}},
        {{UP,BACK,LEFT},{WHITE,RED,BLUE}},
        {{UP,BACK,RIGHT},{WHITE,RED,GREEN}},
        {{DOWN,FRONT,LEFT},{YELLOW,BLUE,RED}},
        {{DOWN,FRONT,RIGHT},{YELLOW,BLUE,ORANGE}},
        {{DOWN,BACK,LEFT},{YELLOW,GREEN,RED}},
        {{DOWN,BACK,RIGHT},{YELLOW,GREEN,ORANGE}}};
    EXPECT_EQ(cube.corner_color_scheme(), expected_corner_coloring);
    EXPECT_FALSE(cube.is_cheated());
}

TEST_F(SimpleMoveTest, FrontMovesAsExpected)
{
    cube.perform(Move(FRONT, true, false));
    EXPECT_EQ(cube.center_color_scheme(), default_color_scheme);
    EdgeColorScheme expected_edge_coloring{
        {{UP,FRONT},{BLUE,ORANGE}},
        {{UP,BACK},{WHITE,RED}},
        {{UP,LEFT},{WHITE,BLUE}},
        {{UP,RIGHT},{WHITE,GREEN}},
        {{DOWN,FRONT},{GREEN,ORANGE}},
        {{DOWN,BACK},{YELLOW,RED}},
        {{DOWN,LEFT},{YELLOW,BLUE}},
        {{DOWN,RIGHT},{YELLOW,GREEN}},
        {{FRONT,LEFT},{ORANGE,YELLOW}},
        {{FRONT,RIGHT},{ORANGE,WHITE}},
        {{BACK,LEFT},{RED,BLUE}},
        {{BACK,RIGHT},{RED,GREEN}}};
    EXPECT_EQ(cube.edge_color_scheme(), expected_edge_coloring);
    CornerColorScheme expected_corner_coloring{
        {{UP,FRONT,LEFT},{BLUE,ORANGE,YELLOW}},
        {{UP,FRONT,RIGHT},{BLUE,ORANGE,WHITE}},
        {{UP,BACK,LEFT},{WHITE,RED,BLUE}},
        {{UP,BACK,RIGHT},{WHITE,RED,GREEN}},
        {{DOWN,FRONT,LEFT},{GREEN,ORANGE,YELLOW}},
        {{DOWN,FRONT,RIGHT},{GREEN,ORANGE,WHITE}},
        {{DOWN,BACK,LEFT},{YELLOW,RED,BLUE}},
        {{DOWN,BACK,RIGHT},{YELLOW,RED,GREEN}}};
    EXPECT_EQ(cube.corner_color_scheme(), expected_corner_coloring);
    EXPECT_FALSE(cube.is_cheated());
}

TEST_F(SimpleMoveTest, BackMovesAsExpected)
{
    cube.perform(Move(BACK, true, false));
    EXPECT_EQ(cube.center_color_scheme(), default_color_scheme);
    EdgeColorScheme expected_edge_coloring{
        {{UP,FRONT},{WHITE,ORANGE}},
        {{UP,BACK},{GREEN,RED}},
        {{UP,LEFT},{WHITE,BLUE}},
        {{UP,RIGHT},{WHITE,GREEN}},
        {{DOWN,FRONT},{YELLOW,ORANGE}},
        {{DOWN,BACK},{BLUE,RED}},
        {{DOWN,LEFT},{YELLOW,BLUE}},
        {{DOWN,RIGHT},{YELLOW,GREEN}},
        {{FRONT,LEFT},{ORANGE,BLUE}},
        {{FRONT,RIGHT},{ORANGE,GREEN}},
        {{BACK,LEFT},{RED,WHITE}},
        {{BACK,RIGHT},{RED,YELLOW}}};
    EXPECT_EQ(cube.edge_color_scheme(), expected_edge_coloring);
    CornerColorScheme expected_corner_coloring{
        {{UP,FRONT,LEFT},{WHITE,ORANGE,BLUE}},
        {{UP,FRONT,RIGHT},{WHITE,ORANGE,GREEN}},
        {{UP,BACK,LEFT},{GREEN,RED,WHITE}},
        {{UP,BACK,RIGHT},{GREEN,RED,YELLOW}},
        {{DOWN,FRONT,LEFT},{YELLOW,ORANGE,BLUE}},
        {{DOWN,FRONT,RIGHT},{YELLOW,ORANGE,GREEN}},
        {{DOWN,BACK,LEFT},{BLUE,RED,WHITE}},
        {{DOWN,BACK,RIGHT},{BLUE,RED,YELLOW}}};
    EXPECT_EQ(cube.corner_color_scheme(), expected_corner_coloring);
    EXPECT_FALSE(cube.is_cheated());
}

TEST_F(SimpleMoveTest, LeftMovesAsExpected)
{
    cube.perform(Move(LEFT, true, false));
    EXPECT_EQ(cube.center_color_scheme(), default_color_scheme);
    EdgeColorScheme expected_edge_coloring{
        {{UP,FRONT},{WHITE,ORANGE}},
        {{UP,BACK},{WHITE,RED}},
        {{UP,LEFT},{RED,BLUE}},
        {{UP,RIGHT},{WHITE,GREEN}},
        {{DOWN,FRONT},{YELLOW,ORANGE}},
        {{DOWN,BACK},{YELLOW,RED}},
        {{DOWN,LEFT},{ORANGE,BLUE}},
        {{DOWN,RIGHT},{YELLOW,GREEN}},
        {{FRONT,LEFT},{WHITE,BLUE}},
        {{FRONT,RIGHT},{ORANGE,GREEN}},
        {{BACK,LEFT},{YELLOW,BLUE}},
        {{BACK,RIGHT},{RED,GREEN}}};
    EXPECT_EQ(cube.edge_color_scheme(), expected_edge_coloring);
    CornerColorScheme expected_corner_coloring{
        {{UP,FRONT,LEFT},{RED,WHITE,BLUE}},
        {{UP,FRONT,RIGHT},{WHITE,ORANGE,GREEN}},
        {{UP,BACK,LEFT},{RED,YELLOW,BLUE}},
        {{UP,BACK,RIGHT},{WHITE,RED,GREEN}},
        {{DOWN,FRONT,LEFT},{ORANGE,WHITE,BLUE}},
        {{DOWN,FRONT,RIGHT},{YELLOW,ORANGE,GREEN}},
        {{DOWN,BACK,LEFT},{ORANGE,YELLOW,BLUE}},
        {{DOWN,BACK,RIGHT},{YELLOW,RED,GREEN}}};
    EXPECT_EQ(cube.corner_color_scheme(), expected_corner_coloring);
    EXPECT_FALSE(cube.is_cheated());
}

TEST_F(SimpleMoveTest, RightMovesAsExpected)
{
    cube.perform(Move(RIGHT, true, false));
    EXPECT_EQ(cube.center_color_scheme(), default_color_scheme);
    EdgeColorScheme expected_edge_coloring{
        {{UP,FRONT},{WHITE,ORANGE}},
        {{UP,BACK},{WHITE,RED}},
        {{UP,LEFT},{WHITE,BLUE}},
        {{UP,RIGHT},{ORANGE,GREEN}},
        {{DOWN,FRONT},{YELLOW,ORANGE}},
        {{DOWN,BACK},{YELLOW,RED}},
        {{DOWN,LEFT},{YELLOW,BLUE}},
        {{DOWN,RIGHT},{RED,GREEN}},
        {{FRONT,LEFT},{ORANGE,BLUE}},
        {{FRONT,RIGHT},{YELLOW,GREEN}},
        {{BACK,LEFT},{RED,BLUE}},
        {{BACK,RIGHT},{WHITE,GREEN}}};
    EXPECT_EQ(cube.edge_color_scheme(), expected_edge_coloring);
    CornerColorScheme expected_corner_coloring{
        {{UP,FRONT,LEFT},{WHITE,ORANGE,BLUE}},
        {{UP,FRONT,RIGHT},{ORANGE,YELLOW,GREEN}},
        {{UP,BACK,LEFT},{WHITE,RED,BLUE}},
        {{UP,BACK,RIGHT},{ORANGE,WHITE,GREEN}},
        {{DOWN,FRONT,LEFT},{YELLOW,ORANGE,BLUE}},
        {{DOWN,FRONT,RIGHT},{RED,YELLOW,GREEN}},
        {{DOWN,BACK,LEFT},{YELLOW,RED,BLUE}},
        {{DOWN,BACK,RIGHT},{RED,WHITE,GREEN}}};
    EXPECT_EQ(cube.corner_color_scheme(), expected_corner_coloring);
    EXPECT_EQ(cube.to_string(),
        "   RRW   \n"
        "   RRW   \n"
        "   RRW   \n"
        "BBBWWOGGG\n"
        "BBBWWOGGG\n"
        "BBBWWOGGG\n"
        "   OOY   \n"
        "   OOY   \n"
        "   OOY   \n"
        "   YYR   \n"
        "   YYR   \n"
        "   YYR   \n");
    EXPECT_FALSE(cube.is_cheated());
}

}  // namespace cube::test

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}