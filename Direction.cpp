#include "Direction.h"
#include <iostream>

namespace cube
{
    
    Direction::Direction(Axis axis, bool first) : axis_{axis}, first_{first} {}
    
    Direction::Direction(const Direction& direction)
        : axis_{direction.axis_}, first_{direction.first_} {}
    
    const Axis Direction::axis() const { return axis_; }
    
    const bool Direction::first() const { return first_; }
    
    const bool Direction::operator==(const Direction& other) const
    {
        return ((axis_ == other.axis_) && (first_ == other.first_));
    }
    
    const bool Direction::operator!=(const Direction& other) const
    {
        return ((axis_ != other.axis_) || (first_ != other.first_));
    }
    
    std::ostream& operator<<(std::ostream& out, const Direction& direction)
    {
        switch(direction.axis())
        {
            case Axis::UPDOWN:
                out << (direction.first() ? "UP" : "DOWN");
                break;
            case Axis::FRONTBACK:
                out << (direction.first() ? "FRONT" : "BACK");
                break;
            default: //case Axis::LEFTRIGHT:
                out << (direction.first() ? "LEFT" : "RIGHT");
                break;
        }
        return out;
    }
    
    size_t DirectionHash::operator()(const Direction& direction) const
    {
        const size_t hash = static_cast<size_t>(direction.axis());
        if (direction.first())
        {
            return 5 - hash;
        }
        else
        {
            return hash;
        }
    }

    bool is_valid(const DirectionMap& orientation_map)
    {
        std::unordered_set<Axis> mapped_axes;
        for (const Axis& axis : all_axes)
        {
            const Direction from_second{axis, false};
            const Direction from_first{axis, true};
            
            const auto to_first_iterator = orientation_map.find(from_first);
            if (to_first_iterator == orientation_map.end())
            {
                return false;
            }
            const auto to_second_iterator = orientation_map.find(from_second);
            if (to_second_iterator == orientation_map.end())
            {
                return false;
            }
            const Direction& to_first = (*to_first_iterator).second;
            const Direction& to_second = (*to_second_iterator).second;
            if (to_first.axis() != to_second.axis())
            {
                return false;
            }
            if (to_first == to_second)
            {
                return false;
            }
            mapped_axes.insert(to_first.axis());
        }
        if (mapped_axes.size() != 3)
        {
            return false;
        }
        return is_valid_top_front_left_corner(orientation_map.at(UP),
            orientation_map.at(FRONT), orientation_map.at(LEFT));
    }
    
    DirectionMap operator~(const DirectionMap& map)
    {
        DirectionMap inverse;
        for (const auto& [from, to] : map)
        {
            inverse.emplace(to, from);
        }
        return inverse;
    }

    DirectionMap operator+(const DirectionMap& first, const DirectionMap& second)
    {
        DirectionMap sum;
        for (const auto& [from, middle] : first)
        {
            sum.emplace(from, second.at(middle));
        }
        return sum;
    }
    
    std::ostream& operator<<(std::ostream& out, const DirectionMap& orientation_map)
    {
        out << "(UP,FRONT)->(" << orientation_map.at(UP) << ','
            << orientation_map.at(FRONT) << ')';
        return out;
    }
    
    DirectionMap random_orientation(std::mt19937_64& random_number_generator)
    {
        std::uniform_int_distribution<short> tumble_distribution{0, 5};
        std::uniform_int_distribution<short> spin_distribution{0, 3};
        short tumble_value = tumble_distribution(random_number_generator);
        short spin_value = spin_distribution(random_number_generator);
        DirectionMap tumble_map;
        switch (tumble_value)
        {
            case 0:
                tumble_map = IDENTITY_DIRECTION_MAP;
                break;
            case 1:
                tumble_map = TUMBLE_FORWARD;
                break;
            case 2:
                tumble_map = TUMBLE_BACK;
                break;
            case 3:
                tumble_map = TUMBLE_LEFT;
                break;
            case 4:
                tumble_map = TUMBLE_RIGHT;
                break;
            case 5:
                tumble_map = FLIP_FORWARD;
                break;
        }
        DirectionMap spin_map;
        switch (spin_value)
        {
            case 0:
                spin_map = IDENTITY_DIRECTION_MAP;
                break;
            case 1:
                spin_map = SPIN_LEFT;
                break;
            case 2:
                spin_map = SPIN_RIGHT;
                break;
            case 3:
                spin_map = SPIN_TWICE;
                break;
        }
        return tumble_map + spin_map;
    }
    
    DirectionMap random_orientation(const unsigned long seed)
    {
        std::mt19937_64 random_number_generator{seed};
        return random_orientation(random_number_generator);
    }
    
    bool DirectionPair::reconcile()
    {
        if (first_.axis() > second_.axis())
        {
            std::swap(first_, second_);
            return true;
        }
        else
        {
            return false;
        }
    }

    void DirectionPair::check_if_has_common_axis()
    {
        if (first_.axis() == second_.axis())
        {
            throw std::invalid_argument("A DirectionPair cannot store "
                "Direction objects on the same axis.");
        }
    }
    
    DirectionPair::DirectionPair(Direction first, Direction second)
        : first_{first}, second_{second}
    {
        check_if_has_common_axis();
        reconcile();
    }
    
    DirectionPair::DirectionPair(Direction first, Direction second, bool& required_swap)
        : first_{first}, second_{second}
    {
        check_if_has_common_axis();
        required_swap = reconcile();
    }
    
    DirectionPair::DirectionPair(const DirectionPair& pair)
        : first_{pair.first_}, second_{pair.second_} {}
    
    const Direction& DirectionPair::first() const {return first_;}
    
    const Direction& DirectionPair::second() const {return second_;}
    
    bool DirectionPair::is_valid() const
    {
        return first().axis() < second().axis();
    }
    
    const bool DirectionPair::operator==(const DirectionPair& other) const
    {
        return ((first() == other.first()) && (second() == other.second()));
    }
    
    std::ostream& operator<<(std::ostream& out, const DirectionPair& pair)
    {
        out << '(' << pair.first() << ',' << pair.second() << ')';
        return out;
    }
    
    size_t DirectionPairHash::operator() (const DirectionPair& pair) const
    {
        const size_t first_hash = direction_hasher(pair.first());
        const size_t second_hash = direction_hasher(pair.second());
        return (first_hash * 6) + second_hash;
    }
    
    DirectionPairMap pair_map_from_direction_map(const DirectionMap& direction_map)
    {
        DirectionPairMap direction_pair_map;
        for (const DirectionPair& before_direction_pair : all_direction_pairs)
        {
            const Direction after_first{direction_map.at(before_direction_pair.first())};
            const Direction after_second{direction_map.at(before_direction_pair.second())};
            bool required_swap;
            const DirectionPair after_direction_pair{after_first, after_second, required_swap};
            direction_pair_map.emplace(before_direction_pair,
                std::pair<DirectionPair,bool>{after_direction_pair, required_swap});
        }
        return direction_pair_map;
    }
    
    DirectionPairMap operator~(const DirectionPairMap& map)
    {
        DirectionPairMap inverse;
        for (const auto& [from, to_and_swap] : map)
        {
            const auto& [to, swap] = to_and_swap;
            const auto from_and_swap = std::make_pair(from, swap);
            inverse.emplace(to, from_and_swap);
        }
        return inverse;
    }
    
    DirectionPairMap operator+(const DirectionPairMap& first, const DirectionPairMap& second)
    {
        DirectionPairMap sum;
        for (const auto& [from, middle_and_first_swap] : first)
        {
            const auto& [middle, first_swap] = middle_and_first_swap;
            const auto& [to, second_swap] = second.at(middle);
            const auto to_and_full_swap = std::make_pair(to,first_swap!=second_swap);
            sum.emplace(from, to_and_full_swap);
        }
        return sum;
    }
    
    TripleSwapInfo DirectionTriple::reconcile()
    {
        bool first_swap_required, second_swap_required, third_swap_required;
        first_swap_required = (first_.axis() > second_.axis());
        if (first_swap_required)
        {
            std::swap(first_, second_);
        }
        second_swap_required = (second_.axis() > third_.axis());
        if (second_swap_required)
        {
            std::swap(second_, third_);
        }
        third_swap_required = (first_.axis() > second_.axis());
        if (first_.axis() > second_.axis())
        {
            std::swap(first_, second_);
        }
        return
            {first_swap_required, second_swap_required, third_swap_required};
    }

    void DirectionTriple::check_if_has_common_axis()
    {
        if ((first_.axis() == second_.axis()) ||
            (second_.axis() == third_.axis()) ||
            (third_.axis() == first_.axis()))
        {
            throw std::invalid_argument(
                "No two directions of a triple can be the same.");
        }
    }

    DirectionTriple::DirectionTriple(
        Direction first, Direction second, Direction third)
        : first_{first}, second_{second}, third_{third}
    {
        check_if_has_common_axis();
        reconcile();
    }

    DirectionTriple::DirectionTriple(Direction first, Direction second,
        Direction third, TripleSwapInfo& required_swaps)
        : first_{first}, second_{second}, third_{third}
    {
        check_if_has_common_axis();
        required_swaps = reconcile();
    }
    
    DirectionTriple::DirectionTriple(const DirectionTriple& triple)
        : first_{triple.first_}, second_{triple.second_}, third_{triple.third_} {}
    
    const Direction& DirectionTriple::first() const {return first_;}
    
    const Direction& DirectionTriple::second() const {return second_;}
    
    const Direction& DirectionTriple::third() const {return third_;}
    
    bool DirectionTriple::is_valid()
    {
        return (
            (first().axis() == Axis::UPDOWN) &&
            (second().axis() == Axis::FRONTBACK) &&
            (third().axis() == Axis::LEFTRIGHT));
    }

    const Direction& DirectionTriple::operator[](const Axis& which) const
    {
        switch (which)
        {
            case Axis::UPDOWN:
                return first_;
                break;
            case Axis::FRONTBACK:
                return second_;
                break;
            default: //case Axis::LEFTRIGHT:
                return third_;
                break;
        }
    }
    
    bool DirectionTriple::operator==(const DirectionTriple& other) const
    {
        return (
            (first() == other.first()) &&
            (second() == other.second()) &&
            third() == other.third());
    }

    const bool is_valid_top_front_left_corner(const Direction& first,
        const Direction& second, const Direction& third)
    {
        TripleSwapInfo swap_info;
        std::tuple<bool,bool,bool> first_axis;
        try
        {
            DirectionTriple triple{first, second, third, swap_info};
            first_axis = std::make_tuple(triple.first().first(),
                triple.second().first(), triple.third().first());
        }
        catch(const std::invalid_argument& error)
        {
            return false;
        }
        bool valid{true};
        if (std::get<0>(swap_info)) {valid = (!valid);}
        if (std::get<1>(swap_info)) {valid = (!valid);}
        if (std::get<2>(swap_info)) {valid = (!valid);}
        if (!std::get<0>(first_axis)) {valid = (!valid);}
        if (!std::get<1>(first_axis)) {valid = (!valid);}
        if (!std::get<2>(first_axis)) {valid = (!valid);}
        return valid;
    }
    
    std::ostream& operator<<(std::ostream& out, const DirectionTriple& triple)
    {
        out << '(' << triple.first() << ','
            << triple.second() << ','
            << triple.third() << ')';
        return out;
    }
    
    std::pair<DirectionTriple,Axis> between(
        const DirectionPair& pair1, const DirectionPair& pair2)
    {
        // it's granted that (pair.first() != pair.second())
        bool first_first = (pair1.first() == pair2.first());
        bool first_second = (pair1.first() == pair2.second());
        bool second_first = (pair1.second() == pair2.first());
        bool second_second = (pair1.second() == pair2.second());
        int num_pairs_equal = (((int)first_first) + ((int)first_second) +
            ((int)second_first) + ((int)second_second));
        if (num_pairs_equal != 1)
        {
            throw std::invalid_argument{"There is no corner between the two "
                "edges because they don't share a direction."};
        }
        // exactly one of the bools defined above is true

        std::unique_ptr<Direction> shared_direction{nullptr};
        std::unique_ptr<std::pair<Direction,Direction>> other_directions{nullptr};
        if (first_first)
        {
            shared_direction = std::make_unique<Direction>(pair1.first());
            other_directions = std::make_unique<std::pair<Direction,Direction>>(
                pair1.second(), pair2.second());
        }
        else if (first_second)
        {
            shared_direction = std::make_unique<Direction>(pair1.first());
            other_directions = std::make_unique<std::pair<Direction,Direction>>(
                pair1.second(), pair2.first());
        }
        else if (second_first)
        {
            shared_direction = std::make_unique<Direction>(pair1.second());
            other_directions = std::make_unique<std::pair<Direction,Direction>>(
                pair1.first(), pair2.second());
        }
        else // second_second
        {
            shared_direction = std::make_unique<Direction>(pair1.second());
            other_directions = std::make_unique<std::pair<Direction,Direction>>(
                pair1.first(), pair2.first());
        }
        return {
            {*shared_direction, other_directions->first, other_directions->second},
            shared_direction->axis()};
    }
    
    size_t DirectionTripleHash::operator()(const DirectionTriple& triple) const
    {
        const size_t first_hash = direction_hasher(triple.first());
        const size_t second_hash = direction_hasher(triple.second());
        const size_t third_hash = direction_hasher(triple.third());
        return (((first_hash * 6) + second_hash) * 6) + third_hash;
    }
    
    DirectionTripleMap operator~(const DirectionTripleMap& map)
    {
        DirectionTripleMap inverse;
        for (const auto& [from, to_and_swap_info] : map)
        {
            const auto& [to, swap_info] = to_and_swap_info;
            const auto from_and_swap = std::make_pair(from, ~swap_info);
            inverse.emplace(to, from_and_swap);
        }
        return inverse;
    }
    
    DirectionTripleMap operator+(const DirectionTripleMap& first, const DirectionTripleMap& second)
    {
        DirectionTripleMap sum;
        for (const auto& [from, middle_and_first_swap_info] : first)
        {
            const auto& [middle, first_swap_info] = middle_and_first_swap_info;
            const auto& [to, second_swap_info] = second.at(middle);
            const auto to_and_full_swap_info =
                std::make_pair(to, first_swap_info * second_swap_info);
            sum.emplace(from, to_and_full_swap_info);
        }
        return sum;
    }
    
    DirectionTripleMap triple_map_from_direction_map(const DirectionMap& direction_map)
    {
        DirectionTripleMap direction_triple_map;
        for (const DirectionTriple& before_direction_triple : all_direction_triples)
        {
            const Direction
                after_first{direction_map.at(before_direction_triple.first())};
            const Direction
                after_second{direction_map.at(before_direction_triple.second())};
            const Direction
                after_third{direction_map.at(before_direction_triple.third())};
            TripleSwapInfo required_swaps;
            const DirectionTriple after_direction_triple{
                after_first, after_second, after_third, required_swaps};
            direction_triple_map.emplace(before_direction_triple,
                std::make_pair(after_direction_triple, required_swaps));
        }
        return direction_triple_map;
    }

    DirectionTripleMap triple_map_from_pair_map(const DirectionPairMap& pair_map)
    {
        DirectionTripleMap triple_map;
        for (auto it1 = pair_map.begin(); it1 != pair_map.end(); it1++)
        {
            const DirectionPair& from1 = it1->first;
            const std::pair<DirectionPair,bool>& to_with_pair_swap1 = it1->second;
            const DirectionPair& to1 = to_with_pair_swap1.first;
            const bool pair_swap1 = to_with_pair_swap1.second;
            const bool from1_to1_equal = (from1 == to1);
            // start at the current iterator so that we only traverse each pair
            // of pairs once.
            for (auto it2 = it1; it2 != pair_map.end(); it2++)
            {
                if (it1 == it2)
                {
                    // one edge can't specify a corner, so we can skip this case
                    continue;
                }
                const DirectionPair& from2 = it2->first;
                const std::pair<DirectionPair,bool>& to_with_pair_swap2 = it2->second;
                const DirectionPair& to2 = to_with_pair_swap2.first;
                if (from1_to1_equal != (from2 == to2))
                {
                    // this corner can only be affected here if the two edges
                    // involved either both move or both stay fixed
                    continue;
                }
                const bool pair_swap2 = to_with_pair_swap2.second;
                std::unique_ptr<std::pair<DirectionTriple,Axis>> between_from{nullptr};
                try
                {
                    between_from =
                        std::make_unique<std::pair<DirectionTriple,Axis>>(between(from1,from2));
                }
                catch(const std::invalid_argument& e)
                {
                    // these two edges did not have a corner between them
                    // before the pairing, so even if they move together, they
                    // do not share a corner that needs a mapping
                    continue;
                }
                const DirectionTriple& from_corner = between_from->first;
                const Axis& from_shared_axis = between_from->second;
                std::unique_ptr<std::pair<DirectionTriple,Axis>> between_to{nullptr};
                between_to = std::make_unique<std::pair<DirectionTriple,Axis>>(between(to1,to2));
                /*
                try
                {
                    between_to =
                        std::make_unique<std::pair<DirectionTriple,Axis>>(between(to1, to2));
                }
                catch(const std::invalid_argument& e)
                {
                    // this means that the two edges which were one corner from
                    // each other before the pairing were more than one corner
                    // apart after the pairing, implying that the edges didn't
                    // move together.
                    continue;
                }
                */
                const DirectionTriple& to_corner = between_to->first;
                const Axis& to_shared_axis = between_to->second;
                if (from_shared_axis != to_shared_axis)
                {
                    // this means one of the edges moved but one of them stayed
                    // fixed. Since we are interested only in corners between
                    // two moving edges, we can skip this case.
                    continue;
                }
                bool first_swap, second_swap, third_swap;
                switch (from_shared_axis)
                {
                    case Axis::UPDOWN:
                        // from1.first().axis(), from2.first().axis(),
                        // to1.first().axis(), to2.first().axis() are all either
                        // UP or DOWN, so first direction of triple is correct
                        first_swap = false;
                        second_swap =
                            (from1.second().axis() != to1.second().axis());
                        third_swap = false;
                        break;
                    case Axis::FRONTBACK:
                        // from_corner.second().axis() == to_corner.second().axis()
                        // so the only possible swap info is whether or not first
                        // and third axes should be swapped. This corresponds to
                        // false,false,false or true,true,true
                        first_swap = pair_swap1;
                        second_swap = pair_swap1;
                        third_swap = pair_swap1;
                        break;
                    case Axis::LEFTRIGHT:
                        // from1.second().axis(), from2.second().axis(),
                        // to1.second().axis(), to2.second().axis() are all either
                        // LEFT or RIGHT, so last direction of triple is correct
                        first_swap =
                            (from1.first().axis() != to1.first().axis());
                        second_swap = false;
                        third_swap = false;
                        break;
                }
                TripleSwapInfo corner_swap_info =
                    std::make_tuple(first_swap, second_swap, third_swap);
                std::pair<DirectionTriple,TripleSwapInfo>
                    to_corner_with_swap_info{to_corner, corner_swap_info};
                triple_map.emplace(from_corner, to_corner_with_swap_info);
            }
        }
        return triple_map;
    }
}