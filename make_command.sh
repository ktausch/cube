#!/bin/bash

PERFORM_TESTS="YES"
while [ $# -gt 0 ]
do
    key="$1"
    case $key in
        --no-tests)
            PERFORM_TESTS="NO"
            shift
            ;;
        *)
            echo "Unrecognized option"
            exit 1
            ;;
    esac
done

if [ -d build ]
then
    rm -r build
fi

if [ -f "build_error.txt" ]
then
    rm build_error.txt
fi

cmake -S . -B build 2> build_error.txt
if [ ! -s "build_error.txt" ]
then
    rm build_error.txt
fi

pushd build > /dev/null

if [ -f "make_error.txt" ]
then
    rm make_error.txt
fi

make 2> make_error.txt

if [ ! -s "make_error.txt" ]
then
    rm make_error.txt
    if [ "$PERFORM_TESTS" == "YES" ]
    then
        for SCRIPT in "triple_swap" "no_moves" "simple_moves" "algorithms" "cube_solver"
        do
            ./test_${SCRIPT}
        done
    else
        echo "The make ran successfully!"
    fi
fi
popd > /dev/null
