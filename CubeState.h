#ifndef CUBESTATE_H
#define CUBESTATE_H

#include <memory>
#include <vector>
#include <unordered_map>
#include "Direction.h"
#include "Color.h"
#include "Move.h"

namespace cube
{
    /// Direction -> Color maps represent a way to color centers
    typedef std::unordered_map<Direction,Color,DirectionHash> CenterColorScheme;
    
    /// DirectionPair -> ColorPair maps represent a way to color edges
    typedef std::unordered_map<DirectionPair,ColorPair,DirectionPairHash> EdgeColorScheme;
    
    /// DirectionTriple -> ColorTriple maps represents a way to color corners
    typedef std::unordered_map<DirectionTriple,ColorTriple,DirectionTripleHash> CornerColorScheme;
    
    /// color scheme representing default orientation (white top, orange front)
    static const CenterColorScheme
        default_color_scheme{{UP,WHITE},{DOWN,YELLOW},{FRONT,ORANGE},
                             {BACK,RED},{LEFT,BLUE},{RIGHT,GREEN}};
    
    /// edge color scheme of solved cube in default orientation
    static const EdgeColorScheme
        solved_edge_color_scheme{{{UP,FRONT},{WHITE,ORANGE}},
                                 {{UP,BACK},{WHITE,RED}},
                                 {{UP,LEFT},{WHITE,BLUE}},
                                 {{UP,RIGHT},{WHITE,GREEN}},
                                 {{DOWN,FRONT},{YELLOW,ORANGE}},
                                 {{DOWN,BACK},{YELLOW,RED}},
                                 {{DOWN,LEFT},{YELLOW,BLUE}},
                                 {{DOWN,RIGHT},{YELLOW,GREEN}},
                                 {{FRONT,LEFT},{ORANGE,BLUE}},
                                 {{FRONT,RIGHT},{ORANGE,GREEN}},
                                 {{BACK,LEFT},{RED,BLUE}},
                                 {{BACK,RIGHT},{RED,GREEN}}};
    
    /// corner color scheme of solved cube in default orientation
    static const CornerColorScheme
        solved_corner_color_scheme{{{UP,FRONT,LEFT},{WHITE,ORANGE,BLUE}},
                                   {{UP,FRONT,RIGHT},{WHITE,ORANGE,GREEN}},
                                   {{UP,BACK,LEFT},{WHITE,RED,BLUE}},
                                   {{UP,BACK,RIGHT},{WHITE,RED,GREEN}},
                                   {{DOWN,FRONT,LEFT},{YELLOW,ORANGE,BLUE}},
                                   {{DOWN,FRONT,RIGHT},{YELLOW,ORANGE,GREEN}},
                                   {{DOWN,BACK,LEFT},{YELLOW,RED,BLUE}},
                                   {{DOWN,BACK,RIGHT},{YELLOW,RED,GREEN}}};
    
    /**
     * Maps Direction objects representing centers to the index of the Color
     * (i.e. how many Color values are printed before that center).
     */
    typedef std::unordered_map<Direction,short,DirectionHash> CenterIndexMap;
    
    /**
     * Maps DirectionPair objects representing edges to the indices of the
     * Color objects (i.e. how many Color values are printed before that each
     * edge direction).
     */
    typedef std::unordered_map<DirectionPair,std::pair<short,short>,DirectionPairHash> EdgeIndexMap;
    
    /**
     * Maps DirectionTriple objects representing corners to the indices of the
     * Color objects (i.e. how many Color values are printed before that each
     * corner direction).
     */
    typedef std::unordered_map<DirectionTriple,std::tuple<short,short,short>,DirectionTripleHash> CornerIndexMap;
    
    /// Static map to use that gets the index of a center Color
    static const CenterIndexMap
        to_string_center_indices{{UP,22}, {DOWN,49}, {FRONT,40},
                                 {BACK,4}, {LEFT,19}, {RIGHT,25}};
    
    /// Static map to use that gets the indices of each edge Color
    static const EdgeIndexMap
        to_string_edge_indices{{{UP,FRONT},{31,37}},{{UP,BACK},{13,7}},
                               {{DOWN,FRONT},{46,43}}, {{DOWN,BACK},{52,1}},
                               {{UP,LEFT},{21,20}}, {{UP,RIGHT},{23,24}},
                               {{DOWN,LEFT},{48,18}}, {{DOWN,RIGHT},{50,26}},
                               {{FRONT,LEFT},{39,28}}, {{FRONT,RIGHT},{41,34}},
                               {{BACK,LEFT},{3,10}}, {{BACK,RIGHT},{5,16}}};
    
    /// Static map to use that gets the indices of each corner Color
    static const CornerIndexMap
        to_string_corner_indices{{{UP,FRONT,LEFT},{30,36,29}},
                                 {{UP,FRONT,RIGHT},{32,38,33}},
                                 {{UP,BACK,LEFT},{12,6,11}},
                                 {{UP,BACK,RIGHT},{14,8,15}},
                                 {{DOWN,FRONT,LEFT},{45,42,27}},
                                 {{DOWN,FRONT,RIGHT},{47,44,35}},
                                 {{DOWN,BACK,LEFT},{51,0,9}},
                                 {{DOWN,BACK,RIGHT},{53,2,17}}};
    
    /**
     * @brief Class representing the current state Rubik's cube
     * 
     * A class representing a Rubik's cube in any state of orientation or layer
     * shuffling.
     */
    class CubeState
    {
        private:
            /// bool describing whether things other than orientation and
            /// layers have been applied to this CubeState.
            bool cheated;
            
            /// bool describing whether this CubeState was set via a scramble
            /// or from reading raw colors
            bool guaranteed_solvable;
            
            /// pointer to the coloring of the centers
            std::unique_ptr<CenterColorScheme> centers{nullptr};
            /// pointer to the coloring of the edges
            std::unique_ptr<EdgeColorScheme> edges{nullptr};
            /// pointer to the coloring of the corners
            std::unique_ptr<CornerColorScheme> corners{nullptr};
            
            /// Initializes the center coloring in the solved state
            void initialize_centers();
            /// Initializes the edge coloring in the solved state
            void initialize_edges();
            /// Initializes the corner coloring in the solved state
            void initialize_corners();

            /**
             * Applies a center map, usually representing a rotation.
             * 
             * @param center_map DirectionMap describing how centers transform
             */
            void apply_center_map(const DirectionMap& center_map);

            /**
             * Applies a pair map, representing either a rotation or a Move.
             * 
             * @param edge_map DirectionPairMap describing how edges transform
             */
            void apply_edge_map(const DirectionPairMap& edge_map);
            
            /**
             * Applies a corner map, representing either a cheat, rotation, or
             * Move.
             * 
             * @param corner_map DirectionTripleMap describing how corners
             *                   transform
             */
            void apply_corner_map(const DirectionTripleMap& corner_map);
            
            /**
             * Calls CubeState::apply_center_map, CubeState::apply_edge_map,
             * and CubeState::apply_corner_map.
             * 
             * @param center_map DirectionMap to pass to
             *                   CubeState::apply_center_map
             * @param edge_map DirectionPairMap to pass to
             *                 CubeState::apply_edge_map
             * @param corner_map DirectionTripleMap to pass to
             *                   CubeState::apply_corner_map
             */
            void apply_maps(const DirectionMap& center_map,
                const DirectionPairMap& edge_map,
                const DirectionTripleMap& corner_map);
            
            /**
             * Applies a rotation given by a DirectionMap.
             * 
             * @param center_map DirectionMap describing the rotation
             */
            void apply_rotation(const DirectionMap& center_map);
            
            /**
             * Applies a move of a layer.
             * 
             * @param edge_map DirectionPairMap describing the layer move
             */
            void apply_layer_move(const DirectionPairMap& edge_map);
            
            /**
             * Gets the DirectionMap corresponding to a given tumble (i.e. a
             * rotation that brings a Color to the top).
             * 
             * @param top the Color to move to the top of the cube
             * @param front the Color to move to the front of the cube
             * 
             * @returns DirectionMap corresponding to the desired rotation
             */
            const DirectionMap& get_tumble_map(const Color& top,
                const Color& front) const;
            
            /**
             * Gets the DirectionMap corresponding to a given spin (i.e. a
             * rotation that keeps the top and bottom fixed).
             * 
             * @param front the Color to move to the front of the cube
             * 
             * @returns DirectionMap corresponding to the desired rotation
             */
            const DirectionMap& get_spin_map(const Color& front) const;
            
            /**
             * Gets the DirectionPairMap corresponding to a given layer move.
             * 
             * @param move the move to apply
             * 
             * @returns the DirectionPairMap corresponding to the given Move
             */
            const DirectionPairMap& get_pair_map(const Move& move) const;
        public:
            /// Initializes a solved cube.
            CubeState();
            
            /**
             * Constructs a cube by copying another
             * 
             * @param other the CubeState to copy
             */
            CubeState(const CubeState& other);
            
            /**
             * Constructs a solved cube and scrambles it.
             * 
             * @param scramble the Algorithm to apply to the solved cube after
             *                 initialization
             */
            CubeState(const Algorithm& scramble);
            
            /**
             * Constructs a solved cube and scrambles and rotates it.
             * 
             * @param scramble the TrackedAlgorithm to apply to the solved cube
             *                 after initialization
             */
            CubeState(const TrackedAlgorithm& scramble);
            
            /**
             * Initilizes a cube with the given coloring.
             * NOTE: this makes a cube that is not guaranteed to be solvable
             * 
             * @param centers the coloring scheme of the centers
             * @param edges the coloring scheme of the edges
             * @param corners the coloring scheme of the corners
             */
            CubeState(const CenterColorScheme& centers,
                const EdgeColorScheme& edges, const CornerColorScheme& corners);
            
            /**
             * Moves a CubeState using the = operator.
             * 
             * @param cube the CubeState to move to this
             */
            void operator=(CubeState&& cube);
            
            /**
             * Gets the current coloring scheme of the centers.
             * 
             * @returns CenterColorScheme describing coloring of the centers
             */
            const CenterColorScheme& center_color_scheme() const;
            
            /**
             * Gets the current coloring scheme of the edges.
             * 
             * @returns EdgeColorScheme describing coloring of the edges
             */
            const EdgeColorScheme& edge_color_scheme() const;
            
            /**
             * Gets the current coloring scheme of the corners.
             * 
             * @returns CornerColorScheme describing coloring of the corners
             */
            const CornerColorScheme& corner_color_scheme() const;
            
            /**
             * Finds the Color of the center at the given Direction.
             * 
             * @param direction the Direction under consideration
             * 
             * @returns the Color of the center at the given Direction
             */
            const Color& center_at(const Direction& direction) const;
            
            /**
             * Finds the Color values describing the edge at the given
             * DirectionPair.
             * 
             * @param pair the DirectionPair describing the edge to find
             * 
             * @returns the ColorPair describing the colors on the edge
             */
            const ColorPair& edge_at(const DirectionPair& pair) const;
            
            /**
             * Finds the Color values describing the corner at the given
             * DirectionTriple.
             * 
             * @param triple the DirectionTriple describing the corner to find
             * 
             * @returns the ColorTriple describing the colors on the corner
             */
            const ColorTriple& corner_at(const DirectionTriple& triple) const;
            
            /**
             * Orients the cube so that the given Color values are on the top
             * and front.
             * 
             * @param top the Color to move to the top
             * @param front the Color to move to the front
             * 
             * @returns the orientation applied as a DirectionMap
             */
            DirectionMap orient(const Color& top, const Color& front);
            
            /**
             * Orients the cube so that the centers described by the given
             * ColorPair are at the top and front.
             * 
             * @param top_front ColorPair describing Color values to move to
             *                  the top and front
             * 
             * @returns the orientation applied as a DirectionMap
             */
            DirectionMap orient(const ColorPair& top_front);
            
            /**
             * Orients the cube using the given DirectionMap.
             * 
             * @param orientation_map DirectionMap describing the orientation
             * 
             * @returns the DirectionMap applied
             */
            DirectionMap orient(const DirectionMap& orientation_map);
            
            /**
             * Finds the Direction that has the given Color center.
             * 
             * @param center the Color of the center to find
             * 
             * @returns the Direction to the desired center
             */
            Direction find(const Color& center) const;
            
            /**
             * Finds the DirectionPair that has the given ColorPair edge.
             * 
             * @param edge the ColorPair of the edge to find
             * 
             * @returns pair whose first element is the DirectionPair of the
             *          edge and whose second element is a bool describing
             *          whether the DirectionPair is ordered opposite to the
             *          ColorPair
             */
            std::pair<DirectionPair,bool> find(const ColorPair& edge) const;
            
            /**
             * Finds the DirectionTriple that has the given ColorTriple corner.
             * 
             * @param corner the ColorTriple of the corner to find
             * 
             * @returns pair whose first element is the DirectionTriple of the
             *          corner and whose second element is a TripleSwapInfo
             *          describing how to make the given ColorTriple into the
             *          ColorTriple of the corner
             */
            std::pair<DirectionTriple,TripleSwapInfo>
                find(const ColorTriple& corner) const;
            
            /**
             * Performs the given move.
             * 
             * @param move the Move to apply
             * 
             * @returns the Move applied
             */
            const Move& perform(const Move& move);
            
            /**
             * Performs the given Algorithm.
             * 
             * @param algorithm the Algorithm of Move objects to apply
             * 
             * @returns the Algorithm applied
             */
            const Algorithm& perform(const Algorithm& algorithm);
            
            /**
             * Performs the given TrackedAlgorithm (i.e.
             * Algorithm + orientation).
             * 
             * @param algorithm the TrackedAlgorithm of Move objects and
             *                  orientations to apply
             * 
             * @returns the TrackedAlgorithm performed
             */
            const TrackedAlgorithm& perform(const TrackedAlgorithm& algorithm);
            
            /**
             * Cheats the cube by directly applying the given center, edge, and
             * corner maps.
             * 
             * @param center_map DirectionMap describing how centers transform
             * @param edge_map DirectionPairMap describing how edges trasnform
             * @param corner_map DirectionTripleMap describing how corners
             *                   transform
             */
            void cheat(const DirectionMap& center_map,
                const DirectionPairMap& edge_map,
                const DirectionTripleMap& corner_map);
            
            /**
             * Checks if this cube is currently solved (i.e. if it is congruent
             * to a cube that was initialized solved).
             * 
             * @returns true if and only if the cube is solved
             */
            bool is_solved() const;
            
            /**
             * Checks for equality between two CubeState objects. Note that two
             * cubes can only be equal if they are oriented in the same way. To
             * ignore orientation, use CubeState::congruent. 
             * 
             * @param other the second CubeState
             * 
             * @returns true if and only if this and other have the same
             *          centers, edges, and corners (including orientation)
             */
            bool operator==(const CubeState& other) const;
            
            /**
             * Checks if this cube has been cheated
             * 
             * @returns true if things other than Move, Algorithm, and
             *          TrackedAlgorithm objects have been applied to the cube
             */
            bool is_cheated() const;

            /**
             * Checks if a cube is guaranteed solvable, meaning that it hasn't
             * been cheated and that it was initialized solved or with a
             * scramble.
             * 
             * @returns true if and only if it is possible to solve this cube
             *          with Move, Algorithm, and/or TrackedAlgorithm objects
             */
            bool is_guaranteed_solvable() const;
            
            /**
             * Checks if this cube is congruent to the other one.
             * 
             * @param other CubeState to check for congruence with
             * 
             * @return true if and only if this can be rotated to be equal to
             *         other
             */
            bool congruent(const CubeState& other) const;
            
            /**
             * Creates a string version of this CubeState for printing purposes
             * 
             * @returns string form of CubeState
             */
            std::string to_string() const;
    };
    
    /**
     * Prints a CubeState string.
     * 
     * @param out the output stream to write to
     * @param cube_state the CubeState to print
     * 
     * @returns reference to the output stream
     */
    inline std::ostream& operator<<(
        std::ostream& out, const CubeState& cube_state)
    {
        out << cube_state.to_string();
        return out;
    }
    
    /**
     * Reads in a CubeState from an input stream.
     * 
     * @param in the input stream to read from
     * @param cube reference to set to read cube
     * 
     * @returns reference to the input stream
     */
    std::istream& operator>>(std::istream& in, CubeState& cube);
    
    /**
     * Computes the number of times this Algorithm must be applied to a solved
     * cube before the cube is solved again.
     * 
     * @param algorithm the Algorithm to apply
     * 
     * @returns the number of times this Algorithm must be applied to a solved
     *          cube before the cube is solved again
     */
    unsigned long period(const Algorithm& algorithm);
    
    /**
     * Computes the number of times this TrackedAlgorithm must be applied to a
     * solved cube before the cube is solved again.
     * 
     * @param algorithm the TrackedAlgorithm to apply
     * 
     * @returns the number of times this TrackedAlgorithm must be applied to a
     *          solved cube before the cube is solved again
     */
    unsigned long period(const TrackedAlgorithm& algorithm);
}

#endif
