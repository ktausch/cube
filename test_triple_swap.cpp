#include "Color.h"
#include "gtest/gtest.h"

namespace cube
{

TEST(TripleSwapInfoTest, MultiplicationTest)
{
    for (int c1 = 0; c1 < 2; c1++)
    {
        for (int c2 = 0; c2 < 2; c2++)
        {
            for (int c3 = 0; c3 < 2; c3++)
            {
                TripleSwapInfo info1{c1 == 0, c2 == 0, c3 == 0};
                for (int c4 = 0; c4 < 2; c4++)
                {
                    for (int c5 = 0; c5 < 2; c5++)
                    {
                        for (int c6 = 0; c6 < 2; c6++)
                        {
                            TripleSwapInfo info2{c4 == 0, c5 == 0, c6 == 0};
                            ColorTriple test_triple1{WHITE,ORANGE,BLUE};
                            ColorTriple test_triple2{test_triple1};
                            test_triple1.perform_swaps(info1);
                            test_triple1.perform_swaps(info2);
                            test_triple2.perform_swaps(info1 * info2);
                            EXPECT_EQ(test_triple1, test_triple2);
                        }
                    }
                }
            }
        }
    }
}

TEST(TripleSwapInfoTest, InversionTest)
{
    for (int c1 = 0; c1 < 2; c1++)
    {
        for (int c2 = 0; c2 < 2; c2++)
        {
            for (int c3 = 0; c3 < 2; c3++)
            {
                TripleSwapInfo info{c1 == 0, c2 == 0, c3 == 0};
                ColorTriple test_triple1{WHITE,ORANGE,BLUE};
                ColorTriple test_triple2{test_triple1};
                test_triple1.perform_swaps(info);
                test_triple1.perform_swaps(~info);
                EXPECT_EQ(test_triple1, test_triple2);
            }
        }
    }
}

}  // namespace cube::test

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}