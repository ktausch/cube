#include <ostream>
#include "Color.h"

namespace cube
{
    char color_to_char(Color color)
    {
        switch (color)
        {
            case WHITE:
                return 'W';
            case ORANGE:
                return 'O';
            case BLUE:
                return 'B';
            case GREEN:
                return 'G';
            case RED:
                return 'R';
            case YELLOW:
            default:
                return 'Y';
        }
    }
    
    Color char_to_color(char character)
    {
        switch (std::toupper(character))
        {
            case 'W':
                return WHITE;
            case 'O':
                return ORANGE;
            case 'B':
                return BLUE;
            case 'G':
                return GREEN;
            case 'R':
                return RED;
            case 'Y':
                return YELLOW;
            default:
                std::string error_message{
                    "No color can be created from the given char, which is '"};
                error_message += character;
                error_message += "'.";
                throw std::invalid_argument(error_message);
        }
    }

    std::ostream& operator<<(std::ostream& out, const Color& color)
    {
#define CASE(clr) case clr: {out << #clr; break;}
        switch (color)
        {
            CASE(WHITE)
            CASE(ORANGE)
            CASE(BLUE)
            CASE(GREEN)
            CASE(RED)
            CASE(YELLOW)
        }
#undef CASE
        return out;
    }

    std::istream& operator>>(std::istream& in, Color& color)
    {
        char character;
        in >> character;
        color = char_to_color(character);
        return in;
    }

    size_t ColorHash::operator()(const Color& color) const
    {
        return static_cast<size_t>(color);
    }
    
    ColorPair::ColorPair(Color first, Color second)
        : first_{first}, second_{second} {}
    
    ColorPair::ColorPair(const ColorPair& pair)
        : first_{pair.first_}, second_{pair.second_} {}

    void ColorPair::swap()
    {
        std::swap(first_, second_);
    }
    
    Color ColorPair::first() const {return first_;}
    
    Color ColorPair::second() const {return second_;}
    
    bool ColorPair::is_valid() const
    {
        const size_t first_hash = color_hasher(first());
        const size_t second_hash = color_hasher(second());
        return ((first_hash + second_hash) != 5) && (first_hash != second_hash);
    }
    
    std::optional<bool> ColorPair::permutation(
        const ColorPair& pair) const
    {
        if (first() == pair.first())
        {
            return (second() == pair.second()) ? std::make_optional<bool>(false) : std::nullopt;
        }
        else if (first() == pair.second())
        {
            return (second() == pair.first()) ? std::make_optional<bool>(true) : std::nullopt;
        }
        else
        {
            return std::nullopt;
        }
    }
    
    bool ColorPair::operator==(const ColorPair& pair) const
    {
        return ((first() == pair.first()) && (second() == pair.second()));
    }
    
    bool ColorPair::operator!=(const ColorPair& pair) const
    {
        return ((first() != pair.first()) || (second() != pair.second()));
    }

    std::ostream& operator<<(std::ostream& out, const ColorPair& pair)
    {
        out << '(' << pair.first() << ',' << pair.second() << ')';
        return out;
    }
    
    size_t ColorPairHash::operator() (const ColorPair& pair) const
    {
        const size_t first_hash = color_hasher(pair.first());
        const size_t second_hash = color_hasher(pair.second());
        return (first_hash * 6) + second_hash;
    }

    ColorTriple::ColorTriple(Color first, Color second, Color third)
        : first_{first}, second_{second}, third_{third} {}
    
    ColorTriple::ColorTriple(const ColorTriple& triple)
        : first_{triple.first_}, second_{triple.second_}, third_{triple.third_} {}
    
    void ColorTriple::perform_swaps(const TripleSwapInfo& required_swaps)
    {
        if (std::get<0>(required_swaps))
        {
            std::swap(first_, second_);
        }
        if (std::get<1>(required_swaps))
        {
            std::swap(second_, third_);
        }
        if (std::get<2>(required_swaps))
        {
            std::swap(first_, second_);
        }
    }
    
    ColorTriple ColorTriple::copy_with_swaps(const TripleSwapInfo& required_swaps) const
    {
        ColorTriple new_color_triple{*this};
        new_color_triple.perform_swaps(required_swaps);
        return new_color_triple;
    }
    
    Color ColorTriple::first() const {return first_;}
    
    Color ColorTriple::second() const {return second_;}
    
    Color ColorTriple::third() const {return third_;}
    
    bool ColorTriple::is_valid() const
    {
        const int first_axis = (color_hasher(first()) % 3) + 1;
        const int second_axis = (color_hasher(second()) % 3) + 1;
        const int third_axis = (color_hasher(third()) % 3) + 1;
        return ((first_axis + second_axis + third_axis) == 6) &&
            (first_axis * second_axis * third_axis);
    }
    
    std::optional<TripleSwapInfo>
        ColorTriple::permutation(const ColorTriple& triple) const
    {
        if (first() == triple.first())
        {
            if (second() == triple.second())
            {
                return (third() == triple.third())
                    ? std::make_optional<TripleSwapInfo>(false, false, false) : std::nullopt;
            }
            else if (second() == triple.third())
            {
                return (third() == triple.second())
                    ? std::make_optional<TripleSwapInfo>(false, true, false) : std::nullopt;
            }
            else
            {
                return std::nullopt;
            }
        }
        else if (first() == triple.second())
        {
            if (second() == triple.first())
            {
                return (third() == triple.third())
                    ? std::make_optional<TripleSwapInfo>(false, false, true) : std::nullopt;
            }
            else if (second() == triple.third())
            {
                // this one should become (false, true, true) if inverse swap info is desired
                return (third() == triple.first())
                    ? std::make_optional<TripleSwapInfo>(true, true, false) : std::nullopt;
            }
            else
            {
                return std::nullopt;
            }
        }
        else if (first() == triple.third())
        {
            if (second() == triple.first())
            {
                // this one should become (true, true, false) if inverse swap info is desired
                return (third() == triple.second())
                    ? std::make_optional<TripleSwapInfo>(false, true, true) : std::nullopt;
            }
            else if (second() == triple.second())
            {
                return (third() == triple.first())
                    ? std::make_optional<TripleSwapInfo>(true, true, true) : std::nullopt;
            }
            else
            {
                return std::nullopt;
            }
        }
        else
        {
            return std::nullopt;
        }
    }
    
    bool ColorTriple::operator==(const ColorTriple& triple) const
    {
        return (
            (first() == triple.first()) &&
            (second() == triple.second()) &&
            (third() == triple.third()));
    }
    
    bool ColorTriple::operator!=(const ColorTriple& triple) const
    {
        return (
            (first() != triple.first()) ||
            (second() != triple.second()) ||
            (third() != triple.third()));
    }

    std::vector<Color> ColorTriple::as_vector() const
    {
        return {first(), second(), third()};
    }
    
    int ColorTriple::shared_colors(const ColorTriple& triple) const
    {
        const std::vector<Color> these = as_vector();
        std::vector<Color> others = triple.as_vector();
        int shared{0};
        for (const Color this_color : these)
        {
            for (auto it = others.begin(); it != others.end(); it++)
            {
                if ((*it) == this_color)
                {
                    shared++;
                    others.erase(it);
                    break;
                }
            }
        }
        return shared;
    }
    
    std::ostream& operator<<(std::ostream& out, const ColorTriple& triple)
    {
        out << '(' << triple.first() << ','
            << triple.second() << ','
            << triple.third() << ')';
        return out;
    }
    
    size_t ColorTripleHash::operator()(const ColorTriple& triple) const
    {
        const size_t first_hash = color_hasher(triple.first());
        const size_t second_hash = color_hasher(triple.second());
        const size_t third_hash = color_hasher(triple.third());
        return (((first_hash * 6) + second_hash) * 6) + third_hash;
    }
}