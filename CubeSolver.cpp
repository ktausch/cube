#include <algorithm>
#include <sstream>
#include <iomanip>
#include <chrono>
#include "CubeSolver.h"

namespace cube
{
    static const Algorithm swap_top_front_and_top_right_edges =
        read_algorithm("L'URU'LU2R'URU2R'");
    static const Algorithm swap_top_front_and_top_left_edges =
        mirror_algorithm(swap_top_front_and_top_right_edges, Axis::LEFTRIGHT);
    static const Algorithm cycle_edges_clockwise =
        read_algorithm("F2ULR'F2L'RUF2");
    static const Algorithm cycle_edges_counter_clockwise =
        mirror_algorithm(cycle_edges_clockwise, Axis::LEFTRIGHT);
    static const Algorithm cycle_corners_counter_clockwise =
        read_algorithm("LF'LB2L'FLB2L2");
    static const Algorithm cycle_corners_clockwise =
        mirror_algorithm(cycle_corners_counter_clockwise, Axis::LEFTRIGHT);
    static const Algorithm swap_opposite_corners_final_layer =
        read_algorithm("U2L2R2DL2R2U2L2R2DL2R2");

    CubeSolver::CubeSolver(const CubeState& state) : cube{state} {}

    const CubeState& CubeSolver::state() const
    {
        return cube;
    }
    
    Algorithm CubeSolver::solve()
    {
        TrackedAlgorithm algorithm{};
        algorithm += solve_first_layer();
        algorithm += solve_second_layer();
        algorithm += solve_third_layer();
        algorithm.reduce();
        return algorithm.algorithm();
    }
    
    TrackedAlgorithm CubeSolver::solve_first_layer()
    {
        TrackedAlgorithm algorithm{};
        algorithm += solve_first_cross();
        algorithm += solve_first_layer_corners();
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::solve_third_layer()
    {
        TrackedAlgorithm algorithm{};
        algorithm += orient_third_layer();
        algorithm += permute_third_layer();
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::orient_third_layer()
    {
        TrackedAlgorithm algorithm{};
        algorithm += orient_third_layer_edges();
        algorithm += orient_third_layer_corners();
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::permute_third_layer()
    {
        TrackedAlgorithm algorithm{};
        algorithm += permute_third_layer_edges();
        algorithm += permute_third_layer_corners();
        return algorithm;
    }

    TrackedAlgorithm CubeSolver::solve_first_cross()
    {
        TrackedAlgorithm algorithm{};
        for (int num_edges_solved = 0; num_edges_solved < 4; num_edges_solved++)
        {
            algorithm += solve_first_layer_edge(num_edges_solved);
        }
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::solve_first_layer_edge(int num_edges_solved)
    {
        TrackedAlgorithm algorithm{};
        if (num_edges_solved >= 4)
        {
            return algorithm;
        }
        const std::vector<Color> edge_colors_to_find{ORANGE,GREEN,RED,BLUE};
        const Color& edge_color_to_find =
            edge_colors_to_find[num_edges_solved];
        const ColorPair edge_to_find{WHITE,edge_color_to_find};
        algorithm += cube.orient(edge_to_find);
        const std::pair<DirectionPair,bool>& edge_location_info =
            cube.find(edge_to_find);
        const DirectionPair& where = edge_location_info.first;
        const bool flipped = edge_location_info.second;
        Algorithm sub_algorithm;
        if (where.first() == UP)
        {
            if (flipped)
            {
                // in this conditional, the desired edge is on top in the
                // wrong orientation
                std::unique_ptr<Move> move_to_get_to_right{nullptr};
                if (where.second() != RIGHT)
                {
                    move_to_get_to_right = std::make_unique<Move>(UP,
                        where.second() == BACK, where.second() == LEFT);
                    sub_algorithm.push_back(*move_to_get_to_right);
                }
                sub_algorithm.emplace_back(RIGHT, false, false);
                if (move_to_get_to_right != nullptr)
                {
                    sub_algorithm.push_back(move_to_get_to_right->inverse());
                }
                sub_algorithm.emplace_back(FRONT, false, false);
            }
            else
            {
                // in this conditional, the desired edge is on top in the right
                // orientation, but might have to be moved to the right place
                if (where.second() == FRONT)
                {
                    // this edge is already solved!
                }
                else if (num_edges_solved == 0)
                {
                    // just rotate top layer until edge is placed
                    sub_algorithm.emplace_back(UP,
                        where.second() == RIGHT, where.second() == BACK);
                }
                else
                {
                    // rotate desired edge so white facing down
                    sub_algorithm.emplace_back(where.second(), true, true);
                    // spin bottom layer until desired edge is at {DOWN,FRONT}
                    sub_algorithm.emplace_back(DOWN,
                        where.second() == LEFT, where.second() == BACK);
                    // rotate the front layer so desired edge is at {UP,FRONT}
                    sub_algorithm.emplace_back(FRONT, true, true);
                }
            }
        }
        else if (where.first() == DOWN)
        {
            if (flipped)
            {
                // in this conditional, the desired edge is on the bottom
                // with the non-UPDOWN color facing down
                if (where.second() != RIGHT)
                {
                    // rotate piece so that WHITE,other is at RIGHT,DOWN
                    sub_algorithm.emplace_back(DOWN,
                        where.second() == FRONT, where.second() == LEFT);
                }
                sub_algorithm.emplace_back(RIGHT, true, false);
                sub_algorithm.emplace_back(FRONT, false, false);
                sub_algorithm.emplace_back(RIGHT, false, false);
            }
            else
            {
                // in this conditional, the desired edge is on the bottom
                // with the white color facing down
                if (where.second() != FRONT)
                {
                    sub_algorithm.emplace_back(DOWN,
                        where.second() == LEFT, where.second() == BACK);
                }
                sub_algorithm.emplace_back(FRONT, true, true);
            }
        }
        else if (where.first() == FRONT)
        {
            if (flipped)
            {
                // in this conditional, the white side of the edge is on
                // the left or right and the other side is on the front
                sub_algorithm.emplace_back(FRONT,
                    where.second() == LEFT, false);
            }
            else
            {
                // in this conditional, the white side of the edge is on
                // the front and the other side is on the left or right
                Move first_move{UP, where.second() == LEFT, false};
                sub_algorithm.push_back(first_move);
                sub_algorithm.emplace_back(where.second(),
                    where.second() == RIGHT, false);
                sub_algorithm.push_back(first_move.inverse());
            }
        }
        else // (where.first() == BACK)
        {
            if (flipped)
            {
                // in this conditional, the white side of the edge is on
                // the left or right and the other side is on the back
                sub_algorithm.emplace_back(UP, true, true);
                sub_algorithm.emplace_back(BACK,
                    where.second() == RIGHT, false);
                sub_algorithm.emplace_back(UP, true, true);
            }
            else
            {
                // in this conditional the white side of the edge is on the
                // back and the other side is on the left or right
                const bool second_on_left = (where.second() == LEFT);
                sub_algorithm.emplace_back(UP, second_on_left, false);
                sub_algorithm.emplace_back(where.second(),
                    second_on_left, false);
                sub_algorithm.emplace_back(UP, !second_on_left, false);
            }
        }
        algorithm += cube.perform(sub_algorithm);
        return algorithm;
    }

    TrackedAlgorithm CubeSolver::solve_first_layer_corners()
    {
        TrackedAlgorithm algorithm{};
        for (int num_corners_solved = 0; num_corners_solved < 4;
            num_corners_solved++)
        {
            algorithm += solve_first_layer_corner(num_corners_solved);
        }
        return algorithm;
    }
    
    TrackedAlgorithm
        CubeSolver::solve_first_layer_corner(int num_corners_solved)
    {
        TrackedAlgorithm algorithm{};
        if (num_corners_solved >= 4)
        {
            return algorithm;
        }
        const std::vector<ColorPair> corner_color_pairs_to_find{
            {ORANGE,BLUE}, {BLUE,RED}, {RED,GREEN}, {GREEN,ORANGE}};
        const ColorPair& corner_color_pair_to_find =
            corner_color_pairs_to_find[num_corners_solved];
        const ColorTriple corner_to_find{WHITE,
            corner_color_pair_to_find.first(),
            corner_color_pair_to_find.second()};
        algorithm += cube.orient(WHITE, corner_color_pair_to_find.first());
        const auto& corner_location_info = cube.find(corner_to_find);
        const DirectionTriple& where = corner_location_info.first;
        const ColorTriple oriented_corner_to_find =
            corner_to_find.copy_with_swaps(corner_location_info.second);
        Algorithm sub_algorithm;
        if (where.first() == UP)
        {
            if (oriented_corner_to_find.first() == WHITE)
            {
                if ((where.second() != FRONT) || (where.third() != LEFT))
                {
                    if (where.second() == FRONT)
                    {
                        sub_algorithm.emplace_back(RIGHT, false, false);
                        sub_algorithm.emplace_back(DOWN, false, false);
                        sub_algorithm.emplace_back(RIGHT, true, false);
                    }
                    else if (where.third() == LEFT)
                    {
                        sub_algorithm.emplace_back(LEFT, false, false);
                        sub_algorithm.emplace_back(DOWN, true, true);
                        sub_algorithm.emplace_back(LEFT, true, false);
                        sub_algorithm.emplace_back(DOWN, false, false);
                    }
                    else
                    {
                        sub_algorithm.emplace_back(RIGHT, true, false);
                        sub_algorithm.emplace_back(DOWN, false, false);
                        sub_algorithm.emplace_back(RIGHT, false, false);
                        sub_algorithm.emplace_back(DOWN, true, true);
                    }
                    sub_algorithm.emplace_back(LEFT, true, false);
                    sub_algorithm.emplace_back(DOWN, true, false);
                    sub_algorithm.emplace_back(LEFT, false, false);
                }
            }
            else if (oriented_corner_to_find.second() == WHITE)
            {
                if (where.second() == FRONT)
                {
                    if (where.third() == LEFT)
                    {
                        sub_algorithm.emplace_back(LEFT, true, false);
                        sub_algorithm.emplace_back(DOWN, false, false);
                        sub_algorithm.emplace_back(LEFT, false, false);
                    }
                    else
                    {
                        sub_algorithm.emplace_back(RIGHT, false, false);
                        sub_algorithm.emplace_back(LEFT, true, false);
                        sub_algorithm.emplace_back(DOWN, true, true);
                        sub_algorithm.emplace_back(RIGHT, true, false);
                        sub_algorithm.emplace_back(LEFT, false, false);
                    }
                    sub_algorithm.emplace_back(DOWN, true, false);
                    sub_algorithm.emplace_back(LEFT, true, false);
                    sub_algorithm.emplace_back(DOWN, false, false);
                    sub_algorithm.emplace_back(LEFT, false, false);
                }
                else if (where.third() == LEFT)
                {
                    sub_algorithm.emplace_back(FRONT, false, false);
                    sub_algorithm.emplace_back(BACK, true, false);
                    sub_algorithm.emplace_back(DOWN, true, false);
                    sub_algorithm.emplace_back(FRONT, true, false);
                    sub_algorithm.emplace_back(BACK, false, false);
                }
                else
                {
                    sub_algorithm.emplace_back(RIGHT, true, false);
                    sub_algorithm.emplace_back(LEFT, true, false);
                    sub_algorithm.emplace_back(DOWN, true, false);
                    sub_algorithm.emplace_back(RIGHT, false, false);
                    sub_algorithm.emplace_back(LEFT, false, false);
                    sub_algorithm.emplace_back(DOWN, true, false);
                    sub_algorithm.emplace_back(LEFT, true, false);
                    sub_algorithm.emplace_back(DOWN, false, false);
                    sub_algorithm.emplace_back(LEFT, false, false);
                }
            }
            else
            {
                if (where.second() == FRONT)
                {
                    if (where.third() == LEFT)
                    {
                        sub_algorithm.emplace_back(LEFT, true, false);
                        sub_algorithm.emplace_back(DOWN, true, false);
                        sub_algorithm.emplace_back(LEFT, false, false);
                        sub_algorithm.emplace_back(DOWN, false, false);
                        sub_algorithm.emplace_back(LEFT, true, false);
                        sub_algorithm.emplace_back(DOWN, true, false);
                    }
                    else
                    {
                        sub_algorithm.emplace_back(RIGHT, false, false);
                        sub_algorithm.emplace_back(LEFT, true, false);
                        sub_algorithm.emplace_back(DOWN, false, false);
                        sub_algorithm.emplace_back(RIGHT, true, false);
                    }
                }
                else if (where.third() == LEFT)
                {
                    sub_algorithm.emplace_back(LEFT, false, false);
                    sub_algorithm.emplace_back(DOWN, true, true);
                    sub_algorithm.emplace_back(LEFT, true, true);
                    sub_algorithm.emplace_back(DOWN, false, false);
                }
                else
                {
                    sub_algorithm.emplace_back(RIGHT, true, false);
                    sub_algorithm.emplace_back(DOWN, true, true);
                    sub_algorithm.emplace_back(LEFT, true, false);
                    sub_algorithm.emplace_back(DOWN, true, false);
                    sub_algorithm.emplace_back(RIGHT, false, false);
                }
                sub_algorithm.emplace_back(LEFT, false, false);
            }
        }
        else
        {
            if (oriented_corner_to_find.first() == WHITE)
            {
                if ((where.second() != FRONT) || (where.third() != RIGHT))
                {
                    if (where.second() == FRONT)
                    {
                        sub_algorithm.emplace_back(DOWN, true, false);
                    }
                    else
                    {
                        sub_algorithm.emplace_back(
                            DOWN, false, where.third() == LEFT);
                    }
                }
                sub_algorithm.emplace_back(LEFT, true, false);
                sub_algorithm.emplace_back(DOWN, true, true);
                sub_algorithm.emplace_back(LEFT, false, false);
                sub_algorithm.emplace_back(FRONT, false, false);
                sub_algorithm.emplace_back(DOWN, false, false);
                sub_algorithm.emplace_back(FRONT, true, false);
            }
            else
            {
                const bool is_front_left_or_back_right =
                    ((where.second() == FRONT) == (where.third() == LEFT));
                const bool white_on_front_back =
                    (oriented_corner_to_find.second() == WHITE);
                const bool would_be_front_on_front_left =
                    (is_front_left_or_back_right == white_on_front_back);
                if ((where.second() != FRONT) || (where.third() != LEFT))
                {
                    if (where.second() == FRONT)
                    {
                        sub_algorithm.emplace_back(DOWN, false, false);
                    }
                    else
                    {
                        sub_algorithm.emplace_back(
                            DOWN, true, where.third() == RIGHT);
                    }
                }
                if (would_be_front_on_front_left)
                {
                    sub_algorithm.emplace_back(FRONT, false, false);
                    sub_algorithm.emplace_back(DOWN, false, false);
                    sub_algorithm.emplace_back(FRONT, true, false);
                }
                else
                {
                    sub_algorithm.emplace_back(LEFT, true, false);
                    sub_algorithm.emplace_back(DOWN, true, false);
                    sub_algorithm.emplace_back(LEFT, false, false);
                }
            }
        }
        algorithm += cube.perform(sub_algorithm);
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::solve_second_layer()
    {
        TrackedAlgorithm algorithm{};
        for (int num_edges_solved = 0; num_edges_solved < 4; num_edges_solved++)
        {
            algorithm += solve_second_layer_edge(num_edges_solved);
        }
        return algorithm;
    }

    TrackedAlgorithm CubeSolver::solve_second_layer_edge(int num_edges_solved)
    {
        TrackedAlgorithm algorithm{};
        if (num_edges_solved >= 4)
        {
            return algorithm;
        }
        const std::vector<ColorPair> edge_color_pairs_to_find{
            {ORANGE,BLUE},{BLUE,RED},{RED,GREEN},{GREEN,ORANGE}};
        const ColorPair& edge_to_find =
            edge_color_pairs_to_find[num_edges_solved];
        while (true)
        {
            algorithm += cube.orient(WHITE,edge_to_find.first());
            const std::pair<DirectionPair,bool>& edge_location_info =
                cube.find(edge_to_find);
            const DirectionPair& where = edge_location_info.first;
            const bool flipped = edge_location_info.second;
            Algorithm sub_algorithm{};
            if (where.first().axis() == Axis::FRONTBACK)
            {
                // in this case, the edge is in the second layer
                if ((where.first() == FRONT) && (where.second() == LEFT))
                {
                    if (!flipped)
                    {
                        // the edge is already solved in this case
                        break;
                    }
                }
                else if (where.first() == FRONT)
                {
                    algorithm += cube.orient(SPIN_LEFT);
                }
                else if (where.second() == LEFT)
                {
                    algorithm += cube.orient(SPIN_RIGHT);
                }
                else
                {
                    algorithm += cube.orient(SPIN_TWICE);
                }
                sub_algorithm.emplace_back(LEFT, true, false);
                sub_algorithm.emplace_back(DOWN, true, false);
                sub_algorithm.emplace_back(LEFT, false, false);
                sub_algorithm.emplace_back(DOWN, false, false);
                sub_algorithm.emplace_back(FRONT, false, false);
                sub_algorithm.emplace_back(DOWN, false, false);
                sub_algorithm.emplace_back(FRONT, true, false);
            }
            else if (flipped)
            {
                if (where.second().axis() == Axis::LEFTRIGHT)
                {
                    if (where.second().first())
                    {
                        sub_algorithm.emplace_back(DOWN, true, true);
                    }
                }
                else
                {
                    sub_algorithm.emplace_back(
                        DOWN, where.second().first(), false);
                }
                sub_algorithm.emplace_back(LEFT, true, false);
                sub_algorithm.emplace_back(DOWN, false, false);
                sub_algorithm.emplace_back(LEFT, false, false);
                sub_algorithm.emplace_back(DOWN, false, false);
                sub_algorithm.emplace_back(FRONT, false, false);
                sub_algorithm.emplace_back(DOWN, true, false);
                sub_algorithm.emplace_back(FRONT, true, false);
            }
            else
            {
                if (where.second().axis() == Axis::FRONTBACK)
                {
                    if (where.second().first())
                    {
                        sub_algorithm.emplace_back(DOWN, true, true);
                    }
                }
                else
                {
                    sub_algorithm.emplace_back(
                        DOWN, !where.second().first(), false);
                }
                sub_algorithm.emplace_back(FRONT, false, false);
                sub_algorithm.emplace_back(DOWN, true, false);
                sub_algorithm.emplace_back(FRONT, true, false);
                sub_algorithm.emplace_back(DOWN, true, false);
                sub_algorithm.emplace_back(LEFT, true, false);
                sub_algorithm.emplace_back(DOWN, false, false);
                sub_algorithm.emplace_back(LEFT, false, false);
            }
            algorithm += cube.perform(sub_algorithm);
        }
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::orient_third_layer_edges()
    {
        TrackedAlgorithm algorithm{};
        algorithm += cube.orient(YELLOW, ORANGE);
        std::vector<Direction> directions_with_flipped_edges{};
        for (const Direction& direction : {FRONT, LEFT, BACK, RIGHT})
        {
            DirectionPair edge_location{UP,direction};
            if (cube.edge_at(edge_location).first() != YELLOW)
            {
                directions_with_flipped_edges.push_back(direction);
            }
        }
        const int num_flipped = directions_with_flipped_edges.size();
        if ((num_flipped % 2) != 0)
        {
            throw std::runtime_error("The cube does not appear to be "
                "solvable. An odd number of final layer edges have been "
                "flipped.");
        }
        TrackedAlgorithm sub_algorithm{};
        if (num_flipped == 2)
        {
            if (directions_with_flipped_edges[0].axis() ==
                directions_with_flipped_edges[1].axis())
            {
                if (directions_with_flipped_edges[0] == LEFT)
                {
                    sub_algorithm += SPIN_LEFT;
                }
                sub_algorithm += Move(RIGHT, true, false);
                sub_algorithm += Move(UP, true, false);
                sub_algorithm += Move(RIGHT, false, false);
                sub_algorithm += Move(UP, false, false);
                sub_algorithm += MIDDLE_UP;
                sub_algorithm += Move(UP, true, false);
                sub_algorithm += Move(RIGHT, true, false);
                sub_algorithm += Move(UP, false, false);
                sub_algorithm += Move(LEFT, false, false);
                sub_algorithm += TUMBLE_FORWARD;
            }
            else
            {
                if (directions_with_flipped_edges[0] == FRONT)
                {
                    if (directions_with_flipped_edges[1] == RIGHT)
                    {
                        sub_algorithm += SPIN_RIGHT;
                    }
                    else
                    {
                        sub_algorithm += SPIN_TWICE;
                    }
                }
                else if (directions_with_flipped_edges[0] == LEFT)
                {
                    sub_algorithm += SPIN_LEFT;
                }
                sub_algorithm += MIDDLE_UP;
                sub_algorithm += Move(UP, false, false);
                sub_algorithm += MIDDLE_DOWN;
                sub_algorithm += Move(UP, true, true);
                sub_algorithm += MIDDLE_UP;
                sub_algorithm += Move(UP, false, false);
                sub_algorithm += MIDDLE_DOWN;
            }
        }
        else if (num_flipped == 4)
        {
            for (int num_middle_moves = 0; num_middle_moves < 8;
                num_middle_moves++)
            {
                if (num_middle_moves < 4)
                {
                    sub_algorithm += MIDDLE_UP;
                }
                else
                {
                    sub_algorithm += MIDDLE_DOWN;
                }
                sub_algorithm += Move(UP, true, false);
            }
        }
        algorithm += cube.perform(sub_algorithm);
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::orient_third_layer_corners()
    {
        TrackedAlgorithm algorithm{};
        algorithm += cube.orient(YELLOW, ORANGE);
        const std::vector<DirectionTriple> corner_triples_to_orient{
            {UP,FRONT,LEFT},{UP,BACK,LEFT},{UP,BACK,RIGHT},{UP,FRONT,RIGHT}};
        std::vector<CubeState> states;
        for (int num_loops = 0;; num_loops++)
        {
            states.emplace_back(cube);
            if (num_loops > 10)
            {
                throw std::runtime_error("The algorithm this program uses "
                    "to solve the cube has hit a snag. It would most likely "
                    "go on in an infinite loop.");
            }
            std::vector<std::pair<Direction,bool>>
                misoriented_yellow_locations{};
            std::vector<bool> oriented_correctly{};
            int first_misoriented = -1;
            for (int index = 0; index < corner_triples_to_orient.size();
                index++)
            {
                const DirectionTriple& direction_triple =
                    corner_triples_to_orient[index];
                const ColorTriple& color_triple =
                    cube.corner_at(direction_triple);
                const bool this_oriented_correctly =
                    (color_triple.first() == YELLOW);
                oriented_correctly.push_back(this_oriented_correctly);
                if (!this_oriented_correctly)
                {
                    if (first_misoriented == -1)
                    {
                        first_misoriented = index;
                    }
                    if (color_triple.second() == YELLOW)
                    {
                        misoriented_yellow_locations.emplace_back(
                            direction_triple.second(),
                            direction_triple.second().first() ==
                            direction_triple.third().first());
                    }
                    else // (color_triple.third() == YELLOW)
                    {
                        misoriented_yellow_locations.emplace_back(
                            direction_triple.third(),
                            direction_triple.second().first() !=
                            direction_triple.third().first());
                    }
                }
            }
            const int num_misoriented_corners =
                misoriented_yellow_locations.size();
            if (num_misoriented_corners == 0) {break;}
            else if (num_misoriented_corners == 1)
            {
                throw std::runtime_error("This cube does not appear to be "
                    "solvable. Only one corner on the last layer is "
                    "misoriented.");
            }
            else if ((num_misoriented_corners == 2) &&
                (misoriented_yellow_locations[0].first ==
                misoriented_yellow_locations[1].first))
            {
                Algorithm sub_algorithm;
                sub_algorithm.emplace_back(RIGHT, true, false);
                sub_algorithm.emplace_back(UP, true, false);
                sub_algorithm.emplace_back(RIGHT, false, false);
                sub_algorithm.emplace_back(UP, true, false);
                sub_algorithm.emplace_back(RIGHT, true, false);
                sub_algorithm.emplace_back(UP, true, true);
                sub_algorithm.emplace_back(RIGHT, false, false);
                algorithm += cube.perform(sub_algorithm);
            }
            else
            {
                int index = 0;
                if ((num_misoriented_corners == 3) &&
                    oriented_correctly[(first_misoriented + 2) % 4])
                {
                    index = 1;
                }
                const Direction& to_bring_to_front =
                    misoriented_yellow_locations[index].first;
                if (to_bring_to_front.axis() == Axis::LEFTRIGHT)
                {
                    algorithm += cube.orient(
                        to_bring_to_front.first() ? SPIN_RIGHT : SPIN_LEFT);
                }
                else if (!to_bring_to_front.first())
                {
                    algorithm += cube.orient(SPIN_TWICE);
                }
                Algorithm sub_algorithm{};
                sub_algorithm.emplace_back(RIGHT, true, false);
                sub_algorithm.emplace_back(LEFT, false, false);
                sub_algorithm.emplace_back(UP, false, false);
                sub_algorithm.emplace_back(LEFT, true, false);
                sub_algorithm.emplace_back(UP, true, false);
                sub_algorithm.emplace_back(RIGHT, false, false);
                sub_algorithm.emplace_back(UP, false, false);
                sub_algorithm.emplace_back(LEFT, false, false);
                sub_algorithm.emplace_back(UP, true, false);
                sub_algorithm.emplace_back(LEFT, true, false);
                if (misoriented_yellow_locations[0].second)
                {
                    sub_algorithm =
                        mirror_algorithm(sub_algorithm, Axis::LEFTRIGHT);
                }
                algorithm += cube.perform(sub_algorithm);
            }
        }
        return algorithm;
    }

    TrackedAlgorithm CubeSolver::permute_third_layer_edges()
    {
        TrackedAlgorithm algorithm{};
        const ColorPair desired_first_edge{YELLOW,ORANGE};
        algorithm += cube.orient(desired_first_edge);
        const Direction& current_orange_location =
            cube.find(desired_first_edge).first.second();
        if (current_orange_location != FRONT)
        {
            const Move move{UP, !current_orange_location.first(),
                current_orange_location.axis() == Axis::FRONTBACK};
            algorithm += cube.perform(move);
        }
        const Color back_edge = cube.edge_at({UP,BACK}).second();
        const Color left_edge = cube.edge_at({UP,LEFT}).second();
        algorithm += cube.orient(SPIN_TWICE);
        if (back_edge == RED)
        {
            if (left_edge == BLUE)
            {
                algorithm += cube.perform(cycle_edges_clockwise);
                algorithm += cube.orient(SPIN_RIGHT);
                algorithm += cube.perform(swap_top_front_and_top_right_edges);
            }
        }
        else if (back_edge == GREEN)
        {
            if (left_edge == RED)
            {
                algorithm += cube.perform(swap_top_front_and_top_right_edges);
            }
            else // (left_edge == BLUE)
            {
                algorithm += cube.perform(cycle_edges_counter_clockwise);
            }
        }
        else if (back_edge == BLUE)
        {
            if (left_edge == GREEN)
            {
                algorithm += cube.perform(swap_top_front_and_top_left_edges);
            }
            else // (left_edge == RED)
            {
                algorithm += cube.perform(cycle_edges_clockwise);
            }
        }
        else
        {
            throw std::runtime_error("Something is wrong. When permuting "
                "the oriented edges of the final layer, an edge is either "
                "not oriented or not on the last layer. Usually this means "
                "the cube is not well formed.");
        }
        return algorithm;
    }
    
    TrackedAlgorithm CubeSolver::permute_third_layer_corners()
    {
        TrackedAlgorithm algorithm{};
        const std::vector<DirectionTriple> corners_to_check{
            {UP,FRONT,LEFT}, {UP,BACK,LEFT}, {UP,BACK,RIGHT}, {UP,FRONT,RIGHT}};
        while (true)
        {
            algorithm += cube.orient(YELLOW, ORANGE);
            std::vector<int> num_shared_colors{};
            bool all_opposite{true};
            int num_solved{0};
            for (const DirectionTriple& corner_to_check : corners_to_check)
            {
                const ColorTriple actual_corner =
                    cube.corner_at(corner_to_check);
                const ColorTriple expected_corner{YELLOW,
                    cube.center_at(corner_to_check.second()),
                    cube.center_at(corner_to_check.third())};
                const int these_shared_colors =
                    actual_corner.shared_colors(expected_corner);
                num_shared_colors.push_back(these_shared_colors);
                all_opposite = all_opposite && (these_shared_colors == 1);
                if (these_shared_colors == 3) {num_solved++;}
            }
            if (num_solved == 4)
            {
                break;
            }
            else if (num_solved == 1)
            {
                const int solved_corner = std::find(num_shared_colors.begin(),
                    num_shared_colors.end(), 3) - num_shared_colors.begin();
                const int clockwise_adjacent_corner =
                    ((solved_corner + 1) % 4);
                const int counter_clockwise_adjacent_corner =
                    ((solved_corner + 3) % 4);
                if (num_shared_colors[clockwise_adjacent_corner] == 1)
                {
                    if ((solved_corner % 2) == 0)
                    {
                        algorithm += cube.orient(
                            (solved_corner == 0) ? SPIN_RIGHT : SPIN_LEFT);
                    }
                    else if (solved_corner != 3)
                    {
                        algorithm += cube.orient(SPIN_TWICE);
                    }
                    algorithm += cube.perform(cycle_corners_counter_clockwise);
                }
                else if
                    (num_shared_colors[counter_clockwise_adjacent_corner] == 1)
                {
                    if ((solved_corner % 2) == 1)
                    {
                        algorithm += cube.orient(
                            ((solved_corner / 2) == 0) ? SPIN_RIGHT : SPIN_LEFT);
                    }
                    else if (solved_corner != 0)
                    {
                        algorithm += cube.orient(SPIN_TWICE);
                    }
                    algorithm += cube.perform(cycle_corners_clockwise);
                }
                else
                {
                    throw std::runtime_error("The permutation of the final layer "
                        "corners failed because the corners could not be fixed by "
                        "3-cycles.");
                }
                break;
            }
            else if (all_opposite)
            {
                algorithm += cube.perform(swap_opposite_corners_final_layer);
                break;
            }
            else if (num_solved == 0)
            {
                algorithm += cube.perform(cycle_corners_clockwise);
            }
            else if (num_solved != 4)
            {
                throw std::runtime_error("The permutation of the final layer "
                    "corners failed because the corners could not be fixed by "
                    "3-cycles.");
            }
        }
        return algorithm;
    }

    extern std::string solve_step_by_step(CubeState cube,
        bool print_intermediates, bool spell_out_first_layer,
        bool spell_out_second_layer, bool spell_out_third_layer)
    {
        std::ostringstream out;
        const auto start = std::chrono::steady_clock::now();
        cube::CubeSolver solver{cube};
        out << "Initial state:\n" << cube << '\n';
        cube::TrackedAlgorithm full_algorithm{};
        cube::TrackedAlgorithm full_first_layer_edge_algorithm{};
        for (int num_edges_solved = 0; num_edges_solved < 4;
            num_edges_solved++)
        {
            cube::TrackedAlgorithm edge_algorithm =
                cube::reduce_tracked_algorithm(
                solver.solve_first_layer_edge(num_edges_solved));
            full_first_layer_edge_algorithm += cube.perform(edge_algorithm);
            if (print_intermediates)
            {
                if (spell_out_first_layer)
                {
                    out << "(First layer edge #"
                        << (num_edges_solved + 1) << ") "
                        << edge_algorithm << '\n';
                    out << "State after first layer edge #"
                        << (num_edges_solved + 1) << ":\n" << cube << '\n';
                }
                else if (num_edges_solved == 3)
                {
                    full_first_layer_edge_algorithm.reduce();
                    out << "(First layer edges) "
                        << full_first_layer_edge_algorithm << '\n';
                    out << "State after first layer edges:\n" << cube << '\n';
                }
            }
        }
        full_algorithm += full_first_layer_edge_algorithm;
        const auto first_layer_edge_end = std::chrono::steady_clock::now();
        cube::TrackedAlgorithm full_first_layer_corner_algorithm{};
        for (int num_corners_solved = 0; num_corners_solved < 4;
            num_corners_solved++)
        {
            cube::TrackedAlgorithm corner_algorithm =
                cube::reduce_tracked_algorithm(
                solver.solve_first_layer_corner(num_corners_solved));
            full_first_layer_corner_algorithm +=
                cube.perform(corner_algorithm);
            if (print_intermediates)
            {
                if (spell_out_first_layer)
                {
                    out << "(First layer corner #" << (num_corners_solved + 1)
                        << ") " << corner_algorithm << '\n';
                    out << "State after first layer corner #"
                        << (num_corners_solved + 1) << ":\n" << cube << '\n';
                }
                else if (num_corners_solved == 3)
                {
                    full_first_layer_corner_algorithm.reduce();
                    out << "(First layer corners) "
                        << full_first_layer_corner_algorithm << '\n';
                    out << "State after first layer corners:\n" << cube
                        << '\n';
                }
            }
        }
        full_algorithm += full_first_layer_corner_algorithm;
        const auto first_layer_end = std::chrono::steady_clock::now();
        cube::TrackedAlgorithm full_second_layer_algorithm{};
        for (int num_edges_solved = 0; num_edges_solved < 4;
            num_edges_solved++)
        {
            cube::TrackedAlgorithm edge_algorithm =
                solver.solve_second_layer_edge(num_edges_solved);
            full_second_layer_algorithm += cube.perform(edge_algorithm);
            if (print_intermediates)
            {
                if (spell_out_second_layer)
                {
                    out << "(Second layer edge #" << (num_edges_solved + 1)
                        << ") " << edge_algorithm << '\n';
                    out << "State after second layer edge #"
                        << (num_edges_solved + 1) << ":\n" << cube << '\n';
                }
                else if (num_edges_solved == 3)
                {
                    full_second_layer_algorithm.reduce();
                    out << "(Second layer) "
                        << full_second_layer_algorithm << '\n';
                    out << "State after second layer:\n" << cube << '\n';
                }
            }
        }
        full_algorithm += full_second_layer_algorithm;
        const auto second_layer_end = std::chrono::steady_clock::now();
        cube::TrackedAlgorithm third_layer_edge_orientation =
            solver.orient_third_layer_edges();
        full_algorithm += cube.perform(third_layer_edge_orientation);
        if (print_intermediates && spell_out_third_layer)
        {
            out << "(Third layer edge orientation) "
                << third_layer_edge_orientation << '\n';
            out << "State after third layer edge orientation:\n"
                << cube << '\n';
        }
        const auto third_layer_edge_orientation_end =
            std::chrono::steady_clock::now();
        cube::TrackedAlgorithm third_layer_corner_orientation =
            solver.orient_third_layer_corners();
        full_algorithm += cube.perform(third_layer_corner_orientation);
        if (print_intermediates && spell_out_third_layer)
        {
            out << "(Third layer corner orientation) "
                << third_layer_corner_orientation << '\n';
            out << "State after third layer corner orientation:\n"
                << cube << '\n';
        }
        const auto third_layer_corner_orientation_end =
            std::chrono::steady_clock::now();
        cube::TrackedAlgorithm third_layer_edge_permutation =
            solver.permute_third_layer_edges();
        full_algorithm += cube.perform(third_layer_edge_permutation);
        if (print_intermediates && spell_out_third_layer)
        {
            out << "(Third layer edge permutation) "
                << third_layer_edge_permutation << '\n';
            out << "State after third layer edge permutation:\n"
                << cube << '\n';
        }
        const auto third_layer_edge_permutation_end =
            std::chrono::steady_clock::now();
        cube::TrackedAlgorithm third_layer_corner_permutation =
            solver.permute_third_layer_corners();
        full_algorithm += cube.perform(third_layer_corner_permutation);
        if (print_intermediates && spell_out_third_layer)
        {
            out << "(Third layer corner permutation) "
                << third_layer_corner_permutation << '\n';
            out << "State after third layer corner permutation:\n"
                << cube << '\n';
        }
        const auto end = std::chrono::steady_clock::now();
        full_algorithm.reduce();
        if (!print_intermediates)
        {
            out << "(Full algorithm) " << full_algorithm << '\n';
            out << "State after full algorithm:\n" << cube << '\n';
        }
        const int64_t first_layer_edge_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            first_layer_edge_end - start).count();
        const int64_t first_layer_corner_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            first_layer_end - first_layer_edge_end).count();
        const int64_t first_layer_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            first_layer_end - start).count();
        const int64_t second_layer_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            second_layer_end - first_layer_end).count();
        const int64_t third_layer_edge_orientation_duration = 
            std::chrono::duration_cast<std::chrono::microseconds>(
            third_layer_edge_orientation_end - second_layer_end).count();
        const int64_t third_layer_corner_orientation_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            third_layer_corner_orientation_end -
            third_layer_edge_orientation_end).count();
        const int64_t third_layer_orientation_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            third_layer_corner_orientation_end - second_layer_end).count();
        const int64_t third_layer_edge_permutation_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            third_layer_edge_permutation_end -
            third_layer_corner_orientation_end).count();
        const int64_t third_layer_corner_permutation_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            end - third_layer_edge_permutation_end).count();
        const int64_t third_layer_permutation_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            end - third_layer_corner_orientation_end).count();
        const int64_t third_layer_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            end - second_layer_end).count();
        const int64_t full_duration =
            std::chrono::duration_cast<std::chrono::microseconds>(
            end - start).count();
        
        const int left_width = 45;
        const int right_width = 7;
        out << "Cube solving time report (all times in us)" << '\n';
        out << std::setfill('_') << std::setw(left_width + right_width)
            << "_" << '\n';
        out << '\n' << std::setfill('-');
        out << std::left << std::setw(left_width) << "Full cube"
            << std::right << std::setw(right_width) << full_duration << '\n';
        out << std::left << std::setw(left_width) << "----First layer"
            << std::right << std::setw(right_width)
            << first_layer_duration << '\n';
        out << std::left << std::setw(left_width)
            << "--------First layer edges" << std::right
            << std::setw(right_width) << first_layer_edge_duration << '\n';
        out << std::left << std::setw(left_width)
            << "--------First layer corners"
            << std::right << std::setw(right_width)
            << first_layer_corner_duration << '\n';
        out << std::left << std::setw(left_width) << "----Second layer"
            << std::right << std::setw(right_width)
            << second_layer_duration << '\n';
        out << std::left << std::setw(left_width) << "----Third layer"
            << std::right << std::setw(right_width)
            << third_layer_duration << '\n';
        out << std::left << std::setw(left_width)
            << "--------Third layer orientation"
            << std::right << std::setw(right_width)
            << third_layer_orientation_duration << '\n';
        out << std::left << std::setw(left_width)
            << "------------Third layer edge orientation"
            << std::right << std::setw(right_width)
            << third_layer_edge_orientation_duration << '\n';
        out << std::left << std::setw(left_width)
            << "------------Third layer corner orientation"
            << std::right << std::setw(right_width)
            << third_layer_corner_orientation_duration << '\n';
        out << std::left << std::setw(left_width)
            << "--------Third layer permutation"
            << std::right << std::setw(right_width)
            << third_layer_permutation_duration << '\n';
        out << std::left << std::setw(left_width)
            << "------------Third layer edge permutation"
            << std::right << std::setw(right_width)
            << third_layer_edge_permutation_duration << '\n';
        out << std::left << std::setw(left_width)
            << "------------Third layer corner permutation"
            << std::right << std::setw(right_width)
            << third_layer_corner_permutation_duration << '\n';
        return out.str();
    }
}