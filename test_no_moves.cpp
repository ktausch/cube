#include "CubeState.h"
#include "gtest/gtest.h"

namespace cube
{

class NoMovesTest : public ::testing::Test
{
    protected:
        CubeState cube{};
};

TEST_F(NoMovesTest, AlreadySolved)
{
    EXPECT_TRUE(cube.is_solved());
}

TEST_F(NoMovesTest, ToString)
{
    EXPECT_EQ(cube.to_string(),
        "   RRR   \n"
        "   RRR   \n"
        "   RRR   \n"
        "BBBWWWGGG\n"
        "BBBWWWGGG\n"
        "BBBWWWGGG\n"
        "   OOO   \n"
        "   OOO   \n"
        "   OOO   \n"
        "   YYY   \n"
        "   YYY   \n"
        "   YYY   \n");
}

TEST_F(NoMovesTest, RotatedIsStillSolved)
{
    cube.orient(YELLOW,BLUE);
    EXPECT_TRUE(cube.is_solved());
}

TEST_F(NoMovesTest, RotatesAsExpected)
{
    cube.orient(YELLOW,BLUE);
    EXPECT_EQ(cube.center_at(UP), YELLOW);
    EXPECT_EQ(cube.center_at(DOWN), WHITE);
    EXPECT_EQ(cube.center_at(FRONT), BLUE);
    EXPECT_EQ(cube.center_at(BACK), GREEN);
    EXPECT_EQ(cube.center_at(LEFT), ORANGE);
    EXPECT_EQ(cube.center_at(RIGHT), RED);

    EXPECT_EQ(cube.edge_at({UP,FRONT}), ColorPair(YELLOW,BLUE));
    EXPECT_EQ(cube.edge_at({UP,BACK}), ColorPair(YELLOW,GREEN));
    EXPECT_EQ(cube.edge_at({UP,LEFT}), ColorPair(YELLOW,ORANGE));
    EXPECT_EQ(cube.edge_at({UP,RIGHT}), ColorPair(YELLOW,RED));
    EXPECT_EQ(cube.edge_at({DOWN,FRONT}), ColorPair(WHITE,BLUE));
    EXPECT_EQ(cube.edge_at({DOWN,BACK}), ColorPair(WHITE,GREEN));
    EXPECT_EQ(cube.edge_at({DOWN,LEFT}), ColorPair(WHITE,ORANGE));
    EXPECT_EQ(cube.edge_at({DOWN,RIGHT}), ColorPair(WHITE,RED));
    EXPECT_EQ(cube.edge_at({FRONT,LEFT}), ColorPair(BLUE,ORANGE));
    EXPECT_EQ(cube.edge_at({FRONT,RIGHT}), ColorPair(BLUE,RED));
    EXPECT_EQ(cube.edge_at({BACK,LEFT}), ColorPair(GREEN,ORANGE));
    EXPECT_EQ(cube.edge_at({BACK,RIGHT}), ColorPair(GREEN,RED));

    EXPECT_EQ(cube.corner_at({UP,FRONT,LEFT}), ColorTriple(YELLOW,BLUE,ORANGE));
    EXPECT_EQ(cube.corner_at({UP,FRONT,RIGHT}), ColorTriple(YELLOW,BLUE,RED));
    EXPECT_EQ(cube.corner_at({UP,BACK,LEFT}), ColorTriple(YELLOW,GREEN,ORANGE));
    EXPECT_EQ(cube.corner_at({UP,BACK,RIGHT}), ColorTriple(YELLOW,GREEN,RED));
    EXPECT_EQ(cube.corner_at({DOWN,FRONT,LEFT}), ColorTriple(WHITE,BLUE,ORANGE));
    EXPECT_EQ(cube.corner_at({DOWN,FRONT,RIGHT}), ColorTriple(WHITE,BLUE,RED));
    EXPECT_EQ(cube.corner_at({DOWN,BACK,LEFT}), ColorTriple(WHITE,GREEN,ORANGE));
    EXPECT_EQ(cube.corner_at({DOWN,BACK,RIGHT}), ColorTriple(WHITE,GREEN,RED));
}

TEST_F(NoMovesTest, InvalidRotation)
{
    EXPECT_ANY_THROW(cube.orient(WHITE,YELLOW));
}

}  // namespace cube::test

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}